package game.ui.controllers.gameplay;

import game.models.board.port.TradePort;
import game.models.items.TradableItem;
import game.models.items.cards.Card;
import game.models.items.treasure.Treasure;
import game.models.player.Player;
import game.ui.components.Component;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.util.ArrayList;

/**
 * @(#) TradeMenuController.java 1.0 2022/05/06
 *
 * Class used for controlling the Trade Menu
 *
 * @author Jake Craven
 * @version 1.0
 *
 * Copyright (c) 2022 Aberystwyth University.
 * All rights reserved.
 */

public class TradeMenuController extends ComponentController {

    /**
     * Variables
     */

    int inventoryValue = 0; // Stores inventory values
    int tradeValue = 0; // // Stores trade values

    @FXML
    private Label portName;

    @FXML
    private ImageView imageView;

    @FXML
    private Button tradeBtn;

    @FXML
    private TableView<TradableItem> inventoryTableView;

    @FXML
    private TableColumn<?, ?> inventoryImageColumn;

    @FXML
    private TableColumn<?, ?> inventoryItemColumn;

    @FXML
    private TableColumn<?, ?> inventoryValueColumn;

    @FXML
    private Label inventoryValueLabel;

    @FXML
    private TableView<TradableItem> tradeTableView;

    @FXML
    private TableColumn<?, ?> tradeImageColumn;

    @FXML
    private TableColumn<?, ?> tradeItemColumn;

    @FXML
    private TableColumn<?, ?> tradeValueColumn;

    @FXML
    private Label tradeValueLabel;

    private TradePort port;

    @FXML
    void btnReturnClick(ActionEvent event) {
        // get a handle to the stage
        Stage stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        // close it
        stage.close();
    }

    /**
     * Button that commences the trade, it does this by saving temporary lists of the trade items and then adds/removes the correct items from the list.
     *
     * @param event
     */

    @FXML
    void btnTradeClick(ActionEvent event) {
        ObservableList<TradableItem> tempInv;
        ObservableList<TradableItem> tempTradeInv;
        tempInv = inventoryTableView.getSelectionModel().getSelectedItems();
        tempTradeInv = tradeTableView.getSelectionModel().getSelectedItems();
        for (TradableItem t: tempInv) {
            tradeTableView.getItems().add(t);
            port.getInventory().add(t);
            inventoryTableView.getItems().remove(t);
            if(t instanceof Treasure){
                getGameController().getGame().getPlayerManager().getCurrentPlayer().getInventory().remove((Treasure)t);
            }else{
                getGameController().getGame().getPlayerManager().getCurrentPlayer().getHand().remove((Card)t);
            }

        }
        for (TradableItem t: tempTradeInv) {
            inventoryTableView.getItems().add(t);
            if(t instanceof Treasure){
                getGameController().getGame().getPlayerManager().getCurrentPlayer().getInventory().add((Treasure)t);
            }else{
                getGameController().getGame().getPlayerManager().getCurrentPlayer().getHand().add((Card)t);
            }
            port.getInventory().remove(t);
            tradeTableView.getItems().remove(t);


        }

    }


    /**
     * Functions for table views being clicked. Sets a variable to the sum of the currently selected items values.
     * Checks if values for both inventories are equal, if so display the trade button, if not hide it.
     *
     * @param event
     */
    @FXML
    void invTableClicked(MouseEvent event) {
        inventoryValue = 0;
        ObservableList<TradableItem> treasureList;
        treasureList = inventoryTableView.getSelectionModel().getSelectedItems();
        for (TradableItem i:treasureList) {
            inventoryValue += i.getValue();
        }
        inventoryValueLabel.setText("Value = " + String.valueOf(inventoryValue));

        if(tradeValue == inventoryValue){
            tradeBtn.setOpacity(1);
            tradeBtn.setDisable(false);
        }
        else {
            tradeBtn.setOpacity(0.4);
            tradeBtn.setDisable(true);
        }
    }

    @FXML
    void tradeTableClicked(MouseEvent event) {
        tradeValue = 0;
        ObservableList<TradableItem> treasureList;
        treasureList = tradeTableView.getSelectionModel().getSelectedItems();
        for (TradableItem i:treasureList) {
            tradeValue += i.getValue();
        }
        tradeValueLabel.setText("Value = " + String.valueOf(tradeValue));

        if(tradeValue == inventoryValue){
            tradeBtn.setOpacity(1);
            tradeBtn.setDisable(false);
        }
        else {
            tradeBtn.setOpacity(0.4);
            tradeBtn.setDisable(true);
        }
    }

    /**
     * Function that is called when program started, used for setting up a lot of the program features.
     * Creates instance of a file for our image to use.
     * Add objects of the Treasure class to our tables and sets selection mode for our tables.
     * Also sets button to not be visible.
     *
     */
    public void setup(){
//        File file = new File("src/Images/PirateBackground.jpg");
//        Image image = new Image(file.toURI().toString());
//        imageView.setImage(image);




        inventoryTableView.setItems(addItems(getGameController().getGame().getPlayerManager().getCurrentPlayer()));
        inventoryItemColumn.setCellValueFactory(new PropertyValueFactory<>("type"));
        inventoryValueColumn.setCellValueFactory(new PropertyValueFactory<>("value"));
//        inventoryImageColumn.setCellValueFactory(new PropertyValueFactory<>("image"));

        tradeTableView.setItems(FXCollections.observableArrayList(port.getInventory()));
        tradeItemColumn.setCellValueFactory(new PropertyValueFactory<>("type"));
        tradeValueColumn.setCellValueFactory(new PropertyValueFactory<>("value"));
//        tradeImageColumn.setCellValueFactory(new PropertyValueFactory<>("image"));

        inventoryTableView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        tradeTableView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        tradeBtn.setOpacity(0.4);
        tradeBtn.setDisable(true);
    }

    public ObservableList<TradableItem> addItems(Player player){
        ObservableList<TradableItem> inventoryList = FXCollections.observableArrayList();
        ArrayList<Card> cards = new ArrayList<>();
        for (Card c:player.getHand()) {
            if(!c.isUsable()){
                cards.add(c);
            }
        }

        inventoryList.addAll(cards);
        inventoryList.addAll(player.getInventory());


        return  inventoryList;
    }


    public void setupPortTrade(TradePort portToTradeWith){
        portName.setText(portToTradeWith.getCity());
        port = portToTradeWith;
    }

    /**
     * Sends signal to controller that particular component was clicked
     *
     * @param component Component that was clicked
     */
    @Override
    public void componentClicked(Component component) {

    }

//    /**
//     * Placeholder function for adding in hard coded objects into our tables.
//     * Expect to be replaced.
//     *
//     * @return
//     */
//
//    public ObservableList<TradableItem> addItems(){
//        ObservableList<TradableItem> inventoryList = FXCollections.observableArrayList();
//
//        File file = new File("src/Images/PirateBackground.jpg");
//        Image image = new Image(file.toURI().toString());
//
//        inventoryList.add(new Treasure("Diamond", 5, image));
//        inventoryList.add(new Treasure("Gold", 3, image));
//        inventoryList.add(new Treasure("Rum", 2, image));
//        inventoryList.add(new CrewCard("Red Crew Card", 1));
//        inventoryList.add(new CrewCard("Black Crew Card", 3));
//        inventoryList.add(new ChanceCard("Chance Card", 3));
//
//        return  inventoryList;
//    }
}

