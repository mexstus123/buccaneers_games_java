package game.ui.controllers.gameplay;

import game.models.items.treasure.Treasure;
import game.models.player.Player;
import game.ui.components.Component;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.util.List;
import java.util.Objects;

/**
 * @(#) PlayerInventoryController.java 1.0 2022/05/06
 *
 * Class used for controlling the Player Inventory
 *
 * @author Franciszek Myslek
 * @version 1.0
 *
 * Copyright (c) 2022 Aberystwyth University.
 * All rights reserved.
 */

public class PlayerInventoryController extends ComponentController {
    // Panes representing the left side of the application
    @FXML
    private StackPane inventoryPane;
    @FXML
    private Rectangle inventoryPaneBg;

    // Text fields representing black and red crew cards
    @FXML
    private Label redCrewCardsNum;
    @FXML
    private Label blackCrewCardsNum;

    // Displayed current player loot
    @FXML
    private Rectangle lootIcon1;
    @FXML
    private Label lootName1;
    @FXML
    private Rectangle lootIcon2;
    @FXML
    private Label lootName2;

    // Initialize the controller
    @FXML
    private void initialize() {
        inventoryPane.getStylesheets().add(Objects.requireNonNull(getClass().getResource("style/player-gui-style.css")).toExternalForm());
    }

    /**
     * Updates the data of current player
     */
    public void update() {
        Player currentPlayer = getGameController().getGame().getPlayerManager().getCurrentPlayer();
        inventoryPaneBg.setFill(new Color(0, 0, 0, 0));
        redCrewCardsNum.setText(String.valueOf(currentPlayer.getRedCrewCards()));
        blackCrewCardsNum.setText(String.valueOf(currentPlayer.getBlackCrewCards()));
        updateLoot();
    }

    /**
     * Sends signal to controller that particular component was clicked
     *
     * @param component Component that was clicked
     */
    @Override
    public void componentClicked(Component component) {

    }

    // Registers press on show crew cards button
    @FXML
    private void showCrewCards(ActionEvent actionEvent) throws IOException {
        Stage stage = new Stage();
        FXMLLoader fxmlLoader = new FXMLLoader(CardMenuController.class.getResource("view/card-menu-view.fxml"));
        Parent start = fxmlLoader.load();
        Scene startScene = new Scene(start);
        startScene.getStylesheets().add(Objects.requireNonNull(CardMenuController.class.getResource("style/card-menu-style.css")).toExternalForm());
        stage.setScene(startScene);
        CardMenuController controller = fxmlLoader.<CardMenuController>getController();
        controller.setPlayersHand(getGameController().getGame().getPlayerManager().getCurrentPlayer().getHand());
        stage.initStyle(StageStyle.TRANSPARENT);
        Color c = new Color(0,0,0, 0.25);
        startScene.setFill(c);
        stage.centerOnScreen();
        //keep the popup centred
        stage.initModality(Modality.APPLICATION_MODAL);
        //get rid of borders
        // setting the owner to the names screen
        stage.initOwner(((Button) actionEvent.getSource()).getScene().getWindow());
        //show and wait to make the popup have to be closed before anything else is clickable
        stage.showAndWait();

        getGameController().updateAllControllers();
    }

    // Updates loot displayed on the screen
    private void updateLoot() {
        List<Treasure> loot = getGameController().getGame().getPlayerManager().getCurrentPlayer().getInventory();
        // Clear the inventory
        lootIcon1.setVisible(false);
        lootIcon2.setVisible(false);
        lootName1.setText(null);
        lootName2.setText(null);

        int alreadyShown = 0;
        for(Treasure treasure : loot) {
            if(alreadyShown == 0) { // Display first treasure
                lootIcon1.setVisible(true);
                lootIcon1.setFill(new ImagePattern(new Image(Objects.requireNonNull(Treasure.class.getResource("assets/" + treasure.getType() + ".png")).toExternalForm())));
                lootName1.setText(treasure.getType());
            } else { // Display second treasure
                lootIcon2.setVisible(true);
                lootIcon2.setFill(new ImagePattern(new Image(Objects.requireNonNull(Treasure.class.getResource("assets/" + treasure.getType() + ".png")).toExternalForm())));
                lootName2.setText(treasure.getType());
                break;
            }
            alreadyShown++;
        }
    }
}
