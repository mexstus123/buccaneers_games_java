package game.ui.controllers.gameplay;

/**
 * @(#) TutorialController.java 1.0 2022/05/06
 *
 * Class used for controlling the Tutorial Screen
 *
 * @author Jordan Pope
 * @version 1.0
 *
 * Copyright (c) 2022 Aberystwyth University.
 * All rights reserved.
 */

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

public class TutorialController {

    private Stage stage;

    @FXML // fx:id="blackRectangle"
    private Rectangle blackRectangle; // Value injected by FXMLLoader

    @FXML // fx:id="tutorialImage"
    private ImageView tutorialImage; // Value injected by FXMLLoader

    @FXML // fx:id="tutorialButton"
    private Button tutorialButton; // Value injected by FXMLLoader

    @FXML // fx:id="exitButton"
    private Button exitButton; // Value injected by FXMLLoader

    @FXML
    void exitOnPressed(ActionEvent event) {
        stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        stage.close();
    }
}