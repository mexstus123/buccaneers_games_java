package game.ui.controllers.gameplay;

import game.exceptions.EndGameDetected;
import game.models.board.BoardPiece;
import game.models.board.Board;
import game.models.board.island.Island;
import game.models.board.island.TreasureIsland;
import game.models.board.port.HomePort;
import game.models.board.port.Port;
import game.models.board.port.TradePort;
import game.models.board.tile.SeaTile;
import game.models.items.cards.Card;
import game.models.items.cards.chance.ChanceCard;
import game.models.items.cards.crew_cards.CrewCard;
import game.models.items.treasure.Treasure;
import game.models.player.Orientation;
import game.models.player.Player;
import game.models.player.PlayerManager;
import game.ui.components.Component;
import game.ui.components.assets.IslandTileAsset;
import game.ui.components.assets.PortTileAsset;
import game.ui.components.assets.SpecialTileAsset;
import game.ui.components.ship.Ship;
import game.ui.components.tile.*;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Pair;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @(#) BoardController.java 1.0 2022/05/06
 *
 * Class used for controlling the UI version of the board
 *
 * @author Franciszek Myslek
 * @version 1.0
 *
 * Copyright (c) 2022 Aberystwyth University.
 * All rights reserved.
 */
public class BoardController extends ComponentController {

    // Pane on which the board will be drawn
    @FXML
    private StackPane boardPane;

    // Pane with ports and special tiles graphics'
    @FXML
    private AnchorPane portPane;

    // Pane holding players ship's
    @FXML
    private AnchorPane shipPane;

    // Grid representing clickable board tiles
    @FXML
    private GridPane boardGrid;

    // All components rendered on the board
    private final List<Tile> components;
    private final Ship[] ships;

    // Indicate possible moves
    private final List<Tile> lightUpTiles;

    /**
     * Width and height of a single tile
     */
    public static final double TILE_SIZE = 30;

    /**
     * Creates the board controller
     */
    public BoardController() {
        components = new ArrayList<>();
        ships = new Ship[PlayerManager.NUM_PLAYERS];
        lightUpTiles = new ArrayList<>();
    }

    // Adds all stylesheets to the class
    @FXML
    private void initialize() {
        boardGrid.getStylesheets().add(Objects.requireNonNull(getClass().getResource("style/tile-style.css")).toExternalForm());
        boardPane.getStylesheets().add(Objects.requireNonNull(getClass().getResource("style/board-style.css")).toExternalForm());
        shipPane.getStylesheets().add(Objects.requireNonNull(getClass().getResource("style/ship-style.css")).toExternalForm());
        portPane.getStylesheets().add(Objects.requireNonNull(getClass().getResource("style/tile-style.css")).toExternalForm());
    }

    /**
     * Renders the board pieces onto the gridPane
     */
    public void renderBoard() {
        initializeBoard();
    }

    /**
     * Updates pieces on the board
     */
    public void update() {
        // Update ships
        for (int i = 0; i < PlayerManager.NUM_PLAYERS; i++) {
            ships[i].sail();
        }

        for (Ship ship : ships) {
            ship.setShipImage();
        }

        // Clear previous possible moves
        for (Tile tile: lightUpTiles) {
            tile.clear();
        }
        lightUpTiles.clear();

        // Draw possible tiles to move to
        List<Tile> possibleMovementTiles = new ArrayList<>();
        for (BoardPiece piece : getGameController().getGame().getCurrentPlayerPossibleMoves())
            possibleMovementTiles.add(getTile(piece));
        indicatePossibleMoveTiles(possibleMovementTiles);
    }

    // Lights up all possible moves for current player
    private void indicatePossibleMoveTiles(List<Tile> tiles) {
        for (Tile tile : tiles) {
            tile.lightUp();
            lightUpTiles.add(tile);
        }
    }

    /**
     * Gets the Tile object located on the given coordinates
     *
     * @param coordinates Coordinates X, Y used like an array
     * @return Tile object located with the given coordinates
     */
    public Tile getTile(Pair<Integer, Integer> coordinates) {
        for (Tile tile : components)
            if (Objects.equals(coordinates, new Pair<>(tile.getColumnIndex(), tile.getRowIndex())))
                return tile;
        return null;
    }

    /**
     * Gets the Tile object representing the board piece
     *
     * @param boardPiece Board piece object on the board
     * @return Tile object connected to the given BoardPiece
     */
    public Tile getTile(BoardPiece boardPiece) {
        for (Tile tile : components)
            if (tile.getBoardPiece().equals(boardPiece))
                return tile;
        return null;
    }


    /**
     * Initializes all dynamically generated components on the board
     */
    public void initializeDynamicComponents() {
        initializeShips();
        initializePortPane();
        update();
    }

    // Generates ships and positions them on the board
    private void initializeShips() {
        Player[] players = getGameController().getGame().getPlayerManager().getPlayers();
        for (int i = 0; i < PlayerManager.NUM_PLAYERS; i++) {
            ships[i] = new Ship(this, players[i]);
            shipPane.getChildren().add(ships[i]);
        }
    }

    // Generates assets representing ports and special tiles
    private void initializePortPane() {
        for (Tile tile : components) {
            if (tile instanceof PortTile)
                portPane.getChildren().add(new PortTileAsset(tile.getLayoutY(), tile.getLayoutX()));
            else if (tile instanceof SpecialTile)
                portPane.getChildren().add(new SpecialTileAsset(tile.getLayoutY(), tile.getLayoutX()));
            else if (tile instanceof IslandTile)
                portPane.getChildren().add(new IslandTileAsset(tile.getLayoutY(), tile.getLayoutX(),
                        ((Island) tile.getBoardPiece()).getSizeX(), ((Island) tile.getBoardPiece()).getSizeY()));
        }
    }

    // Generates the visual board from the game
    private void initializeBoard() {
        List<Pair<Integer, Integer>> blocked = new ArrayList<>();
        Board board = getGameController().getGame().getBoard();
        BoardPiece piece;
        boolean isBlocked;
        for (int i = 0; i < Board.ROWS; i++) {
            // Search from bottom - up because that is how the coordinates works (lower left corner)
            for (int j = Board.COLUMNS - 1; j >= 0; j--) {
                isBlocked = false;
                for (Pair<Integer, Integer> coordinates : blocked)
                    if (Objects.equals(coordinates, new Pair<>(i, j)))
                        isBlocked = true;
                piece = board.getBoardPiece(i + 1, 20 - j);
                if (!isBlocked) {
                    Tile tile;
                    // Add an Island tile
                    if (piece instanceof Island) {
                        int width = ((Island) piece).getSizeX();
                        int height = ((Island) piece).getSizeY();
                        tile = new IslandTile(this, i, j, piece);
                        // Block occupied tiles
                        for (int x = i; x < i + width; x++)
                            for (int y = j - height + 1; y <= j; y++)
                                blocked.add(new Pair<>(x, y));
                        boardGrid.add(tile, i, j - height + 1, width, height);
                        components.add(tile);
                        continue;
                    } else if (piece instanceof Port)
                        tile = new PortTile(this, i + 1, 20 - j, piece);
                    else if (piece instanceof SeaTile)
                        tile = new NormalTile(this, i + 1, 20 - j, piece);
                    else
                        tile = new SpecialTile(this, i + 1, 20 - j, piece);
                    boardGrid.add(tile, i, j);
                    components.add(tile);
                }
            }
        }
    }

    /**
     * Registers the signal from a component when clicked
     *
     * @param component Component that was clicked
     */
    public void componentClicked(Component component) {
        // React to interaction with a sea tile, port, or special tile
        if ((component instanceof PortTile tile) && (tile.getBoardPiece() instanceof HomePort port)
                && (port.getPlayer().equals(getGameController().getGame().getPlayerManager().getCurrentPlayer()))) {
            // Home port clicked
            if (lightUpTiles.contains(tile)) {
                move(tile);
                try {
                    port.unloadShip();
                } catch (EndGameDetected e) {
                    endTheGame(component);
                    return;
                }
            } else if (getGameController().getGame().getPlayerManager().getCurrentPlayer().getCoordinates().equals(
                    new Pair<>(tile.getColumnIndex(), tile.getRowIndex()))) {
                // Player clicked on the home port and is in it
                try {
                    openHomePortMenu(component, port);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        } else if ((component instanceof PortTile tile) && (tile.getBoardPiece() instanceof TradePort port)) {
            if (lightUpTiles.contains(tile)) {
                move(tile);
            } else if (getGameController().getGame().getPlayerManager().getCurrentPlayer().getCoordinates().equals(
                    new Pair<>(tile.getColumnIndex(), tile.getRowIndex())))
                // Player clicked on the trade port and is in it
                try {
                    openTradePortMenu(component, port);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
        } else if (component instanceof Tile tile && !(tile instanceof IslandTile)) {
            tile.sendInteractionSignal();
            // Move the player to the correct tile
            if (lightUpTiles.contains(tile)) {
                move(tile);
            }
        }

        getGameController().updateAllControllers();
        update();
    }

    private void move(Tile t) {
        if (!getGameController().getGame().getPlayerManager().isSpecialMode()) {
            for (Tile tile : lightUpTiles) {
                for (Player player : getGameController().getGame().getPlayerManager().getPlayers()) {
                    if (!player.equals(getGameController().getGame().getPlayerManager().getCurrentPlayer())
                            && player.getCoordinates().equals(new Pair<>(tile.getColumnIndex(), tile.getRowIndex()))) {
                        if (!t.equals(tile)) {
                            if (!openAttackChoicePopup(t))
                                break;
                        }
                        attackMode(getGameController().getGame().getPlayerManager().getCurrentPlayer(), player, tile);
                        return;
                    }
                }
            }
        }
        movePlayer(getGameController().getGame().getPlayerManager().getCurrentPlayer().getX(),getGameController().getGame().getPlayerManager().getCurrentPlayer().getY(), t);movePlayer(getGameController().getGame().getPlayerManager().getCurrentPlayer().getX(),getGameController().getGame().getPlayerManager().getCurrentPlayer().getY(), t);
        if(getGameController().getGame().getPlayerManager().isSpecialMode())
            getGameController().getGame().getPlayerManager().setSpecialMode(false, null);
    }

    private void attackMode(Player attacker, Player defender, Tile tile) {
        getGameController().getGame().moveCurrentPlayer(tile.getColumnIndex(), tile.getRowIndex());

        // Compare strength
        Player loser = attacker.getPlayerStrength() > defender.getPlayerStrength() ? defender : attacker;
        Player winner = loser.equals(attacker)? defender : attacker;
        getGameController().getGame().getPlayerManager().setSpecialMode(true, loser);
        String winnings = "The winner won: ";
        int tmpValue = 0;
        if(!loser.getInventory().isEmpty()){
            if (winner.getRoomForTreasure() == 1){
                //Take first treasure from loser inv
                List<Treasure> tmpinv = loser.getInventory();
                winner.addTreasure(loser.withdrawTreasure(tmpinv.get(0)));
                ((TreasureIsland) getGameController().getGame().getBoard().getBoardPiece(9, 9)).depositTradabelItem(loser.withdrawTreasure(tmpinv.get(1)));
                winnings = winnings + tmpinv.get(0).getType() + " for a total value of " + tmpinv.get(0).getValue() + " ";
                System.out.println(winnings);
            }
            else if(winner.getRoomForTreasure() == 2){
                //Take all treasure from loser inv
                winner.setInventory(loser.unloadShip());
                List<Treasure> treasureList = winner.getInventory();
                for(Treasure t : treasureList){
                    tmpValue += t.getValue();
                    winnings = winnings + t.getType() + " of value: " + t.getValue() + " ";
                }
                System.out.println(winnings);
            }
            else {
                List<Treasure> treasureList = loser.unloadShip();
                for(Treasure t : treasureList){
                    ((TreasureIsland) getGameController().getGame().getBoard().getBoardPiece(9, 9)).depositTradabelItem(loser.withdrawTreasure(t));
                }
            }
        } else if(loser.getInventory().isEmpty() && loser.getHand().size() > 0) {
            for (int i = 0; i < 2; i++) {
                //Withdraws 2 lowest value cards from losers hand and adds them to winners inventory if loser doesn't have any treasure to lose
                Card tmpCard = null;
                for (Card c : loser.getHand()) {
                    if (tmpCard == null) {
                        tmpCard = c;
                    }
                    if (tmpCard.getValue() > c.getValue()) {
                        tmpCard = c;
                    }
                }
                if (loser.getHand().size() == 1) {
                    i++;//Checks if they only have one card to make it only loop once instead of twice.
                }
                winner.addCard(loser.withdrawCard(tmpCard));

                //Add winning info to a string to return
                if (tmpCard instanceof CrewCard) {
                    if (((CrewCard) tmpCard).getIsBlack()) {
                        winnings = winnings + "Black Crew Card Rank " + ((CrewCard) tmpCard).getRank() + " ";
                        System.out.println(winnings);
                    } else {
                        winnings = winnings + "Red Crew Card Rank " + ((CrewCard) tmpCard).getRank() + " ";
                        System.out.println(winnings);
                    }
                } else if (tmpCard instanceof ChanceCard) {
                    winnings = winnings + "Chance Card " + ((ChanceCard) tmpCard).getCardNumber() + "with a value of " + ((ChanceCard) tmpCard).getValue() + " ";
                }
            }
        }


        // Tell who won
        String title = loser.equals(attacker) ? "Battle LOST!" : "Battle WON!";
        getGameController().updateAllControllers();
        openLoserPopup(title, winnings, tile);
    }



    public boolean openAttackChoicePopup(Component component) {
        Stage stage = new Stage();
        FXMLLoader fxmlLoader = new FXMLLoader(PlayerChoiceController.class.getResource("view/player-choice-popup.fxml"));
        Parent start = null;
        try {
            start = fxmlLoader.load();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        Scene startScene = new Scene(start);
//        startScene.getStylesheets().add(Objects.requireNonNull(HomePortController.class.getResource("style/home-port-style.css")).toExternalForm());
        stage.setScene(startScene);
        PlayerChoiceController controller = fxmlLoader.<PlayerChoiceController>getController();
        controller.setupChoicePopup(true, true, getGameController().getGame().getPlayerManager().getCurrentPlayer(), getGameController().getGame().getPlayerManager().getPlayers(), false, true);
        stage.initStyle(StageStyle.TRANSPARENT);
        Color c = new Color(0,0,0, 0.25);
        startScene.setFill(c);
        stage.centerOnScreen();
        //keep the popup centred
        stage.initModality(Modality.APPLICATION_MODAL);
        //get rid of borders
        // setting the owner to the names screen
        stage.initOwner(component.getScene().getWindow());
        //show and wait to make the popup have to be closed before anything else is clickable
        stage.showAndWait();

//        getGameController().updateAllControllers();
        return controller.isAttacking();
    }

    private void openLoserPopup(String title, String body, Component component){
        //setting up the stage
        Stage stage = new Stage();
        FXMLLoader fxmlLoader = new FXMLLoader(Objects.requireNonNull(ErrorPopUpController.class.getResource("view/ErrorPopUp.fxml")));
        Parent popUp = null;
        try {
            popUp = fxmlLoader.load();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        Scene popUpScene = new Scene(popUp);
//        popUpScene.getStylesheets().add(Objects.requireNonNull(getClass().getResource("style/player-screen-style.css")).toExternalForm());
        ErrorPopUpController controller = fxmlLoader.getController();
        controller.setupError(title, body);
        stage.setScene(popUpScene);
        //keep the popup centred
        stage.initModality(Modality.APPLICATION_MODAL);
        //get rid of borders
        stage.initStyle(StageStyle.UNDECORATED);
        //setting the owner to the names screen
        stage.initOwner(component.getScene().getWindow());
        //show and wait to make the popup have to be closed before anything else is clickable
        stage.showAndWait();
    }

    private void openHomePortMenu(Component component, HomePort port) throws IOException {
        Stage stage = new Stage();
        FXMLLoader fxmlLoader = new FXMLLoader(HomePortController.class.getResource("view/home-port-view.fxml"));
        Parent start = fxmlLoader.load();
        Scene startScene = new Scene(start);
        startScene.getStylesheets().add(Objects.requireNonNull(HomePortController.class.getResource("style/home-port-style.css")).toExternalForm());
        stage.setScene(startScene);
        HomePortController controller = fxmlLoader.<HomePortController>getController();
        controller.setupInventories(port);
        stage.initStyle(StageStyle.TRANSPARENT);
        Color c = new Color(0,0,0, 0.25);
        startScene.setFill(c);
        stage.centerOnScreen();
        //keep the popup centred
        stage.initModality(Modality.APPLICATION_MODAL);
        //get rid of borders
        // setting the owner to the names screen
        stage.initOwner(component.getScene().getWindow());
        //show and wait to make the popup have to be closed before anything else is clickable
        stage.showAndWait();

        getGameController().updateAllControllers();
    }

    private void openTradePortMenu(Component component, TradePort port) throws IOException {
        Stage stage = new Stage();
        FXMLLoader fxmlLoader = new FXMLLoader(TradeMenuController.class.getResource("view/trade-menu-view.fxml"));
        Parent start = fxmlLoader.load();
        Scene startScene = new Scene(start);
        startScene.getStylesheets().add(Objects.requireNonNull(TradeMenuController.class.getResource("style/trade-menu-style.css")).toExternalForm());
        stage.setScene(startScene);
        TradeMenuController controller = fxmlLoader.<TradeMenuController>getController();
        controller.injectGameController(getGameController());
        controller.setupPortTrade(port);
        controller.setup();
        stage.initStyle(StageStyle.TRANSPARENT);
        Color c = new Color(0,0,0, 0.25);
        startScene.setFill(c);
        stage.centerOnScreen();
        //keep the popup centred
        stage.initModality(Modality.APPLICATION_MODAL);
        //get rid of borders
        // setting the owner to the names screen
        stage.initOwner(component.getScene().getWindow());
        //show and wait to make the popup have to be closed before anything else is clickable
        stage.showAndWait();

        getGameController().updateAllControllers();
    }

    private void movePlayer(int oldX,int oldY,Tile tile) {
        getGameController().getGame().getPlayerManager().moveCurrentPlayer(tile.getColumnIndex(), tile.getRowIndex());
        int currentX = getGameController().getGame().getPlayerManager().getCurrentPlayer().getX();
        int currentY = getGameController().getGame().getPlayerManager().getCurrentPlayer().getY();
        if (oldX == currentX) {
            if (oldY < currentY) {
                getGameController().getGame().getPlayerManager().getCurrentPlayer().setOrientation(Orientation.N);
            } else if (oldY > currentY) {
                getGameController().getGame().getPlayerManager().getCurrentPlayer().setOrientation(Orientation.S);
            }
        } else if (oldY == currentY) {
            if (oldX < currentX) {
                getGameController().getGame().getPlayerManager().getCurrentPlayer().setOrientation(Orientation.E);
            } else if (oldX > currentX) {
                getGameController().getGame().getPlayerManager().getCurrentPlayer().setOrientation(Orientation.W);
            }
        } else if (oldX < currentX) {
            if (oldY < currentY) {
                getGameController().getGame().getPlayerManager().getCurrentPlayer().setOrientation(Orientation.NE);
            } else if (oldY > currentY) {
                getGameController().getGame().getPlayerManager().getCurrentPlayer().setOrientation(Orientation.SE);
            }
        } else if (oldX > currentX) {
            if (oldY < currentY) {
                getGameController().getGame().getPlayerManager().getCurrentPlayer().setOrientation(Orientation.NW);
            } else if (oldY > currentY) {
                getGameController().getGame().getPlayerManager().getCurrentPlayer().setOrientation(Orientation.SW);
            }
        }
    }

    private void endTheGame(Component component) {
        Stage stage = new Stage();
        FXMLLoader fxmlLoader = new FXMLLoader(EndScreenController.class.getResource("view/EndScreen.fxml"));
        Parent start = null;
        try {
            start = fxmlLoader.load();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        Scene startScene = new Scene(start);
        startScene.getStylesheets().add(Objects.requireNonNull(EndScreenController.class.getResource("style/EndScreen.css")).toExternalForm());
        stage.setScene(startScene);
        EndScreenController controller = fxmlLoader.<EndScreenController>getController();
        controller.setupEndScreen(getGameController().getGame().getPlayerManager().getPlayers());
        stage.show();
    }

}
