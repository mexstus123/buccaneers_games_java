package game.ui.controllers.gameplay;

import game.models.board.port.HomePort;
import game.models.items.treasure.Treasure;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

/**
 * @(#) HomePortController.java 1.0 2022/05/06
 *
 * Class used for controlling the UI version of the board
 *
 * @author Jake Craven
 * @version 1.0
 *
 * Copyright (c) 2022 Aberystwyth University.
 * All rights reserved.
 */

public class HomePortController {

    private HomePort homePort;

    @FXML
    private Rectangle portColour;

    @FXML
    private Button exitBtn;

    @FXML
    private Label inventoryValueLabel;

    @FXML
    private TableColumn<?, ?> portInventoryItemColumn;

    @FXML
    private Label portInventoryLabel;

    @FXML
    private TableView<Object> portInventoryTableView;

    @FXML
    private TableColumn<?, ?> portInventoryValueColumn;

    @FXML
    private Label portNameLabel;

    @FXML
    private Label safeStorageInventory;

    @FXML
    private TableColumn<?, ?> safeStorageItemColumn;

    @FXML
    private TableView<Object> safeStorageTableView;

    @FXML
    private TableColumn<?, ?> safeStorageValueColumn;


    public void setupInventories(HomePort homePort){
        this.homePort = homePort;

        portInventoryTableView.setItems(addItems(homePort));
        portInventoryItemColumn.setCellValueFactory(new PropertyValueFactory<>("type"));
        portInventoryValueColumn.setCellValueFactory(new PropertyValueFactory<>("value"));

        safeStorageTableView.setItems(addItemsStorage(homePort));
        safeStorageItemColumn.setCellValueFactory(new PropertyValueFactory<>("type"));
        safeStorageValueColumn.setCellValueFactory(new PropertyValueFactory<>("value"));

        inventoryValueLabel.setText("Value = " + addValues());
        portNameLabel.setText(homePort.getCity());
        portColour.setFill(homePort.getPlayer().getColor().getJavafxColor());
    }

    private ObservableList<Object> addItems(HomePort homePort){
        ObservableList<Object> inventoryList = FXCollections.observableArrayList();

        inventoryList.addAll(homePort.getInventory());

        return  inventoryList;
    }

    public ObservableList<Object> addItemsStorage(HomePort homePort){
        ObservableList<Object> inventoryList = FXCollections.observableArrayList();

        inventoryList.addAll(homePort.getSafeInventory());

        return  inventoryList;
    }
    @FXML
    void exitMenu(ActionEvent event) {

        // get a handle to the stage
        Stage stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        // close it
        stage.close();
    }

    public Integer addValues() {
        int value = 0;
        Treasure treasure;

        for(int i = 0; i <portInventoryTableView.getItems().size(); i++){
            treasure = (Treasure) portInventoryTableView.getItems().get(i);
            value += treasure.getValue();
        }

        for(int i = 0; i <safeStorageTableView.getItems().size(); i++){
            treasure = (Treasure) safeStorageTableView.getItems().get(i);
            value += treasure.getValue();
        }

        return value;
    }
}
