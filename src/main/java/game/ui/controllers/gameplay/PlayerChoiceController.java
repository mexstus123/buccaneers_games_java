package game.ui.controllers.gameplay;

import game.models.player.Player;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;

/**
 * @(#) PlayerChoiceController.java 1.0 2022/05/06
 *
 * Class used for controlling the player choice screen
 *
 * @author Franciszek Myslek
 * @version 1.0
 *
 * Copyright (c) 2022 Aberystwyth University.
 * All rights reserved.
 */

public class PlayerChoiceController {

    private boolean isChoosingPlayer;
    private boolean isAttacking;
    private boolean pickedTreasure;
    private List<Player> playersList;
    private Player playerPicked;


    @FXML
    private Button choiceBtn1;

    @FXML
    private Button choiceBtn2;

    @FXML
    private Button choiceBtn3;

    @FXML
    private Label title;

    @FXML
    void choice1(ActionEvent event) {//TODO - send to card function method
        if (isChoosingPlayer) {
            playerPicked = playersList.get(0);
        }else if(isAttacking){
            isAttacking = true;
        }else{
            pickedTreasure = true;
        }
        // get a handle to the stage
        Stage stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        // close it
        stage.close();
    }

    @FXML
    void choice2(ActionEvent event) {//TODO - send to card function method
        if (isChoosingPlayer) {
            playerPicked = playersList.get(1);
        }
        // get a handle to the stage
        Stage stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        // close it
        stage.close();
    }

    @FXML
    void choice3(ActionEvent event) {//TODO - send to card function method
        if (isChoosingPlayer) {
            playerPicked = playersList.get(2);
        }else if(isAttacking){
            isAttacking = false;
        }else{
            pickedTreasure = false;
        }
        // get a handle to the stage
        Stage stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        // close it
        stage.close();
    }

    @FXML
    void closePopUp(ActionEvent event) {
        // get a handle to the stage
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        // close it
        stage.close();

    }

    public void setupChoicePopup(boolean validChoiceTreasure, boolean validChoiceCrewCard, Player currentPlayer, Player[] players, boolean isChoosingPlayer,boolean isAttacking) {
        this.isChoosingPlayer = isChoosingPlayer;
        this.isAttacking = isAttacking;
        List<Player> playersList = new ArrayList<>();
        playersList.addAll(List.of(players));
        playersList.remove(currentPlayer);
        if (isChoosingPlayer) {
            this.playersList = playersList;
            choiceBtn1.setText(playersList.get(0).getName());
            choiceBtn2.setText(playersList.get(1).getName());
            choiceBtn3.setText(playersList.get(2).getName());
        }
        else if(isAttacking){
            title.setText("Attack Player?");
            choiceBtn1.setText("Yes");
            choiceBtn2.setText("");
            choiceBtn2.setDisable(true);
            choiceBtn2.setVisible(false);
            choiceBtn3.setText("No");
        }
    else{
            choiceBtn1.setText("Treasure");
            choiceBtn2.setText("");
            choiceBtn2.setDisable(true);
            choiceBtn2.setVisible(false);
            choiceBtn3.setText("Crew Card");
            if(!(validChoiceTreasure)){
                choiceBtn1.setDisable(true);
            }else if(!(validChoiceCrewCard)){
                choiceBtn3.setDisable(true);
            }
    }
}
    public boolean isPickedTreasure() {
        System.out.println(pickedTreasure);
        return pickedTreasure;

    }
    public boolean isAttacking() {
        System.out.println(isAttacking);
        return isAttacking;
    }

    public Player getPlayerPicked() {
        System.out.println(playerPicked);
        return playerPicked;
    }
}

