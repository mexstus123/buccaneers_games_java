package game.ui.controllers.gameplay;

import game.models.board.port.Port;
import game.models.player.Orientation;
import game.models.player.Player;
import game.ui.components.Component;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Pair;

import java.util.HashSet;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Set;

/**
 * @(#) OrientationController.java 1.0 2022/05/06
 *
 * Class used for controlling the Orientation and Comapss
 *
 * @author Adrian Zlotkowski
 * @version 1.0
 *
 * Copyright (c) 2022 Aberystwyth University.
 * All rights reserved.
 */

public class OrientationController extends ComponentController{
    private Orientation orientation = Orientation.N;
    @FXML
    private ImageView compass;

    @FXML
    private ScrollBar scrollBar;

    @FXML
    private Button endTurnBtn;

    @FXML
    private Button TurnBtn;

    @FXML
    void endTurn(ActionEvent event) {
        if (getGameController().getGame().getPlayerManager().isSpecialMode()) {
            getGameController().getGame().getPlayerManager().setSpecialMode(false, null);
        } else {
            try {
                int choice = getGameController().getGame().endTurn();
                // ToDo: ask player something
                if (choice == 2 || choice == 20){
                    System.out.println("Choose a player");
                    Player playerChoosen = askPlayerPopup(event);
                    getGameController().getGame().setChosenPlayer(playerChoosen);
                }
                else {
                    System.out.println(choice);
                }
            } catch (Exception e) {
                // ToDo: no cards left pop up
                System.out.println("No treasure left");
            }

        }
        getGameController().getGame().forceEndTurn();
        getGameController().updateAllControllers();
//        scrollBar.setDisable(false);
//        TurnBtn.setDisable(false);
//        endTurnBtn.setDisable(true);
    }
    private void changeDirection(int o){
        orientation = orientation.getNext(o);
        validTurn();
    }

    private Player askPlayerPopup(ActionEvent event) {
        return null;
    }

    @FXML
    void turnShip(ActionEvent event) {
        getGameController().getGame().getPlayerManager().getCurrentPlayer().setOrientation(orientation);
        getGameController().updateAllControllers();
        scrollBar.setDisable(true);
        TurnBtn.setDisable(true);
        endTurnBtn.setDisable(false);
    }

    @FXML
    protected void onRotate() {
        changeDirection((int)scrollBar.getValue());
        Image image = new Image(Objects.requireNonNull(getClass().getResource("assets/directions/compass" + orientation + ".png")).toExternalForm());
        compass.setImage(image);
    }

    @FXML
    private void initialize(){
//        scrollBar.setDisable(false);
//        TurnBtn.setDisable(false);
//        endTurnBtn.setDisable(true);
    }


    @Override
    public void componentClicked(Component component) {

    }

    private void validTurn(){
        Set<Orientation> orientationSet = new HashSet<>();
        orientationSet.add(orientation);
        if(!(getGameController().getGame().getBoard().getPossibleMovesAtDirection(getGameController().getGame().getPlayerManager().getPlayers(),getGameController().getGame().getPlayerManager().getCurrentPlayer(), orientationSet).size() > 0)){
            TurnBtn.setDisable(true);
        }else{
            TurnBtn.setDisable(false);
        }
        orientationSet.clear();
    }


    public void update() {
        scrollBar.setDisable(false);
        this.orientation = getGameController().getGame().getPlayerManager().getCurrentPlayer().getOrientation();
        Image image = new Image(Objects.requireNonNull(getClass().getResource("assets/directions/compass" + orientation + ".png")).toExternalForm());
        compass.setImage(image);
        validTurn();
        Pair<Integer, Integer> coordinates = getGameController().getGame().getPlayerManager().getCurrentPlayer().getCoordinates();
        if(getGameController().getGame().getBoard().getBoardPiece(coordinates.getKey(), coordinates.getValue()) instanceof Port){
            TurnBtn.setDisable(true);
            endTurnBtn.setDisable(false);
            scrollBar.setDisable(true);
        }
//        scrollBar.setValue();
    }
}