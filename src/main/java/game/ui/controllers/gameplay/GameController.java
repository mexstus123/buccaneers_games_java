package game.ui.controllers.gameplay;

import game.models.app.GameManager;
import game.models.app.GameSetup;
import game.models.player.Orientation;
import javafx.fxml.FXML;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.util.Objects;

/**
 * @(#) GameController.java 1.0 2022/05/06
 *
 * Class representing the controller for the game
 *
 * @author Franciszek Myslek
 * @version 1.0
 *
 * Copyright (c) 2022 Aberystwyth University.
 * All rights reserved.
 */

public class GameController {

    @FXML
    private ImageView playerLayer;

    // Controller used for the board
    @FXML
    private BoardController boardController;

    // Controller used for players name and score board menu
    @FXML
    private PlayerNameController playerNameController;

    // Controller used for the compass and ending turn
    @FXML
    private OrientationController compassController;

    // Controller used for player's inventory
    @FXML
    private PlayerInventoryController playerInventoryController;

    // Model class representing the game
    private GameManager game;


    // Initializes all controllers
    @FXML
    private void initialize() {
        boardController.injectGameController(this);
        playerNameController.injectGameController(this);
        playerInventoryController.injectGameController(this);
        compassController.injectGameController(this);
    }

    public void loadGame(GameManager gameManager) {
        game = gameManager;
        playerNameController.update();
        playerInventoryController.update();
        compassController.update();
        boardController.renderBoard();


    }
    public void setPlayerDetails(GameSetup setup) {
        game = new GameManager(setup);
        boardController.renderBoard();
        playerNameController.update();
        playerInventoryController.update();
        compassController.update();
        updateGameView();
    }

    /**
     * Because the tiles are generated after stage.show(), I can't access their position with getLayoutX/Y()
     */
    public void startGame() {
        boardController.initializeDynamicComponents();
        updateAllControllers();
    }

    // Sends update signal to all controllers
    public void updateAllControllers() {
        playerNameController.update();
        playerInventoryController.update();
        compassController.update();
        boardController.update();
        updateGameView();
    }

    // Updates player background layer
    private void updateGameView() {
        String url = Objects.requireNonNull(getClass().getResource(
                "assets/playerLayer/" + getGame().getPlayerManager().getCurrentPlayer().getColor().toString().toLowerCase()
                        + "_player.png")).toExternalForm();
        playerLayer.setImage(new Image(url, 1280, 720, true, true));
    }

    /**
     * Gets the game object
     *
     * @return The current game object
     */
    public GameManager getGame() {
        return game;
    }
}