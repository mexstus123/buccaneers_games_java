package game.ui.controllers.gameplay;

import game.models.items.cards.Card;
import game.models.items.cards.chance.ChanceCard;
import game.models.items.cards.crew_cards.CrewCard;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;

/**
 * @(#) BoardController.java 1.0 2022/05/06
 *
 * Class used for controlling the Card Menu
 *
 * @author Michael Knowler-Davis
 * @version 1.0
 *
 * Copyright (c) 2022 Aberystwyth University.
 * All rights reserved.
 */

public class CardMenuController {

    private Stage stage;


    private int location = 0;
    private List<Card> playersHand;
    public void setPlayersHand(List<Card> playersHand){
        this.playersHand = new ArrayList<Card>();
        this.playersHand = playersHand;
        useButton.setVisible(false);
        setCardInfo(playersHand.get(location));
        //background.setStyle("-fx-background-color: transparent");
    }

    @FXML
    private ImageView leftBack;

    @FXML
    private ImageView rightBack;


    @FXML
    private AnchorPane background;

    @FXML
    private Button leftButton;

    @FXML
    private Button rightButton;

    @FXML
    private Rectangle bottomColour;

    @FXML
    private Label cardName;

    @FXML
    private Label mainText;

    @FXML
    private Button exit;

    @FXML
    private Button useButton;

    @FXML
    void useCard(ActionEvent event) {
        //TODO - pass card to board

        // get a handle to the stage
        stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        // close it
        stage.close();
    }

    @FXML
    void exitButtonPressed(ActionEvent event) {
        // get a handle to the stage
        stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        // close it
        stage.close();
    }

    @FXML
    void leftButtonPressed(ActionEvent event) {
        if(location != 0) {
            location -= 1;
            leftButton.setId("leftBtn");
            rightButton.setId("rightBtn");
            setCardInfo(playersHand.get(location));
            leftBack.setVisible(true);
            rightBack.setVisible(true);
        }
        if(location == 0){
            leftButton.setId("leftBtnGreyed");
            leftBack.setVisible(false);
        }
    }

    @FXML
    void rightButtonPressed(ActionEvent event) {
        if(location != playersHand.size() - 1){
            location += 1;
            rightButton.setId("rightBtn");
            leftButton.setId("leftBtn");
            setCardInfo(playersHand.get(location));
            rightBack.setVisible(true);
            leftBack.setVisible(true);

        }
        if(location == playersHand.size() - 1){
            System.out.println(playersHand.size());
            rightButton.setId("rightBtnGreyed");
            rightBack.setVisible(false);
        }
    }
    private void setCardInfo(Card card){
        if(card instanceof CrewCard) {
            cardName.setText("Crew Card");
            mainText.setText(String.valueOf(((CrewCard) playersHand.get(location)).getRank()));
            if (!((CrewCard) playersHand.get(location)).getIsBlack()) {
                bottomColour.setId("cardColourRed");
            } else {
                bottomColour.setId("cardColourBlack");
            }
        }else{
            bottomColour.setId("cardColourChance");
            cardName.setText("Chance Card");
            setUseButtonVisible((ChanceCard) playersHand.get(location));
            mainText.setText(((ChanceCard) playersHand.get(location)).getTextOnCard());
        }
    }
    private void setUseButtonVisible(ChanceCard card){
        useButton.setVisible(card.isUsable());
    }
}
