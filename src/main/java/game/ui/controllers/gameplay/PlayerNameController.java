package game.ui.controllers.gameplay;

import game.models.player.Player;
import game.ui.components.Component;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.util.Objects;

/**
 * @(#) PlayerNameContrller.java 1.0 2022/05/06
 *
 * Class used for controlling the Player Names
 *
 * @author Franciszek Myslek
 * @version 1.0
 *
 * Copyright (c) 2022 Aberystwyth University.
 * All rights reserved.
 */

public class PlayerNameController extends ComponentController {

    @FXML
    private ImageView helpButton;

    // Pane representing top part of the application
    @FXML
    private AnchorPane topPane;
    @FXML
    private StackPane playerNamePane;

    // Label representing player's name and background
    @FXML
    private Label playerName;
    @FXML
    private Rectangle playerNameBg;


    /**
     * Initialize the controller
     */
    @FXML
    public void initialize() {
        topPane.getStylesheets().add(Objects.requireNonNull(getClass().getResource("style/player-gui-style.css")).toExternalForm());
        helpButton.setPickOnBounds(true);
        helpButton.setOnMouseClicked((MouseEvent e) -> {
            try {
                showTutorial(e);
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        });
    }

    /**
     * Updates the data of current player
     */
    public void update() {
        Player currentPlayer = getGameController().getGame().getPlayerManager().getCurrentPlayer();
        playerName.setText(currentPlayer.getHomePort().getCity());
        playerNameBg.setFill(new Color(0, 0, 0, 0));
    }

    /**
     * Sends signal to controller that particular component was clicked
     *
     * @param component Component that was clicked
     */
    @Override
    public void componentClicked(Component component) {

    }

    /**
     * Method that loads scoreboard onto the screen
     *
     * @param actionEvent event that triggered the method
     */
    public void showScoreboard(ActionEvent actionEvent) throws IOException {
        Stage stage = new Stage();
        FXMLLoader fxmlLoader = new FXMLLoader(ScoreBoardController.class.getResource("view/score-board-view.fxml"));
        Parent start = fxmlLoader.load();
        Scene startScene = new Scene(start);
        startScene.getStylesheets().add(Objects.requireNonNull(ScoreBoardController.class.getResource("style/score-board-style.css")).toExternalForm());
        stage.setScene(startScene);
        ScoreBoardController controller = fxmlLoader.<ScoreBoardController>getController();
        controller.setup(getGameController().getGame().getPlayerManager().getPlayers());
        stage.initStyle(StageStyle.TRANSPARENT);
        Color c = new Color(0,0,0, 0.25);
        startScene.setFill(c);
        stage.centerOnScreen();
        //keep the popup centred
        stage.initModality(Modality.APPLICATION_MODAL);
        //get rid of borders
        // setting the owner to the names screen
        stage.initOwner(((Button) actionEvent.getSource()).getScene().getWindow());
        //show and wait to make the popup have to be closed before anything else is clickable
        stage.showAndWait();

        getGameController().updateAllControllers();
    }

    public void showTutorial(MouseEvent mouseEvent) throws IOException {
        Stage stage = new Stage();
        FXMLLoader fxmlLoader = new FXMLLoader(TutorialController.class.getResource("view/tutorial-view.fxml"));
        Parent start = fxmlLoader.load();
        Scene startScene = new Scene(start);
        startScene.getStylesheets().add(Objects.requireNonNull(TutorialController.class.getResource("style/tutorial-style.css")).toExternalForm());
        stage.setScene(startScene);
        stage.initStyle(StageStyle.TRANSPARENT);
        Color c = new Color(0,0,0, 0.25);
        startScene.setFill(c);
        stage.centerOnScreen();
        //keep the popup centred
        stage.initModality(Modality.APPLICATION_MODAL);
        //get rid of borders
        // setting the owner to the names screen
        stage.initOwner(((ImageView) mouseEvent.getSource()).getScene().getWindow());
        //show and wait to make the popup have to be closed before anything else is clickable
        stage.showAndWait();

        getGameController().updateAllControllers();
    }
}
