package game.ui.controllers.gameplay;

import game.models.player.Player;
import game.ui.controllers.setup.NamesScreenController;
import game.ui.controllers.setup.StartScreenController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;

/**
 * @(#) EndScreenContrller.java 1.0 2022/05/06
 *
 * Class used for controlling the End Screen
 *
 * @author Michael Knowler-Davis
 * @version 1.0
 *
 * Copyright (c) 2022 Aberystwyth University.
 * All rights reserved.
 */

public class EndScreenController {


    @FXML
    private Label name1;

    @FXML
    private Label city1;

    @FXML
    private Label name2;

    @FXML
    private Label city2;

    @FXML
    private Label name3;

    @FXML
    private Label city3;

    @FXML
    private Label name4;

    @FXML
    private Label city4;

    @FXML
    private Label turnCount;

    @FXML
    private Label timeElapsed;

    @FXML
    private Label nameOnRight1;

    @FXML
    private Label nameOnRight2;

    @FXML
    private Label nameOnRight3;

    @FXML
    private Label nameOnRight4;

    public void setupEndScreen(Player[] players){
        List<Player> playersList = new ArrayList<>();
        playersList.addAll(List.of(players));
        playersList.sort(Comparator.comparing(Player::getPortTotalValue).thenComparing(Comparator.comparing(Player::getPortTotalValue)));

        name1.setText(playersList.get(3).getName());
        nameOnRight1.setText(playersList.get(3).getName());
        city1.setText(playersList.get(3).getHomePort().getCity());
        name2.setText(playersList.get(2).getName());
        nameOnRight2.setText(playersList.get(2).getName());
        city2.setText(playersList.get(2).getHomePort().getCity());
        name3.setText(playersList.get(1).getName());
        nameOnRight3.setText(playersList.get(1).getName());
        city3.setText(playersList.get(1).getHomePort().getCity());
        name4.setText(playersList.get(0).getName());
        nameOnRight4.setText(playersList.get(0).getName());
        city4.setText(playersList.get(0).getHomePort().getCity());

    }

    @FXML
    void exitGame(ActionEvent event) {
        // get a handle to the stage
        Stage stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        // close it
        stage.close();
    }

    @FXML
    void playAgain(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(Objects.requireNonNull(NamesScreenController.class.getResource("view/player-screen-view.fxml")));
        root.getStylesheets().add(Objects.requireNonNull(NamesScreenController.class.getResource("style/player-screen-style.css")).toExternalForm());
        Stage stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.centerOnScreen();
        stage.show();
    }

}
