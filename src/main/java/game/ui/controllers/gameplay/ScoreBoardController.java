package game.ui.controllers.gameplay;

import game.models.items.TradableItem;
import game.models.items.treasure.Treasure;
import game.models.player.Player;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

/**
 * @(#) ScoreBoardController.java 1.0 2022/05/06
 *
 * Class used for controlling the ScoreBoard
 *
 * @author Michael Knowler-Davis
 * @version 1.0
 *
 * Copyright (c) 2022 Aberystwyth University.
 * All rights reserved.
 */

public class ScoreBoardController {

        @FXML
        private Label player1City;

        @FXML
        private Rectangle player1Colour;

        @FXML
        private Label player1Name;

        @FXML
        private Label player1Score;

        @FXML
        private Label player2City;

        @FXML
        private Label player2Name;

        @FXML
        private Label player2Score;

        @FXML
        private Rectangle player2Colour;

        @FXML
        private Label player3City;

        @FXML
        private Label player3Name;

        @FXML
        private Label player3Score;

        @FXML
        private Rectangle player3Colour;

        @FXML
        private Label player4City;

        @FXML
        private Label player4Name;

        @FXML
        private Label player4Score;

        @FXML
        private Rectangle player4Colour;

        @FXML
        void exitGame(ActionEvent event) {
            // get a handle to the stage
            Stage stage = (Stage)((Node)event.getSource()).getScene().getWindow();
            // close it
            stage.close();
        }
        public void setup(Player[] players){
            int value = 0;
            player1Name.setText(players[0].getName());
            player1City.setText(players[0].getHomePort().getCity());
            player1Colour.setFill(players[0].getColor().getJavafxColor());//TODO Get Frank to add colour to each player rect
            for (TradableItem t:players[0].getHomePort().getInventory()) {
                if (t instanceof Treasure treasure)
                    value += treasure.getValue();
            }
            for (Treasure t:players[0].getHomePort().getSafeInventory()) {
                value += t.getValue();
            }
            player1Score.setText(String.valueOf(value));
            value = 0;

            player2Name.setText(players[1].getName());
            player2City.setText(players[1].getHomePort().getCity());
            player2Colour.setFill(players[1].getColor().getJavafxColor());
            for (TradableItem t:players[1].getHomePort().getInventory()) {
                if (t instanceof Treasure treasure)
                    value += treasure.getValue();
            }
            for (Treasure t:players[1].getHomePort().getSafeInventory()) {
                value += t.getValue();
            }
            player2Score.setText(String.valueOf(value));
            value = 0;

            player3Name.setText(players[2].getName());
            player3City.setText(players[2].getHomePort().getCity());
            player3Colour.setFill(players[2].getColor().getJavafxColor());
            for (TradableItem t:players[2].getHomePort().getInventory()) {
                if (t instanceof Treasure treasure)
                    value += treasure.getValue();
            }
            for (Treasure t:players[2].getHomePort().getSafeInventory()) {
                value += t.getValue();
            }
            player3Score.setText(String.valueOf(value));
            value = 0;

            player4Name.setText(players[3].getName());
            player4City.setText(players[3].getHomePort().getCity());
            player4Colour.setFill(players[3].getColor().getJavafxColor());
            for (TradableItem t:players[3].getHomePort().getInventory()) {
                if (t instanceof Treasure treasure)
                    value += treasure.getValue();
            }
            for (Treasure t:players[3].getHomePort().getSafeInventory()) {
                value += t.getValue();
            }
            player4Score.setText(String.valueOf(value));
    }
}
