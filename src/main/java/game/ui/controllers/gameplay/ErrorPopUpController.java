package game.ui.controllers.gameplay;


import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.stage.Stage;

/**
 * @(#) ErrorPopUpController.java 1.0 2022/05/06
 *
 * Class used for showing an Error Pop-up
 *
 * @author Franciszek Myslek
 * @version 1.0
 *
 * Copyright (c) 2022 Aberystwyth University.
 * All rights reserved.
 */

public class ErrorPopUpController {



        @FXML
        private Label body;

        @FXML
        private Label title;

        @FXML
        void closePopUp(ActionEvent event) {
                Stage stage = (Stage)((Node)event.getSource()).getScene().getWindow();
                // close it
                stage.close();
        }
        public void setupError(String title,String body){
                this.body.setText(body);
                this.title.setText(title);
        }

    }
