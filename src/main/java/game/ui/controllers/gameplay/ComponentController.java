package game.ui.controllers.gameplay;

import game.ui.components.Component;

/**
 * @(#) ComponentController.java 1.0 2022/05/06
 *
 * Parent Class for each Component Controller
 *
 * @author Franciszek Myslek
 * @version 1.0
 *
 * Copyright (c) 2022 Aberystwyth University.
 * All rights reserved.
 */
public abstract class ComponentController {

    // Main game controller
    private GameController gameController;

    /**
     * Initializes the link between component controller and main game controller
     *
     * @param gameController Game controller that holds the game info
     */
    public void injectGameController(GameController gameController) throws RuntimeException {
        if (this.gameController == null)
            this.gameController = gameController;
        else
            throw new RuntimeException("Game controller already connected");
    }

    /**
     * Sends signal to controller that particular component was clicked
     *
     * @param component Component that was clicked
     */
    public abstract void componentClicked(Component component);

    /**
     * Gets the game controller
     *
     * @return Parent controller of the component controller
     */
    public GameController getGameController() {
        return gameController;
    }
}
