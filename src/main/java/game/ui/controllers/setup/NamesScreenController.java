package game.ui.controllers.setup;

import game.models.app.GameSetup;
import game.models.player.PlayerColor;
import game.models.player.PlayerSetup;
import game.ui.controllers.gameplay.GameController;
import game.ui.main.GameView;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.util.Objects;

/**
 * @(#) NamesScreenController.java 1.0 2022/05/06
 *
 * Class used for controlling the UI version of the board
 *
 * @author Michael Knowler-Davis
 * @version 1.0
 *
 * Copyright (c) 2022 Aberystwyth University.
 * All rights reserved.
 */

public class NamesScreenController {

    private Parent root;
    private Stage stage;
    private Scene scene;

    @FXML
    private TextField txtField1;

    @FXML
    private TextField txtField2;

    @FXML
    private TextField txtField3;

    @FXML
    private TextField txtField4;

    @FXML
    private Rectangle playerClr1;

    @FXML
    private Rectangle playerClr2;

    @FXML
    private Rectangle playerClr3;

    @FXML
    private Rectangle playerClr4;

    @FXML
    private Button plyer1Leftbtn;

    @FXML
    private Button plyer1Rightbtn;

    @FXML
    private Button plyer2Leftbtn;

    @FXML
    private Button plyer2Rightbtn;

    @FXML
    private Button plyer3Leftbtn;

    @FXML
    private Button plyer3Rightbtn;

    @FXML
    private Button plyer4Leftbtn;

    @FXML
    private Button plyer4Rightbtn;

    @FXML
    private Circle symbol1;

    @FXML
    private Circle symbol2;

    @FXML
    private Circle symbol3;

    @FXML
    private Circle symbol4;

    @FXML
    private Button rdyBtn1;

    @FXML
    private Button rdyBtn2;

    @FXML
    private Button rdyBtn3;

    @FXML
    private Button rdyBtn4;

    @FXML
    private Button startbtn;


    PlayerSetup playerSetup1 = new PlayerSetup(PlayerColor.GREEN,false);
    PlayerSetup playerSetup2 = new PlayerSetup(PlayerColor.BLUE,false);
    PlayerSetup playerSetup3 = new PlayerSetup(PlayerColor.RED,false);
    PlayerSetup playerSetup4 = new PlayerSetup(PlayerColor.PINK,false);

    private final PlayerSetup[] playerSetupList = {playerSetup1, playerSetup2, playerSetup3, playerSetup4};



    public void initializeButtons() {
        changeBothColors(rdyBtn1, playerClr1, playerSetup1.getColor());
        changeBothColors(rdyBtn2, playerClr2, playerSetup2.getColor());
        changeBothColors(rdyBtn3, playerClr3, playerSetup3.getColor());
        changeBothColors(rdyBtn4, playerClr4, playerSetup4.getColor());
    }

    private void changeBothColors(Button button, Rectangle rectangle, PlayerColor color) {
        button.setBackground(new Background(new BackgroundFill(color.getJavafxColor(), CornerRadii.EMPTY, Insets.EMPTY)));
        rectangle.setFill(color.getJavafxColor());
    }

    /**
     * This method error checks the players name input
     * @param txtField to get the user input
     * @param btnPressed for popup
     * @param currentPlayer to check name against other players
     * @return boolean - true if no errors found
     * @throws IOException checks for nulls
     */
    boolean textErrorCheck(TextField txtField, Button btnPressed, PlayerSetup currentPlayer) throws IOException {
        //getting text from text field
        String textToCheck = txtField.getText();
        //checking if no input has been made
        if(textToCheck.equals("") || textToCheck.length() >= 25){
            //display error
            displayErrorMessage(btnPressed);
            return false;
        }
        //check each character of the text using ascii
        for (int i = 0; i < textToCheck.length(); i++) {
            if(!((textToCheck.charAt(i) >= 48 && textToCheck.charAt(i) <= 57) || (textToCheck.charAt(i) >= 65 && textToCheck.charAt(i) <= 90) || (textToCheck.charAt(i) >= 97 && textToCheck.charAt(i) <= 122))){
                    displayErrorMessage(btnPressed);
                    return false;
            }
        }
        //checking the name against all other ready players
        if(!(sameNameCheck(playerSetupList,currentPlayer, textToCheck))) {
            displayErrorMessage(btnPressed);
            return false;
        }
        //if none of these conditions have been met then return true
        return true;
    }

    /**
     * checking one player against all other players
     * which are ready
     * @param players all players
     * @param playerToCheck Player to check
     * @param name name input
     * @return boolean - true if no same names
     */
    boolean sameNameCheck(PlayerSetup[] players, PlayerSetup playerToCheck, String name){
        //for all players
        for (PlayerSetup p: players) {
            //checking if player is ready, has the same name and not the player to check
            if(p.isReady() && p.getName().equals(name) && p != playerToCheck){
                return false;
            }
        }
        return true;
    }

    /**
     * method for when a ready button is pressed
     * This method is the main method for the names screen
     * @param event ready button pressed
     * @throws IOException null values
     */
    @FXML
    void toggleButton(ActionEvent event) throws IOException {//TODO - make this function less repetitive - HARD! buttons dont work in arrays
        //check if it was button 1 pressed
        if(event.getSource() == rdyBtn1){
            //if no errors in the name
            if(textErrorCheck(txtField1,(Button) event.getSource(), playerSetup1)) {
                //changing the text on button, colour of symbol and player ready boolean
                playerSetup1.changeButtonState(rdyBtn1, symbol1);
                //setting players name from user input
                playerSetup1.setName(txtField1.getText());
                //changing button to greyed or un-greyed
                changeButtonColour((Button) event.getSource());
                //making the text field un-editable when player is ready and editable when ready
                txtField1.setEditable(!playerSetup1.isReady());
            }
        }
        //repeated for player 2
        if(event.getSource() == rdyBtn2){
            if(textErrorCheck(txtField2,(Button) event.getSource(), playerSetup2)) {
                playerSetup2.changeButtonState(rdyBtn2, symbol2);
                playerSetup2.setName(txtField2.getText());
                changeButtonColour((Button) event.getSource());
                txtField2.setEditable(!playerSetup2.isReady());
            }
        }
        //repeated for player 3
        if(event.getSource() == rdyBtn3){
            if(textErrorCheck(txtField3,(Button) event.getSource(), playerSetup3)) {
                playerSetup3.changeButtonState(rdyBtn3, symbol3);
                playerSetup3.setName(txtField3.getText());
                changeButtonColour((Button) event.getSource());
                txtField3.setEditable(!playerSetup3.isReady());
            }
        }
        //repeated for player 4
        if(event.getSource() == rdyBtn4){
            if(textErrorCheck(txtField4,(Button) event.getSource(), playerSetup4)) {
                playerSetup4.changeButtonState(rdyBtn4, symbol4);
                playerSetup4.setName(txtField4.getText());
                changeButtonColour((Button) event.getSource());
                txtField4.setEditable(!playerSetup4.isReady());
            }
        }
        //when all players are in the ready state the start will be un-greyed
        if( playerSetup1.isReady() & playerSetup2.isReady() & playerSetup3.isReady() & playerSetup4.isReady()){
            startbtn.setId("start");
        }
        else{
            startbtn.setId("startGreyed");
        }

    }

    /**
     * displaying error popup if name input is incorrect
     * @param buttonPressed ready button
     * @throws IOException checking for nulls
     */
    void displayErrorMessage(Button buttonPressed) throws IOException {
        //setting up the stage
        Stage stage = new Stage();
        FXMLLoader fxmlLoader = new FXMLLoader(Objects.requireNonNull(getClass().getResource("view/same-name-pop-up-view.fxml")));
        Parent popUp = fxmlLoader.load();
        Scene popUpScene = new Scene(popUp);
        popUpScene.getStylesheets().add(Objects.requireNonNull(getClass().getResource("style/player-screen-style.css")).toExternalForm());
        stage.setScene(popUpScene);
        //keep the popup centred
        stage.initModality(Modality.APPLICATION_MODAL);
        //get rid of borders
        stage.initStyle(StageStyle.UNDECORATED);
        //setting the owner to the names screen
        stage.initOwner(buttonPressed.getScene().getWindow());
        //show and wait to make the popup have to be closed before anything else is clickable
        stage.showAndWait();
    }

    /**
     * closing error pop up once button is pressed
     * @param event pressing close button
     */
    @FXML
    void closePopUp(ActionEvent event) {
        // get a handle to the stage
        stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        // close it
        stage.close();
    }

    /**
     * changing stage to main game UI when the start
     * button is pressed with all players ready
     * @param event start button pressed
     * @throws IOException catching null
     */
    @FXML
    void startGame(ActionEvent event) throws IOException {
        //checking if the button is not grey i.e. all players are ready
        if(!(startbtn.getId().equals("startGreyed"))){
            FXMLLoader loader = new FXMLLoader(Objects.requireNonNull(GameView.class.getResource("view/game-view.fxml")));
            root = loader.load();
            root.getStylesheets().add(Objects.requireNonNull(GameView.class.getResource("style/game-style.css")).toExternalForm());
            GameController game = loader.getController();
            game.setPlayerDetails(new GameSetup(playerSetupList));
            stage = (Stage)((Node)event.getSource()).getScene().getWindow();
            scene = new Scene(root, 1280, 720);
            stage.setScene(scene);
            stage.show();
            game.startGame();
        }
    }

    /**
     * changes ready button id to make it grey
     * or un-greyed
     * @param btn ready buttons
     */
    void changeButtonColour(Button btn){
        btn.setDisable(!btn.isDisable());
        // ToDo: make darker
    }

    /**
     * checks for which button was pressed
     * and then changes colour accordingly
     * @param event left colour change button
     */
    @FXML
    void leftColourChange(ActionEvent event) {
        //if player 1's colour
        if(event.getSource() == plyer1Leftbtn){
            //change the colour of ready button and colour bar
            colourChangeLeft(rdyBtn1,playerClr1, playerSetupList, playerSetup1);
        }
        //if player 2's colour
        if(event.getSource() == plyer2Leftbtn){
            colourChangeLeft(rdyBtn2,playerClr2, playerSetupList, playerSetup2);
        }
        //if player 3's colour
        if(event.getSource() == plyer3Leftbtn){
            colourChangeLeft(rdyBtn3,playerClr3, playerSetupList, playerSetup3);
        }
        //if player 4's colour
        if(event.getSource() == plyer4Leftbtn){
            colourChangeLeft(rdyBtn4,playerClr4, playerSetupList, playerSetup4);
        }
    }


    private void colourChangeLeft(Button btn, Rectangle clrRect, PlayerSetup[] playerSetups, PlayerSetup playerSetupToChange){
        if (!playerSetupToChange.isReady()) {
            do {
                playerSetupToChange.setColor(playerSetupToChange.getColor().getPrevious());
//                if (playerSetupToChange.getColor() == -1) {
//                    playerSetupToChange.setColourNumb(rdyButtonColours.length - 1);
//                }
            } while (playerColourCheck(playerSetups, playerSetupToChange));

            changeBothColors(btn, clrRect, playerSetupToChange.getColor());
        }
    }

    private void colourChangeRight(Button btn, Rectangle clrRect, PlayerSetup[] playerSetups, PlayerSetup playerSetupToChange){
        if (!playerSetupToChange.isReady()) {
            do {
                playerSetupToChange.setColor(playerSetupToChange.getColor().getNext());
            } while (playerColourCheck(playerSetups, playerSetupToChange));

            changeBothColors(btn, clrRect, playerSetupToChange.getColor());
        }
    }

    private boolean playerColourCheck(PlayerSetup[] playerSetups, PlayerSetup playerSetupToCheck){
        for (PlayerSetup p: playerSetups) {
            if(p != playerSetupToCheck && p.getColor() == playerSetupToCheck.getColor()){
                return true;
            }
        }
        return false;
    }

    @FXML
    void rightColourChange(ActionEvent event) {
        if(event.getSource() == plyer1Rightbtn){
            colourChangeRight(rdyBtn1,playerClr1, playerSetupList, playerSetup1);
        }
        if(event.getSource() == plyer2Rightbtn){
            colourChangeRight(rdyBtn2,playerClr2, playerSetupList, playerSetup2);
        }
        if(event.getSource() == plyer3Rightbtn){
            colourChangeRight(rdyBtn3,playerClr3, playerSetupList, playerSetup3);
        }
        if(event.getSource() == plyer4Rightbtn){
            colourChangeRight(rdyBtn4,playerClr4, playerSetupList, playerSetup4);
        }
    }
}
