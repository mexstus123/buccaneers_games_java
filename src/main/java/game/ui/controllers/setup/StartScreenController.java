package game.ui.controllers.setup;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Objects;

/**
 * @(#) NamesScreenController.java 1.0 2022/05/06
 *
 * Class used for controlling the UI version of the board
 *
 * @author Michael Knowler-Davis
 * @version 1.0
 *
 * Copyright (c) 2022 Aberystwyth University.
 * All rights reserved.
 */

public class StartScreenController {
    private Stage stage;

    @FXML
    private Button btnQuit;

    @FXML
    private Button btnStart;

    /**
     * closes application once exit button is pressed
     *
     * @param event exit button pressed
     */
    @FXML
    void exitGame(ActionEvent event) {
        // get a handle to the stage
        stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        // close it
        stage.close();
    }

    /**
     * changes stage to player names screen once
     * the start button is pressed
     *
     * @param event strat button pressed
     * @throws IOException null
     */
    @FXML
    void toPlayerScreen(ActionEvent event) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(Objects.requireNonNull(getClass().getResource("view/player-screen-view.fxml")));
        Parent root = fxmlLoader.load();
        ((NamesScreenController) fxmlLoader.getController()).initializeButtons();
        root.getStylesheets().add(Objects.requireNonNull(getClass().getResource("style/player-screen-style.css")).toExternalForm());
        stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Scene scene = new Scene(root);

        stage.setScene(scene);
        stage.centerOnScreen();
        stage.show();
    }

}
