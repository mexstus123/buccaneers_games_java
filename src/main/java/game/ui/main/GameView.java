package game.ui.main;

import game.exceptions.RequirementAlreadySatisfied;
import game.models.app.GameManager;
import game.models.app.GameSetup;
import game.ui.controllers.gameplay.GameController;
import game.ui.controllers.setup.StartScreenController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.util.Objects;

/**
 * @(#) GameView.java 1.0 2022/05/06
 *
 * Main Class to start the UI and the Game
 *
 * @author Franciszek Myslek
 * @version 1.0
 *
 * Copyright (c) 2022 Aberystwyth University.
 * All rights reserved.
 */
public class GameView extends Application {

    /**
     * Starts the UI application
     * @param stage Canvas on which the app will be displayed
     * @throws IOException Thrown when the application is unable to find the resources necessary to start the game
     */
    @Override
    public void start(Stage stage) throws IOException {
        try {

            GameManager gameManager = new GameManager();
            gameManager.load();
            FXMLLoader loader = new FXMLLoader(Objects.requireNonNull(GameView.class.getResource("view/game-view.fxml")));
            Parent root = loader.load();
            root.getStylesheets().add(Objects.requireNonNull(GameView.class.getResource("style/game-style.css")).toExternalForm());
            GameController game = loader.getController();
            game.loadGame(gameManager);
            Scene scene = new Scene(root, 1280, 720);
            stage.setScene(scene);
            stage.setResizable(false);
            stage.show();
            game.startGame();
        } catch (IOException | RequirementAlreadySatisfied e) {
            FXMLLoader fxmlLoader = new FXMLLoader(StartScreenController.class.getResource("view/start-screen-view.fxml"));
            Scene scene = new Scene(fxmlLoader.load(), 1280, 720);
            scene.getStylesheets().add(Objects.requireNonNull(StartScreenController.class.getResource("style/start-screen-style.css")).toExternalForm());
            stage.setTitle("Buccaneer Board Game");
            stage.setScene(scene);
            stage.setResizable(false);
            stage.show();
        }
    }

    /**
     * Launches the application
     * @param args Parameters past to the program at the start
     */
    public static void main(String[] args) {
        launch();
    }
}