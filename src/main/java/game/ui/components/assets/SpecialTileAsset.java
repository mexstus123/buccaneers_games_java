package game.ui.components.assets;

/**
 * @(#) SpecialTileAsset.java 1.0 2022/05/06
 *
 * Class extending Asset representing a Special Tile asset
 *
 * @author Franciszek Myslek
 * @version 1.0
 *
 * Copyright (c) 2022 Aberystwyth University.
 * All rights reserved.
 */

public class SpecialTileAsset extends Asset {

    /**
     * Creates special tile asset and positions it on the AnchorPane
     *
     * @param top  Distance from the top of the AnchorPane
     * @param left Distance from the left of the AnchorPane
     */
    public SpecialTileAsset(double top, double left) {
        super(top, left);
    }
}
