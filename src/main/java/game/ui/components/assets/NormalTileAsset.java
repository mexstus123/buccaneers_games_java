package game.ui.components.assets;

/**
 * @(#) NormalTileAsset.java 1.0 2022/05/06
 *
 * Class extending Asset representing a Normal Tile asset
 *
 * @author Franciszek Myslek
 * @version 1.0
 *
 * Copyright (c) 2022 Aberystwyth University.
 * All rights reserved.
 */

public class NormalTileAsset extends Asset{
    /**
     * Creates asset and positions it on the AnchorPane
     *  @param top  Distance from the top of the AnchorPane
     *
     * @param left Distance from the left of the AnchorPane
     */
    protected NormalTileAsset(double top, double left) {
        super(top, left);
    }
}
