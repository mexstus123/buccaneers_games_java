package game.ui.components.assets;

import game.ui.controllers.gameplay.BoardController;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Rectangle;

/**
 * @(#) Asset.java 1.0 2022/05/06
 *
 * Class representing a graphical asset
 *
 * @author Franciszek Myslek
 * @version 1.0
 *
 * Copyright (c) 2022 Aberystwyth University.
 * All rights reserved.
 */
public abstract class Asset extends Rectangle {

    /**
     * ToDo: add id for different tiles (used for personalized styling)
     * Creates asset and positions it on the AnchorPane
     *
     * @param top  Distance from the top of the AnchorPane
     * @param left Distance from the left of the AnchorPane
     */
    protected Asset(double top, double left) {
        super(BoardController.TILE_SIZE, BoardController.TILE_SIZE);
        AnchorPane.setTopAnchor(this, top);
        AnchorPane.setLeftAnchor(this, left);
    }
}
