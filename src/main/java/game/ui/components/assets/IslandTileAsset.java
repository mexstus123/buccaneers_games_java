package game.ui.components.assets;

/**
 * @(#) IslandTileAsset.java 1.0 2022/05/06
 *
 * Class extending Asset representing a Island Tile asset
 *
 * @author Franciszek Myslek
 * @version 1.0
 *
 * Copyright (c) 2022 Aberystwyth University.
 * All rights reserved.
 */

public class IslandTileAsset extends Asset{
    /**
     * Creates island asset and positions it on the AnchorPane
     *
     * @param top  Distance from the top of the AnchorPane
     * @param left Distance from the left of the AnchorPane
     */
    public IslandTileAsset(double top, double left, int width, int height) {
        super(top, left);
        setWidth(getWidth() *  width);
        setHeight(getHeight() * height);
    }
}
