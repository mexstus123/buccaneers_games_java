package game.ui.components.tile;

import game.models.board.BoardPiece;
import game.models.board.port.Port;
import game.ui.controllers.gameplay.BoardController;
import game.ui.controllers.gameplay.ComponentController;
import javafx.scene.control.Tooltip;
import javafx.util.Duration;

/**
 * @(#) PortTile.java 1.0 2022/05/06
 *
 * Class extending Tile representing a Port Tile.
 *
 * @author Franciszek Myslek
 * @version 1.0
 *
 * Copyright (c) 2022 Aberystwyth University.
 * All rights reserved.
 */
public class PortTile extends Tile {

    /**
     * Creates a new blue tile
     *
     * @param componentController Parent controller of the component
     * @param rowIndex            X coordinate of the tile
     * @param columnIndex         Y coordinate of the tile
     * @param boardPiece          BoardPiece that is assigned to this tile
     */
    public PortTile(ComponentController componentController, int rowIndex, int columnIndex, BoardPiece boardPiece) {
        super(componentController, BoardController.TILE_SIZE, BoardController.TILE_SIZE,
                rowIndex, columnIndex, boardPiece);
        initializeTooltip();
    }

    // Creates a tooltip when hovering over the port
    private void initializeTooltip() {
        Tooltip tooltip = new Tooltip(((Port) getBoardPiece()).getCity());
        tooltip.setShowDelay(Duration.seconds(0.2));
        tooltip.setShowDuration(Duration.INDEFINITE);
        Tooltip.install(this, tooltip);
    }

}
