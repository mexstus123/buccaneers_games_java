package game.ui.components.tile;

import game.models.board.BoardPiece;
import game.ui.components.Component;
import game.ui.controllers.gameplay.ComponentController;
import javafx.scene.input.MouseEvent;

/**
 * @(#) Asset.java 1.0 2022/05/06
 *
 * Superclass representing a tile
 *
 * @author Franciszek Myslek
 * @version 1.0
 *
 * Copyright (c) 2022 Aberystwyth University.
 * All rights reserved.
 */
public abstract class Tile extends Component {

    // Position on the board - board[columnIndex][rowIndex]
    private final int columnIndex;
    private final int rowIndex;

    /**
     * Creates a new blue tile
     *
     * @param componentController Parent controller of the component
     * @param width               Width of the component on the screen
     * @param height              Height of the component on the screen
     * @param rowIndex            Y coordinate of the tile
     * @param columnIndex         X coordinate of the tile
     * @param boardPiece          BoardPiece that is assigned to this tile
     */
    protected Tile(ComponentController componentController, double width, double height, int columnIndex, int rowIndex,
                   BoardPiece boardPiece) {
        super(componentController, width, height, boardPiece);
        initializeMouseInteractions();

        if (columnIndex < 0 || rowIndex < 0)
            throw new IllegalArgumentException("Passed index less than 0");
        this.columnIndex = columnIndex;
        this.rowIndex = rowIndex;
    }

    /**
     * Gets the column index of the tile (position on the board)
     *
     * @return Column index of the tile relative to the board
     */
    public int getColumnIndex() {
        return columnIndex;
    }

    /**
     * Gets the row index of the tile (position on the board)
     *
     * @return Row index of the tile relative to the board
     */
    public int getRowIndex() {
        return rowIndex;
    }

    /*
     * Methods for interacting with the TileS
     */

    // Initializes mouse interactions with the tile
    private void initializeMouseInteractions() {
        this.addEventHandler(MouseEvent.MOUSE_CLICKED, this::tileClick);
    }

    // Registers signal and send it to the controller that the tile was clicked
    protected void tileClick(MouseEvent mouseEvent) {
        getController().componentClicked(this);
    }

    /**
     * Example method for interaction between board and tile.
     * Used for receiving signals from the controller.
     */
    public void sendInteractionSignal() {
        System.out.println("Tile {" + getColumnIndex() + ", " + getRowIndex() + "} clicked ");
    }

    /**
     * Light up the tile as possible movement
     */
    public void lightUp() {
        setId("possibleMove");
    }

    /**
     * Clears the tile to a factory setting
     */
    public void clear() {
        setId(null);
    }


    /**
     * Checks if given to tiles are equal
     * @param o Object to compare
     * @return Whether the tiles are equal
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Tile tile)) return false;

        if (getColumnIndex() != tile.getColumnIndex()) return false;
        return getRowIndex() == tile.getRowIndex();
    }
}
