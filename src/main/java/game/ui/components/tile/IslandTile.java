package game.ui.components.tile;

import game.models.board.BoardPiece;
import game.models.board.island.Island;
import game.ui.controllers.gameplay.BoardController;
import game.ui.controllers.gameplay.ComponentController;
import javafx.scene.control.Tooltip;
import javafx.util.Duration;

/**
 * @(#) IslandTile.java 1.0 2022/05/06
 *
 * Subclass of Tile representing a Island Tile on the board
 *
 * @author Franciszek Myslek
 * @version 1.0
 *
 * Copyright (c) 2022 Aberystwyth University.
 * All rights reserved.
 */
public class IslandTile extends Tile {

    /**
     * Creates a new blue tile
     *
     * @param componentController Parent controller of the component
     * @param rowIndex            X coordinate of the tile
     * @param columnIndex         Y coordinate of the tile
     * @param boardPiece          BoardPiece that is assigned to this tile
     */
    public IslandTile(ComponentController componentController, int rowIndex, int columnIndex, BoardPiece boardPiece) {
        super(componentController, BoardController.TILE_SIZE,
                BoardController.TILE_SIZE, rowIndex, columnIndex, boardPiece);
        setWidth((getWidth() * ((Island) boardPiece).getSizeX()));
        setHeight(getHeight() * ((Island) boardPiece).getSizeY());
        initializeTooltip();
    }

    // Creates a tooltip when hovering over the island
    private void initializeTooltip() {
        Tooltip tooltip = new Tooltip(((Island) getBoardPiece()).getName());
        tooltip.setShowDelay(Duration.seconds(0.2));
        tooltip.setShowDuration(Duration.INDEFINITE);
        Tooltip.install(this, tooltip);
    }
}
