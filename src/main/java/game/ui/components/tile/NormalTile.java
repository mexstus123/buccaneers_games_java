package game.ui.components.tile;

import game.models.board.BoardPiece;
import game.ui.controllers.gameplay.BoardController;
import game.ui.controllers.gameplay.ComponentController;

/**
 * @(#) NormalTile.java 1.0 2022/05/06
 *
 * Class extending Tile representing a normal tile
 *
 * @author Franciszek Myslek
 * @version 1.0
 *
 * Copyright (c) 2022 Aberystwyth University.
 * All rights reserved.
 */
public class NormalTile extends Tile {

    /**
     * Creates a new sea tile
     *
     * @param componentController Parent controller of the component
     * @param rowIndex            X coordinate of the tile
     * @param columnIndex         Y coordinate of the tile
     * @param boardPiece          BoardPiece that is assigned to this tile
     */
    public NormalTile(ComponentController componentController, int rowIndex, int columnIndex, BoardPiece boardPiece) {
        super(componentController, BoardController.TILE_SIZE, BoardController.TILE_SIZE, rowIndex, columnIndex, boardPiece);
    }
}
