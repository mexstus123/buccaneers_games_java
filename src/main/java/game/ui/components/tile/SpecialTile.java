package game.ui.components.tile;

import game.models.board.BoardPiece;
import game.models.board.tile.SpecialPlaceTile;
import game.ui.controllers.gameplay.BoardController;
import game.ui.controllers.gameplay.ComponentController;
import javafx.scene.control.Tooltip;
import javafx.util.Duration;

/**
 * @(#) SpecialTile.java 1.0 2022/05/06
 *
 * Class extending Tile representing Special Tiles.
 *
 * @author Franciszek Myslek
 * @version 1.0
 *
 * Copyright (c) 2022 Aberystwyth University.
 * All rights reserved.
 */
public class SpecialTile extends Tile {

    /**
     * Creates a new blue tile
     *
     * @param componentController Parent controller of the component
     * @param rowIndex            X coordinate of the tile
     * @param columnIndex         Y coordinate of the tile
     * @param boardPiece          BoardPiece that is assigned to this tile
     */
    public SpecialTile(ComponentController componentController, int rowIndex, int columnIndex, BoardPiece boardPiece) {
        super(componentController, BoardController.TILE_SIZE, BoardController.TILE_SIZE, rowIndex, columnIndex, boardPiece);
        initializeTooltip();
    }

    // Creates a tooltip when hovering over the tile
    private void initializeTooltip() {
        Tooltip tooltip = new Tooltip(((SpecialPlaceTile) getBoardPiece()).getName());
        tooltip.setShowDelay(Duration.seconds(0.2));
        tooltip.setShowDuration(Duration.INDEFINITE);
        Tooltip.install(this, tooltip);
    }
}
