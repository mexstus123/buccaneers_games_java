package game.ui.components;

import game.models.board.BoardPiece;
import game.ui.controllers.gameplay.ComponentController;
import javafx.scene.shape.Rectangle;

/**
 * Parent class for all components e.g. tiles, ships
 *
 * @author Franciszek Myslek
 */
public abstract class Component extends Rectangle {

    // Parent controller of the component
    private final ComponentController controller;

    // Model class representing given component
    private final BoardPiece boardPiece;

    /**
     * Initializes the component and the connection with the controller
     *
     * @param componentController Parent controller of the component
     * @param width               Width of the component on the screen
     * @param height              Height of the component on the screen
     * @param boardPiece          BoardPiece that is assigned to this tile
     */
    protected Component(ComponentController componentController, double width, double height, BoardPiece boardPiece) {
        super(width, height);
        controller = componentController;
        this.boardPiece = boardPiece;
    }

    /**
     * Get the controller for the component
     *
     * @return The parent controller of the component
     */
    public ComponentController getController() {
        return controller;
    }

    /**
     * Get the model class representing the component
     *
     * @return The model class representing the component
     */
    public BoardPiece getBoardPiece() {
        return boardPiece;
    }
}
