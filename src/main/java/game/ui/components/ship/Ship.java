package game.ui.components.ship;

import game.models.player.Player;
import game.ui.components.Component;
import game.ui.controllers.gameplay.BoardController;
import game.ui.controllers.gameplay.ComponentController;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.ImagePattern;
import javafx.util.Pair;

import java.util.Objects;

/**
 * @(#) Ship.java 1.0 2022/05/06
 *
 * Class representing a player's ship on the board
 *
 * @author Franciszek Myslek
 * @version 1.0
 *
 * Copyright (c) 2022 Aberystwyth University.
 * All rights reserved.
 */
public class Ship extends Component {

    // Temporary size
    private static final double size = BoardController.TILE_SIZE;
    // Duration of movement animation for moving one tile (in millis)
    private final int animationTime = 1000;
    // Current coordinates of the ship
    private Pair<Integer, Integer> coordinates;

    /**
     * Creates a ship and the connection with the controller
     *
     * @param componentController Parent controller of the component
     * @param player              BoardPiece that is assigned to this tile
     */
    public Ship(ComponentController componentController, Player player) {
        super(componentController, size, size, player);
        this.coordinates = getBoardPiece().getCoordinates();
        setShipImage();
        reposition();
    }

    public void setShipImage() {
        // Set image
        String url = "assets/" + ((Player) getBoardPiece()).getColor().toString().toLowerCase() + "/"
                + ((Player) getBoardPiece()).getColor().toString().toLowerCase() + "_"
                + ((Player) getBoardPiece()).getOrientation() + ".png";
        setFill(new ImagePattern(new Image(Objects.requireNonNull(getClass().getResource(url)).toExternalForm())));
    }

    /**
     * Moves the ship to the correct tile according to coordinates in the model
     */
    public void sail() {
        Pair<Integer, Integer> newCoordinates = getBoardPiece().getCoordinates();
//        Path path = new Path();
//        path.getElements().add(new MoveTo(400, 400));
//        PathTransition pathTransition = new PathTransition(Duration.millis(animationTime), path, this);
//        pathTransition.setAutoReverse(false);
//        pathTransition.play();
        setCoordinates(newCoordinates);
        reposition();

    }

    /**
     * Gets the current coordinates of the ship on the board
     *
     * @return Current coordinates on the board
     */
    public Pair<Integer, Integer> getCoordinates() {
        return coordinates;
    }

    // Sets the coordinates of the ship
    private void setCoordinates(Pair<Integer, Integer> newCoordinates) {
        this.coordinates = newCoordinates;
    }

    // Repositions the ship to the correct place
    private void reposition() {
        AnchorPane.setTopAnchor(this,
                ((BoardController) getController()).getTile(getCoordinates()).getLayoutY());
        AnchorPane.setLeftAnchor(this,
                ((BoardController) getController()).getTile(getCoordinates()).getLayoutX());
    }
}
