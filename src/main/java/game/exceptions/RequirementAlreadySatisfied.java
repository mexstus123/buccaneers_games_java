package game.exceptions;

/**
 * Exception thrown when the specified requirement is already satisfied.
 * Occurs when trying to overwrite something that can be changed only once
 *
 * @author Franciszek Myslek [frm20]
 * @version 30th April 2022
 * @(#) RequirementAlreadySatisfied.java 2022/05/03
 *
 * Copyright (c) 2022 Aberystwyth University.
 * All rights reserved.
 */

public class RequirementAlreadySatisfied extends Exception {

    /**
     * Creates the exception with a specified message
     *
     * @param message Displayed when error is thrown
     */
    public RequirementAlreadySatisfied(String message) {
        super(message);
    }
}
