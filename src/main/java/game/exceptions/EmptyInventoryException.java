/*
 *
 *  @(#) SomeClass.java ?.? 2022/05/??
 *
 *  Copyright (c) 2022 Aberystwyth University.
 *  All rights reserved.
 *
 * /
 */

package game.exceptions;
/**
 * Exception thrown when the player has an empty inventory
 *
 * @author AAAA AAAAAA [AAA1]
 * @version AAAA
 * @(#) EmptyInventoryException.java 2022/05/04
 *
 * Copyright (c) 2022 Aberystwyth University.
 * All rights reserved.
 */

public class EmptyInventoryException extends Exception {
    public EmptyInventoryException(String message){
        super(message);
    }
}
