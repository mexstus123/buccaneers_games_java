package game.exceptions;

/**
 * Exception thrown when the player hasn't interacted with the game enough to do the intended action
 *
 * @author Franciszek Myslek [frm20]
 * @version 30th April 2022
 * @(#) InsufficientPlayerInteraction.java 2022/05/03
 *
 * Copyright (c) 2022 Aberystwyth University.
 * All rights reserved.
 */

public class InsufficientPlayerInteraction extends Exception {

    /**
     * Creates the exception with a specified message
     *
     * @param message Displayed when error is thrown
     */
    public InsufficientPlayerInteraction(String message) {
        super(message);
    }
}
