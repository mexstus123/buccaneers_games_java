package game.exceptions;

/**
 * Exception thrown when End of Game is detected
 *
 * @(#) EndGameDetected.java 1.1 2022/05/06
 *
 * @author
 * @version
 *
 * Copyright (c) 2022 Aberystwyth University.
 * All rights reserved.
 *
 */

public class EndGameDetected extends Exception {
    public EndGameDetected(String message){
        super(message);
    }
}
