package game.models.app;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import game.models.board.port.Port;
import game.models.board.port.TradePort;
import game.models.items.TradableItem;
import game.models.items.cards.chance.ChanceCard;
import game.models.items.cards.crew_cards.CrewCard;

import java.io.IOException;
import java.util.List;

/**
 * Custom serializer for the manager class
 *
 * @(#) GameManagerSerializer.java 2022/05/04
 * @author Adrian Zlotkowski [adz11]
 * @version 1.0
 *
 * Copyright (c) 2022 Aberystwyth University.
 * All rights reserved.
 */
public class GameManagerSerializer extends StdSerializer<GameManager> {


    protected GameManagerSerializer(Class<GameManager> t) {
        super(t);
    }


    /**
     * Custom serialize method
     *
     * @param gameManager
     * @param jsonGenerator
     * @param serializerProvider
     * @throws IOException
     */
    @Override
    public void serialize(GameManager gameManager, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeNumberField("crewCardsSize", gameManager.getCrewCardsDeck().size());
        jsonGenerator.writeNumberField("chanceCardsSize", gameManager.getChanceDeck().size());
        TradePort venice = (TradePort) gameManager.getBoard().getBoardPiece(1, 7);
        Port london = (Port) gameManager.getBoard().getBoardPiece(1, 14);
        Port cadiz = (Port) gameManager.getBoard().getBoardPiece(14, 20);
        TradePort amsterdam = (TradePort) gameManager.getBoard().getBoardPiece(20, 14);
        Port marseilles = (Port) gameManager.getBoard().getBoardPiece(20, 7);
        Port genoa = (Port) gameManager.getBoard().getBoardPiece(7, 1);

        List<CrewCard> crewList = (List<CrewCard>) gameManager.getCrewCardsDeck();
        List<ChanceCard> chanceCards = (List<ChanceCard>) gameManager.getChanceDeck();

        jsonGenerator.writeNumberField("veniceSize", venice.getInventory().size());
        for (int i = 0; i < venice.getInventory().size(); i++) {
            TradableItem tradableItem = venice.getInventory().get(i);
            if (tradableItem instanceof CrewCard) {
                jsonGenerator.writeStringField("venice" + i + "type", "Crew Card");
                jsonGenerator.writeBooleanField("venice" + i + "boolean", ((CrewCard) tradableItem).getIsBlack());
                jsonGenerator.writeNumberField("venice" + i + "rank", tradableItem.getValue());
            } else {
                jsonGenerator.writeStringField("venice" + i + "type", tradableItem.getType());
                jsonGenerator.writeNumberField("venice" + i + "value", tradableItem.getValue());
            }
        }
        jsonGenerator.writeNumberField("londonSize", london.getInventory().size());
        for (int i = 0; i < london.getInventory().size(); i++) {
            jsonGenerator.writeNumberField("london" + i, london.getInventory().get(i).getValue());
        }
        jsonGenerator.writeNumberField("cadizSize", cadiz.getInventory().size());
        for (int i = 0; i < cadiz.getInventory().size(); i++) {
            jsonGenerator.writeNumberField("cadiz" + i, cadiz.getInventory().get(i).getValue());
        }
        jsonGenerator.writeNumberField("amsterdamSize", amsterdam.getInventory().size());
        for (int i = 0; i < amsterdam.getInventory().size(); i++) {
            TradableItem tradableItem = amsterdam.getInventory().get(i);
            if (tradableItem instanceof CrewCard) {
                jsonGenerator.writeStringField("amsterdam" + i + "type", "Crew Card");
                jsonGenerator.writeBooleanField("amsterdam" + i + "boolean", ((CrewCard) tradableItem).getIsBlack());
                jsonGenerator.writeNumberField("amsterdam" + i + "rank", tradableItem.getValue());
            } else
                jsonGenerator.writeStringField("amsterdam" + i + "type", tradableItem.getType());
            jsonGenerator.writeNumberField("amsterdam" + i + "value", tradableItem.getValue());
        }
        jsonGenerator.writeNumberField("marseillesSize", marseilles.getInventory().size());
        for (int i = 0; i < marseilles.getInventory().size(); i++) {
            jsonGenerator.writeNumberField("marseilles" + i, marseilles.getInventory().get(i).getValue());
        }
        jsonGenerator.writeNumberField("genoaSize", genoa.getInventory().size());
        for (int i = 0; i < genoa.getInventory().size(); i++) {
            jsonGenerator.writeNumberField("genoa" + i, genoa.getInventory().get(i).getValue());
        }
        for (int i = 0; i < crewList.size(); i++) {
            jsonGenerator.writeNumberField("crewCard" + i + "rank", crewList.get(i).getRank());
            jsonGenerator.writeBooleanField("crewCard" + i + "boolean", crewList.get(i).getIsBlack());
        }
        for (int i = 0; i < chanceCards.size(); i++) {
            jsonGenerator.writeNumberField("chanceCard" + i, chanceCards.get(i).getCardNumber());

        }
    }
}
