package game.models.app;

import game.models.board.port.HomePort;
import game.models.board.port.HomePortName;
import game.models.board.port.Port;
import game.models.board.port.TradePort;
import game.models.items.TradableItem;
import game.models.items.cards.chance.ChanceCard;
import game.models.items.cards.crew_cards.CrewCard;
import game.models.items.treasure.Treasure;
import game.models.player.Player;
import game.models.player.PlayerSetup;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

/**
 * Class that is responsible to set up the game with provided players information
 *
 * @author Michael Knowler-Davies [mik46], edited by Franciszek Myslek [frm20]
 * @version 3rd May 2022
 * @(#) GameSetup.java 2022/05/03
 *
 * Copyright (c) 2022 Aberystwyth University.
 * All rights reserved.
 */
public class GameSetup {

    private final long seed = System.currentTimeMillis();
    //setting up ports
    private final HomePort london = new HomePort(1, 14, HomePortName.LONDON, 5);
    private final HomePort genoa = new HomePort(7, 1, HomePortName.GENOA, 6);
    private final HomePort marseilles = new HomePort(20, 7, HomePortName.MARSEILLES, 7);
    private final HomePort cadiz = new HomePort(14, 20, HomePortName.CADIZ, 8);

    private final TradePort amsterdam = new TradePort(20, 14, "Amsterdam", 10);
    private final TradePort venice = new TradePort(1, 7, "Venice", 9);

    private final Port[] ports = {london, genoa, marseilles, cadiz, venice, amsterdam};

    //creating the players
    private final Player player1 = new Player(null, london, null);
    private final Player player2 = new Player(null, genoa, null);
    private final Player player3 = new Player(null, marseilles, null);
    private final Player player4 = new Player(null, cadiz, null);

    private final Player[] players = {player1, player2, player3, player4};
    private final ArrayList<CrewCard> crewDeck = new ArrayList<>();
    private ArrayList<ChanceCard> chanceDeck = new ArrayList<>();
    private ArrayList<Treasure> islandTreasure = new ArrayList<>();



    /**
     * This method sets up the game port & players
     * using most of the other methods inside this class
     *
     * @param playersInfo players selections from names screen
     */
    public GameSetup(PlayerSetup[] playersInfo) {
        //assigning ports to each player
        assignRandomPorts(playersInfo);
        //setting up & randomizing both decks
        setupCrewDeck();
        setupChanceDeck();
        setIslandTreasure();
        //giving the starting cards and treasure to each player
        playerHandSetUp(players);
        //setting up the trading ports by giving them cards & treasure
        tradingPortsSetUp(new TradePort[]{venice, amsterdam});
    }

    public ArrayList<Treasure> getIslandTreasure() {
        return islandTreasure;
    }


    /**
     * Sets up the starting treasure and cards of the trading ports
     *
     * @param tradePorts all trading ports
     */
    private void tradingPortsSetUp(TradePort[] tradePorts) {
        //add 2 crew cards to each trading port
        for (int i = 0; i < 2; i++) {
            //go through each trading port twice
            for (TradePort t : tradePorts) {
                //add a card from the deck to the trading port
                t.addTradableItem(crewDeck.get(crewDeck.size() - 1));
                // remove that same card from the deck
                crewDeck.remove(crewDeck.get(crewDeck.size() - 1));
            }
        }
        //adding treasure to each trading port depending on crew cards in port
        for (TradePort t : tradePorts) {
            //initialize the value of inventory to 0
            int inventoryValue = 0;
            //calculate the current value of trading port from the crew cards value
            for (TradableItem i : t.getInventory()) {
                inventoryValue += i.getValue();
            }
            //initializing the Treasure to value 2 as 2 two treasures are needed to be added when both crew cards are of value 1
            Treasure treasure = new Treasure("", 2);
            //if the crew cards are both of value 1
            if (8 - inventoryValue == 6) {
                //add treasure value 2
                t.addTradableItem(treasure);
                //create a new valued treasure to add
                treasure = new Treasure("", 4);

            } else {
                //change the treasure to the value which would make the inventory value to 8
                treasure = new Treasure("", 8 - inventoryValue);
            }
            //add the treasure to port
            t.addTradableItem(treasure);
        }
    }

    /**
     * Returns the players created by users
     *
     * @return Array of players created on start up
     */
    public Player[] getPlayers() {
        return players;
    }

    /**
     * Returns correctly setup ports
     *
     * @return Array of ports created on start up
     */
    public Port[] getPorts() {
        return ports;
    }


    public ArrayList<CrewCard> getCrewDeck() {
        return crewDeck;
    }

    public ArrayList<ChanceCard> getChanceDeck() {
        return chanceDeck;
    }

    /**
     * Giving the cards from the deck to players
     *
     * @param players all players
     */
    private void playerHandSetUp(Player[] players) {
        //giving each player 5 cards from the deck
        for (int i = 0; i < 5; i++) {
            //for each player 5 times
            for (Player p : players) {
                //add a crew card from the deck
                p.addCard(crewDeck.get(crewDeck.size() - 1));
                //remove the same card
                crewDeck.remove(crewDeck.get(crewDeck.size() - 1));
            }
        }
    }

    /**
     * Randomizing the players order and assigning them to ports/player classes
     *
     * @param playersDetails players
     */
    private void assignRandomPorts(PlayerSetup[] playersDetails) {
        //randomizing the players in the array
        Collections.shuffle(Arrays.asList(playersDetails));
        //for each player
        for (int i = 0; i < playersDetails.length; i++) {
            //get the player info input from names screen and assign it to a player class
            players[i].setName(playersDetails[i].getName());
            players[i].setColor(playersDetails[i].getColor());
        }
    }

    /**
     * Adding crew cards to the crew card deck
     */
    private void setupCrewDeck() {
        //nested for loops to create each different type of crew card 6 times

        for (int i = 1; i <= 2; i++) { //for colour
            for (int j = 1; j <= 3; j++) { //for strength
                for (int k = 0; k < 6; k++) {//making 6 of each
                    //creating each card and adding it to the deck
                    CrewCard crewCard = new CrewCard("Crew Card", j, i);
                    crewDeck.add(crewCard);
                }
            }
        }
        //shuffling the crew card deck - makes random
        Collections.shuffle(crewDeck, new Random(seed));
    }

    /**
     * Adding chance cards to the chance card deck
     */
    private void setupChanceDeck() {
        //making all 28 of the chance cards and adding to the deck and then shuffling
        for (int i = 1; i < 29; i++) {
            ChanceCard chanceCard = new ChanceCard("Chance Card", i);
            chanceDeck.add(chanceCard);
        }
        Collections.shuffle(chanceDeck, new Random(seed));
    }

    private void setIslandTreasure(){
        for (int i = 2; i < 7; i++) {
            for (int j = 0; j < 4; j++) {
                Treasure t = new Treasure("",i);
                islandTreasure.add(t);
            }
        }
        Collections.shuffle(islandTreasure, new Random(seed));
    }

}
