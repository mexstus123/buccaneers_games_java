package game.models.app;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import game.models.board.Board;
import game.models.board.port.HomePort;
import game.models.board.port.HomePortName;
import game.models.board.port.Port;
import game.models.board.port.TradePort;
import game.models.items.TradableItem;
import game.models.items.cards.chance.ChanceCard;
import game.models.items.cards.crew_cards.CrewCard;
import game.models.items.treasure.Treasure;

import java.io.IOException;
import java.util.LinkedList;
import java.util.Queue;

/**
 * Custom deserializer for the GameManager class
 *
 * @author Adrian Zlotkowski [adz11]
 * @version 1.0
 * <p>
 * Copyright (c) 2022 Aberystwyth University.
 * All rights reserved
 * @(#) GameManagerDeserializer.java 2022/05/04
 */
public class GameManagerDeserializer extends StdDeserializer<GameManager> {

    public GameManagerDeserializer
            () {
        this((JavaType) null);
    }

    protected GameManagerDeserializer(Class<?> vc) {
        super(vc);
    }

    protected GameManagerDeserializer(JavaType valueType) {
        super(valueType);
    }

    protected GameManagerDeserializer(StdDeserializer<?> src) {
        super(src);
    }

    /**
     * @param jsonParser
     * @param deserializationContext
     * @return
     * @throws IOException
     * @throws JacksonException
     */
    @Override
    public GameManager deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JacksonException {
        JsonNode node = jsonParser.getCodec().readTree(jsonParser);
        int crewCardsSize = (int) node.get("crewCardsSize").numberValue();
        int chanceCardsSize = (int) node.get("chanceCardsSize").numberValue();
        Queue<ChanceCard> chanceCards = new LinkedList<>();
        Queue<CrewCard> crewCards = new LinkedList<>();
        int veniceSize = (int) node.get("veniceSize").numberValue();
        int londonSize = (int) node.get("londonSize").numberValue();
        int cadizSize = (int) node.get("cadizSize").numberValue();
        int amsterdamSize = (int) node.get("amsterdamSize").numberValue();
        int marseillesSize = (int) node.get("marseillesSize").numberValue();
        int genoaSize = (int) node.get("genoaSize").numberValue();
        HomePort london = new HomePort(1, 14, HomePortName.LONDON, 5);
        HomePort genoa = new HomePort(7, 1, HomePortName.GENOA, 6);
        HomePort marseilles = new HomePort(20, 7, HomePortName.MARSEILLES, 7);
        HomePort cadiz = new HomePort(14, 20, HomePortName.CADIZ, 8);
        TradePort venice = new TradePort(1, 7, "venice", 9);
        TradePort amsterdam = new TradePort(20, 14, "amsterdam", 10);
        for (int i = 0; i < veniceSize; i++) {
            String type = node.get("venice" + i + "type").textValue();
            if (type.equals("Crew Card")) {
                int bool = 0;
                boolean isBlack = node.get("venice" + i + "boolean").booleanValue();
                if (!isBlack) {
                    bool = 1;
                } else bool = 0;

                venice.addTradableItem(new CrewCard("Crew Card", (int) node.get("venice" + i + "rank").numberValue(), bool));
            } else if (type.equals("Chance Card")) {
                venice.addTradableItem(new ChanceCard(type, (int) node.get("venice" + i + "value").numberValue()));
            } else {
                venice.addTradableItem(new Treasure(type, (int) node.get("venice" + i + "value").numberValue()));
            }
        }
        for (int i = 0; i < londonSize; i++) {
            london.addTradableItem(new TradableItem("Crew Card", (int) node.get("london" + i).numberValue()));
        }
        for (int i = 0; i < cadizSize; i++) {
            cadiz.addTradableItem(new TradableItem("Crew Card", (int) node.get("cadiz" + i).numberValue()));
        }
        for (int i = 0; i < amsterdamSize; i++) {
            String type = node.get("amsterdam" + i + "type").textValue();
            if (type.equals("Crew Card")) {
                int bool = 0;
                boolean isBlack = node.get("amsterdam" + i + "boolean").booleanValue();
                if (!isBlack) {
                    bool = 1;
                } else bool = 0;

                amsterdam.addTradableItem(new CrewCard("Crew Card", (int) node.get("amsterdam" + i + "rank").numberValue(), bool));
            } else if (type.equals("Chance Card")) {
                venice.addTradableItem(new ChanceCard(type, (int) node.get("amsterdam" + i + "value").numberValue()));
            } else if (type.equals("")) {
                venice.addTradableItem(new Treasure(type, (int) node.get("amsterdam" + i + "value").numberValue()));
            }
        }
        for (int i = 0; i < marseillesSize; i++) {
            marseilles.addTradableItem(new TradableItem("Crew Card", (int) node.get("marseilles" + i).numberValue()));
        }
        for (int i = 0; i < genoaSize; i++) {
            genoa.addTradableItem(new TradableItem("Crew Card", (int) node.get("genoa" + i).numberValue()));
        }

        Port[] ports = {london, genoa, marseilles, cadiz, venice, amsterdam};

        for (int i = 0; i < chanceCardsSize; i++) {
            chanceCards.add(new ChanceCard("Crew Card", (int) node.get("chanceCard" + i).numberValue()));
        }
        for (int i = 0; i < crewCardsSize; i++) {
            //ToDO cast boolean to int
            crewCards.add(new CrewCard("Crew Card", (int) node.get("crewCard" + i + "rank").numberValue(), 1));
        }
        GameManager gameManager = new GameManager();
        Board board = new Board(ports);
        gameManager.setCrewCardsDeck(crewCards);
        gameManager.setChanceDeck(chanceCards);
        gameManager.setBoard(board);
        return gameManager;

    }
}
