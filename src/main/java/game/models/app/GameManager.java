package game.models.app;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import game.exceptions.InsufficientPlayerInteraction;
import game.exceptions.RequirementAlreadySatisfied;
import game.models.board.Board;
import game.models.board.BoardPiece;
import game.models.board.island.FlatIsland;
import game.models.board.island.PirateIsland;
import game.models.board.island.TreasureIsland;
import game.models.board.port.HomePort;
import game.models.items.cards.Card;
import game.models.items.cards.chance.ChanceCard;
import game.models.items.cards.chance.ChanceCardController;
import game.models.items.cards.crew_cards.CrewCard;
import game.models.items.treasure.Treasure;
import game.models.player.Player;
import game.models.player.PlayerDeserializer;
import game.models.player.PlayerManager;
import game.models.player.PlayerSerializer;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.*;

/**
 * Main class for running the game
 *
 * @author Franciszek Myslek [frm20]
 * @version 3rd May 2022
 * @(#) GameManager.java 2022/05/03
 *
 * Copyright (c) 2022 Aberystwyth University.
 * All rights reserved.
 */
public class GameManager {
    // Board holding all the board pieces
    private  Board board;

    // Manager for interacting with the players
    private  PlayerManager playerManager;
    private  Queue<CrewCard> crewCardsDeck;
    private Queue<ChanceCard> chanceDeck;

    private Player chosenPlayer;
    private int drawnCard;

    /**
     * Constructor for JSON
     */
    public GameManager() {
        playerManager = null;
        board = null;
    }

    /**
     * Initializes all parts of the game
     */
    public GameManager(GameSetup gameSetup) {
        board = new Board(gameSetup.getPorts());
        playerManager = new PlayerManager(gameSetup.getPlayers());
        ((TreasureIsland) board.getBoardPiece(9, 9)).setChanceDeck(new LinkedList<>(gameSetup.getChanceDeck()));
        ((TreasureIsland) board.getBoardPiece(9, 9)).setInventory(new LinkedList<>(gameSetup.getIslandTreasure()));
        crewCardsDeck = new LinkedList<>(gameSetup.getCrewDeck());
        chanceDeck = new LinkedList<>(gameSetup.getChanceDeck());
    }

    public void moveCurrentPlayer(int x, int y) {
        getPlayerManager().moveCurrentPlayer(x, y);
    }

    /**
     * Gets the board with all board pieces
     *
     * @return Board that the game is played on
     */
    public Board getBoard() {
        return board;
    }

    public void setChosenPlayer(Player chosenPlayer) {
        this.chosenPlayer = chosenPlayer;
        ChanceCardController c = new ChanceCardController();
        c.execute(drawnCard, board, ((TreasureIsland) board.getBoardPiece(9, 9)), ((PirateIsland) board.getBoardPiece(17, 2)), ((FlatIsland) board.getBoardPiece(2, 16)), getPlayerManager().getPlayers(), getPlayerManager().getCurrentPlayer(), getPlayerManager().getCurrentPlayer().getOrientation(), false, chosenPlayer);
        this.chosenPlayer = null;
        this.drawnCard = -1;
    }

    public void anchorBayExecution() {
        ChanceCardController c = new ChanceCardController();
        c.execute(drawnCard, board, ((TreasureIsland) board.getBoardPiece(9, 9)), ((PirateIsland) board.getBoardPiece(17, 2)), ((FlatIsland) board.getBoardPiece(2, 16)), getPlayerManager().getPlayers(), getPlayerManager().getCurrentPlayer(), getPlayerManager().getCurrentPlayer().getOrientation(), false, getPlayerManager().getCurrentPlayer());
    }

    /**
     * Gets a list of board pieces that are possible moves for the current player
     *
     * @return List with possible moves
     */
    public List<BoardPiece> getCurrentPlayerPossibleMoves() {
        return !getPlayerManager().isCurrentPlayerMovedThisTurn() ?
                getBoard().getPossibleMoves(playerManager.getPlayers(), playerManager.getCurrentPlayer(), getPlayerManager().isSpecialMode())
                : new ArrayList<>();
    }

    /**
     * Returns the player manager
     *
     * @return Player manager
     */
    public PlayerManager getPlayerManager() {
        return playerManager;
    }

    public enum TurnState {
        TRADE_PORT, HOME_PORT, ANCHOR_BAY, TREASURE_ISLAND, PIRATE_ISLAND, FLAT_ISLAND,
        ATTACK,
        END_TURN,
        END_GAME
    }

    // Get turn state from board
    public TurnState getTurnState() {
        return TurnState.ATTACK;
    }

    public void turnStateController(TurnState turnState) {
        switch (turnState) {
            // UI Related
            case TRADE_PORT -> System.out.println("Initiate Trade");
            case HOME_PORT -> System.out.println("Initiate Home");
            case TREASURE_ISLAND -> System.out.println("Initiate Treasure");
            case ATTACK -> System.out.println("Initiate Attack");
            case END_TURN -> System.out.println("Initiate End");
            case END_GAME -> System.out.println("Initiate End");

            // Computation and Transaction
            case ANCHOR_BAY -> System.out.println("Initiate Anchor");
            case PIRATE_ISLAND -> System.out.println("Initiate Pirate");
            case FLAT_ISLAND -> System.out.println("Initiate Flat");
        }
    }

    public int endTurn() throws IOException {
        int x = getPlayerManager().getCurrentPlayer().getX();
        int y = getPlayerManager().getCurrentPlayer().getY();
        ChanceCardController controller = new ChanceCardController();
        if(Objects.equals(getPlayerManager().getCurrentPlayer().getHomePort().getCity(), "Cadiz")){
            save();
        }

        //ISLANDS

        if (board.getAdjacentIslandID(x, y) > 13 && board.getAdjacentIslandID(x, y) < 17) {
            int pos = board.getAdjacentIslandID(x, y);
            switch (pos) {
                case 14:
                    //Flat Island(2,16)
                    //Withdraw all contents from Flat Island
                    FlatIsland flatIsland = (FlatIsland) board.getBoardPiece(2, 16);

                    List<Card> tmpCardList = ((FlatIsland) board.getBoardPiece(2, 16)).withdrawCards();
                    List<Treasure> tmpTreasureList = ((FlatIsland) board.getBoardPiece(2, 16)).withdrawTreasure(getPlayerManager().getCurrentPlayer().getRoomForTreasure());

                    for (Card c : tmpCardList) {
                        getPlayerManager().getCurrentPlayer().addCard(c);
                    }

                    for (Treasure t : tmpTreasureList) {
                        getPlayerManager().getCurrentPlayer().addTreasure(t);
                    }

                    return Board.ISLAND_FLAT;
                case 16:
                    //Treasure Island
                    //Deal the Player a chance card
                    ChanceCard card = ((TreasureIsland) board.getBoardPiece(9, 9)).dealCard();
                    int cardNumber = card.getCardNumber();
                    System.out.println(cardNumber);
                    Player chosenPlayer;

                    if (cardNumber > 23 && cardNumber < 27) {
                        getPlayerManager().getCurrentPlayer().addCard(card);
                        System.out.println("Drew a card");
                        return Board.ISLAND_TREASURE;
                    } else {
                        if (cardNumber == 2 || cardNumber == 20) {
                            drawnCard = cardNumber;
                            System.out.println("Drew a card");
                            return -1;
                        } else {
                            //Execute card
                            controller.execute(cardNumber, board, ((TreasureIsland) board.getBoardPiece(9, 9)), ((PirateIsland) board.getBoardPiece(17, 2)), ((FlatIsland) board.getBoardPiece(2, 16)), getPlayerManager().getPlayers(), getPlayerManager().getCurrentPlayer(), getPlayerManager().getCurrentPlayer().getOrientation(), false, getPlayerManager().getCurrentPlayer());
                        }
                    }
                case 15:
                    //Pirate Island
                    //No interaction

                    return Board.ISLAND_PIRATE;
            }
        }

        //Bays

        else if (board.getStaticTiles(x, y) == 12) {
            for (Card c : getPlayerManager().getCurrentPlayer().getHand()) {
                if (((ChanceCard) c).getCardNumber() == 25 || ((ChanceCard) c).getCardNumber() == 26) {
                    controller.execute(((ChanceCard) c).getCardNumber(), board, ((TreasureIsland) board.getBoardPiece(9, 9)), ((PirateIsland) board.getBoardPiece(17, 2)), ((FlatIsland) board.getBoardPiece(2, 16)), getPlayerManager().getPlayers(), getPlayerManager().getCurrentPlayer(), getPlayerManager().getCurrentPlayer().getOrientation(), false, getPlayerManager().getCurrentPlayer());
                }
            }
        }
        return 0;
    }

    public void forceEndTurn() {
        try {
            getPlayerManager().endPlayerTurn();
        } catch (InsufficientPlayerInteraction e) {
            throw new RuntimeException(e);
        }
    }

    public void setBoard(Board board) {
        this.board = board;
    }

    public void setPlayerManager(PlayerManager playerManager) {
        this.playerManager = playerManager;
    }

    public Queue<CrewCard> getCrewCardsDeck() {
        return crewCardsDeck;
    }

    public void setCrewCardsDeck(Queue<CrewCard> crewCardsDeck) {
        this.crewCardsDeck = crewCardsDeck;
    }

    public Queue<ChanceCard> getChanceDeck() {
        return chanceDeck;
    }

    public void setChanceDeck(Queue<ChanceCard> chanceDeck) {
        this.chanceDeck = chanceDeck;
    }

    public void save() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addSerializer(Player.class, new PlayerSerializer(null));
        mapper.registerModule(module);
        for (int i = 1; i <= 4; i++) {
            mapper.writeValue(Paths.get("player" + i).toFile(), playerManager.getPlayer(i - 1));
        }
        module.addSerializer(GameManager.class, new GameManagerSerializer(null));
        mapper.writeValue(Paths.get("gameManager").toFile(), this);
    }

    public void load() throws IOException, RequirementAlreadySatisfied {
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(GameManager.class, new GameManagerDeserializer());
        mapper.registerModule(module);
        GameManager tempGameManager;
        tempGameManager = mapper.readValue(Paths.get("gameManager").toFile(), GameManager.class);
        module.addDeserializer(Player.class, new PlayerDeserializer());
        mapper.registerModule(module);
        Player[] players = new Player[4];
        for(int i = 1; i <= players.length; i ++){
            players[i-1]= mapper.readValue(Paths.get("player" + i).toFile(), Player.class);
        }
        HomePort london = (HomePort) tempGameManager.getBoard().getBoardPiece(1,14);
        london.setPlayer(players[0]);
        HomePort genoa = (HomePort) tempGameManager.getBoard().getBoardPiece(7,1);
        genoa.setPlayer(players[1]);
        HomePort marseilles = (HomePort) tempGameManager.getBoard().getBoardPiece(20,7);
        marseilles.setPlayer(players[2]);
        HomePort cadiz = (HomePort) tempGameManager.getBoard().getBoardPiece(14,20);
        cadiz.setPlayer(players[3]);
        this.setBoard(tempGameManager.getBoard());
        this.setPlayerManager(new PlayerManager(players));
        this.setChanceDeck(tempGameManager.getChanceDeck());
        this.setCrewCardsDeck(tempGameManager.getCrewCardsDeck());

    }
}
