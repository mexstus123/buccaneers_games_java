package game.models.player;

import javafx.scene.paint.Color;

/**
 * ToDo Add Class Functionality
 *
 * @author AAAA AAAAAA [AAA1]
 * @version AAAA
 * @(#) PlayerColor.java 2022/11/11
 *
 * Copyright (c) 2022 Aberystwyth University.
 * All rights reserved.
 */

public enum PlayerColor {
    GREEN(Color.rgb(123, 255, 8), 0), BLUE(Color.rgb(112, 183, 255), 1),
    RED(Color.rgb(255, 79, 67), 2), PINK(Color.rgb(248, 123, 255), 3),
    ORANGE(Color.rgb(255, 161, 38), 4), YELLOW(Color.rgb(245, 255, 64), 5);

    private static final PlayerColor[] values = values();
    private final Color javafxColor;
    private final int id;

    PlayerColor(Color javafxColor, int id) {
        this.javafxColor = javafxColor;
        this.id = id;
    }

    public Color getJavafxColor() {
        return javafxColor;
    }

    public PlayerColor getNext() {
        return values[(getId() + 1) % values.length];
    }

    public PlayerColor getPrevious() {
        return values[(getId() - 1 + values.length) % values.length];
    }

    private int getId() {
        return id;
    }
}

