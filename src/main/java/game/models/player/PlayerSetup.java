package game.models.player;

import javafx.scene.control.Button;
import javafx.scene.shape.Circle;

/**
 * Setup class for players, holds players data before starting the game
 *
 * @author mik46
 * @version 00th MONTH 2022
 * @(#) PlayerSetup.java 2022/22/22
 * <p>
 * Copyright (c) 2022 Aberystwyth University.
 * All rights reserved.
 */

public class PlayerSetup {

    private PlayerColor color;
    private String name;
    private boolean isReady;

    public PlayerSetup(PlayerColor colour, boolean isReady) {
        this.color = colour;
        name = "";
        this.isReady = isReady;
    }

    public void setColor(PlayerColor colourNumb) {
        this.color = colourNumb;
    }

    public PlayerColor getColor() {
        return color;
    }

    public void setReady(boolean ready) {
        isReady = ready;
    }
    public boolean isReady() {
        return isReady;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }


    public void changeButtonState(Button btn,Circle symbol){
        if(!isReady){
            btn.setText("Unready");
            isReady = true;
            symbol.setId("symbolGreen");
        }else{
            btn.setText("Ready");
            isReady = false;
            symbol.setId("symbolRed");
        }
    }

    @Override
    public String toString() {
        return "PlayerSetup{" +
                "colourNumb=" + color +
                ", name='" + name + '\'' +
                ", isReady=" + isReady +
                '}';
    }
}


