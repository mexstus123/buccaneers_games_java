package game.models.player;

import game.exceptions.RequirementAlreadySatisfied;
import game.models.board.port.HomePortName;
import game.exceptions.InsufficientPlayerInteraction;
import game.models.items.treasure.Treasure;

/**
 * Class that is responsible for managing players, their data, and switching between them
 *
 * @author Franciszek Myslek
 * @version 30th April 2022
 *
 * Copyright (c) 2022 Aberystwyth University.
 * All rights reserved.
 */
public class PlayerManager {

    /**
     * Number of players in the game
     */
    public static final int NUM_PLAYERS = 4;

    // Players currently in the game
    private final Player[] players;

    // Index of the current player
    private HomePortName currentPlayer = HomePortName.LONDON;

    private boolean currentPlayerMovedThisTurn;

    private boolean specialMode;
    private Player specialPlayer = null;

    /**
     * Initializes the player manager
     */
    public PlayerManager(Player[] players) {
        this.players = players;
        setPlayersOrientations();
        setHomePorts();
        currentPlayerMovedThisTurn = false;
        specialMode = false;
    }

    // Initializes players orientations
    private void setPlayersOrientations() {
        if (players[0].getOrientation() == null) {
            players[0].setOrientation(Orientation.E);
            players[1].setOrientation(Orientation.N);
            players[2].setOrientation(Orientation.W);
            players[3].setOrientation(Orientation.S);
        }
    }

    private void setHomePorts() {
        try {
            players[0].getHomePort().setPlayer(players[0]);
            players[1].getHomePort().setPlayer(players[1]);
            players[2].getHomePort().setPlayer(players[2]);
            players[3].getHomePort().setPlayer(players[3]);
        } catch (RequirementAlreadySatisfied e) {
            throw new RuntimeException(e);
        }
    }

    public void setSpecialMode(boolean mode, Player player) {
        this.specialMode = mode;
        this.specialPlayer = player;
    }

    public boolean isSpecialMode() {
        return specialMode;
    }

    /**
     * Gets the current player
     *
     * @return Player whose turn it is
     */
    public Player getCurrentPlayer() {
        return isSpecialMode() ? specialPlayer : getPlayer(currentPlayer);
    }

    /**
     * Ends the current player's turn if possible
     */
    public void endPlayerTurn() throws InsufficientPlayerInteraction {
        if (!canEndTurn())
            throw new InsufficientPlayerInteraction("Current player cannot end the turn yet");
        // ToDo: End player's turn and switch to the next one
        currentPlayerMovedThisTurn = false;
        currentPlayer = currentPlayer.getNext();
    }

    /**
     * Checks if current player can end his turn
     *
     * @return Whether the current player can end the turn or not
     */
    public boolean canEndTurn() {
        // ToDo: check if the player can end the turn
        return true;
    }

    /**
     * Records a move signal for a current player
     *
     * @param newX New X coordinate which the player wants to sail to
     * @param newY New Y coordinate which the player wants to sail to
     */
    public void moveCurrentPlayer(int newX, int newY) {
        if (!currentPlayerMovedThisTurn) {
            getCurrentPlayer().move(newX, newY);
            currentPlayerMovedThisTurn = true;
        } else if (isSpecialMode()) {
            getCurrentPlayer().move(newX, newY);
        }
    }


    public boolean isCurrentPlayerMovedThisTurn() {
        return !isSpecialMode() && currentPlayerMovedThisTurn;
    }

    /**
     * Get player with specified index, players are sorted in a way of taking turns
     *
     * @param index Index of the player in the order of taking turns (starts from 0)
     * @return Player on the correct index or null
     */
    public Player getPlayer(int index) {
        return index >= 0 && index < NUM_PLAYERS ? players[index] : null;
    }

    /**
     * Get player with specified home port name
     *
     * @param homePortName Name of the home port
     * @return Player on the correct index
     */
    public Player getPlayer(HomePortName homePortName) {
        return players[homePortName.getTurnIndex()];
    }

    /**
     * Returns an array of players sorted in the order of taking turns
     *
     * @return An array of players (size = NUM_PLAYERS)
     */
    public Player[] getPlayers() {
        return players;
    }
}
