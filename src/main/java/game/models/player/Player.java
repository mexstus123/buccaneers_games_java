package game.models.player;

import game.models.board.BoardPiece;
import game.models.board.port.HomePort;
import game.models.items.cards.Card;
import game.models.items.cards.chance.ChanceCard;
import game.models.items.cards.crew_cards.CrewCard;
import game.models.items.treasure.Treasure;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * ToDo Add Class Functionality
 *
 * @author AAAA AAAAAA [AAA1]
 * @version AAAA
 * @(#) Player.java 2022/11/11
 *
 * Copyright (c) 2022 Aberystwyth University.
 * All rights reserved.
 */

public class Player extends BoardPiece {
    private final HomePort homePort;
    private String name;
    private PlayerColor color;
    private final List<Card> hand;
    private List<Treasure> inventory;
    private Orientation direction;


    public Player(String name, HomePort homePort, PlayerColor color) {
        super(homePort.getX(), homePort.getY(), homePort.getTileID() - 4);
        this.name = name;
        this.color = color;
        this.homePort = homePort;

        hand = new ArrayList<>();
        inventory = new ArrayList<>();
    }

    public int getPortTotalValue() {
        return getHomePort().returnPortValue();
    }

    public void move(int x, int y) {
        setCoordinates(x, y);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public HomePort getHomePort() {
        return homePort;
    }

    public PlayerColor getColor() {
        return color;
    }

    public void setColor(PlayerColor color) {
        this.color = color;
    }

    public int getRedCrewCards() {
        int totalRedCardValue = 0;
        for (Card c : hand) {
            if (!((CrewCard) c).getIsBlack()){
                totalRedCardValue += ((CrewCard) c).getRank();
            }
        }
        return totalRedCardValue;
    }
    public int getBlackCrewCards() {
        int totalBlackCardValue = 0;
        for (Card c : hand) {
            if (((CrewCard) c).getIsBlack()){
                totalBlackCardValue += ((CrewCard) c).getRank();
            }
        }
        return totalBlackCardValue;
    }
    public int getTotalInventoryValue(){
        int totalValue =+ getCrewSize();
        for (Treasure t : inventory) {
            totalValue =+ t.getValue();
        }
        return totalValue;
    }

    public List<Card> getHand() {
        return hand;
    }

    public List<Treasure> getInventory() {
        return inventory;
    }

    public void setInventory(List<Treasure> inventory) {
        this.inventory = inventory;
    }
    public void addTreasure(Treasure treasure) {
        inventory.add(treasure);
    }

    /**
     * Methods for adding and withdrawing Cards from a player's hand
     *
     * @param card
     */

    public void addCard(Card card) {
        hand.add(card);
    }

    /**
     * DOES NOT WORK YET PLACEHOLDER, DONT CALL VERY BROKEN
     *
     * @param card
     * @return
     */
    public Card withdrawCard(Card card) {
        for (Card c : hand) {
            if (card.getValue() == c.getValue()) {
                hand.remove(c);
                break;
            }
        }
        return card;
    }


    @Override
    public String toString() {
        return "Player{" +
                "name='" + name + '\'' +
                ", assignedPort=" + getHomePort().getCity() + ", playerColour=" + getColor() +
                '}';
    }

    public List<Treasure> getTreasure() {
        return inventory;
    }

    public List<Card> getCrewHand() {
        return hand;
    }

    public void setOrientation(Orientation direction) {
        this.direction = direction;
    }

    public int getCrewSize() {
        return hand.size();
    }

    public int getCrewCount() {
        return hand.size();
    }

    public int getPlayerStrength() {
        return Math.abs(getBlackCrewCards() - getRedCrewCards());
    }

    public int getRoomForTreasure() {
        return 2 - inventory.size();
    }

    public boolean isContainTreasure(Treasure treasure) {
        return inventory.contains(treasure);
    }

    public void removeTreasure(Treasure treasure) {
        if (isContainTreasure(treasure)) {
            inventory.remove(treasure);
        }
    }

    public boolean withdrawCrewCard(Card card) {
        return hand.remove(card);
    }

    public boolean withdrawChanceCard(ChanceCard card) {
        return hand.remove(card);
    }

    public void addCrewCard(Card card) {
        hand.add(card);
    }

    public void addChanceCard(ChanceCard card) {
        hand.add(card);
    }

    public void sortCrewByAsc() {
        //Crew.CrewValueComparator crewComparator = new Crew.CrewValueComparator();
        //hand.sort(crewComparator);
        hand.sort(Comparator.comparing(Card::getValue).thenComparing(Comparator.comparing(Card::getValue)));
    }

    public void sortCrewByDsc() {
        hand.sort(Comparator.comparing(Card::getValue).thenComparing(Comparator.comparing(Card::getValue).reversed()));
    }

    public void sortTreasureByAsc() {
        inventory.sort(Comparator.comparing(Treasure::getValue).thenComparing(Comparator.comparing(Treasure::getValue)));
    }

    public int getTotalDistance() {
        int sumValue = 0;
        for (Card c: hand) {
            sumValue += c.getValue();
        }
        return sumValue;
    }

    public Orientation getOrientation() {return direction; }

    /*public void setInventory(List<TradableItem> newInventory){
        this.inventory = newInventory;
    }

    public void depositTradableItem(TradableItem tradableItem){
        inventory.add(tradableItem);
    }*///Unsure if needed cus changing inventory systems

    public Treasure withdrawTreasure(Treasure treasure){
        for (Treasure t : inventory) {
            if(treasure.getType().equals(t.getType())){
                inventory.remove(t);
                break;
            }
        }
        return treasure;
    }
    public List<Treasure> unloadShip(){
        List<Treasure> unloadedInventory= new ArrayList<>(inventory);
        inventory.clear();
        return unloadedInventory;
    }

}
