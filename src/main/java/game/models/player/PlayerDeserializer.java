package game.models.player;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import game.models.board.port.HomePort;
import game.models.board.port.HomePortName;
import game.models.items.cards.Card;
import game.models.items.cards.crew_cards.CrewCard;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Custom deserializer for the player class
 *
 * @author Adrian Zlotkowski [adz11]
 * @version 1.0
 *
 * Copyright (c) 2022 Aberystwyth University.
 * All rights reserved.
 */
public class PlayerDeserializer extends StdDeserializer<Player> {

    /**
     * Custom constructor
     */
    public PlayerDeserializer() {
        this((JavaType) null);
    }

    protected PlayerDeserializer(Class<?> vc) {
        super(vc);
    }

    protected PlayerDeserializer(JavaType valueType) {
        super(valueType);
    }

    protected PlayerDeserializer(StdDeserializer<?> src) {
        super(src);
    }

    @Override
    public Player deserialize(JsonParser jp, DeserializationContext ctxt)
            throws IOException, JsonProcessingException {
        JsonNode node = jp.getCodec().readTree(jp);
        String name = node.get("name").textValue();
        String city = node.get("city").textValue();
        int portX = (int) node.get("portX").numberValue();
        int portY = (int) node.get("portY").numberValue();
        int positionX = (int) node.get("positionX").numberValue();
        int positionY = (int) node.get("positionY").numberValue();
        int crewSize = (int) node.get("crewSize").numberValue();
        String colour = node.get("colour").textValue();
        String orientation = node.get("orientation").textValue();
        Orientation o = Orientation.valueOf(orientation);
        HomePort homeport = new HomePort(portX, portY, HomePortName.valueOf(city), 1);
        PlayerColor color = PlayerColor.BLUE;
        List<Card> crewCards = new ArrayList<>();
        Player player = new Player(name, homeport, color);
        player.setOrientation(o);
        player.setCoordinates(positionX, positionY);
        player.setColor(PlayerColor.valueOf(colour));
        int bool = 0;
        for (int i = 0; i < crewSize; i++) {
            boolean isBlack = node.get("crewCard" + i + "boolean").booleanValue();
            if (!isBlack) {
                bool = 1;
            } else bool = 0;
            player.addCard(new CrewCard("Crew Card",(int) node.get("crewCard" + i + "rank").numberValue(), bool));

        }

        return player;
    }
}
