package game.models.player;

/**
 * ToDo Add Class Functionality
 *
 * @author AAAA AAAAAA [AAA1]
 * @version AAAA
 * @(#) Orientation.java 2022/11/11
 *
 * Copyright (c) 2022 Aberystwyth University.
 * All rights reserved.
 */

public enum Orientation {
    N, NE, E, SE, S, SW, W, NW;

    public Orientation getNext(int position) {
        return values()[position % values().length];
    }
}
