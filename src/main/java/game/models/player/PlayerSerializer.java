package game.models.player;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import game.models.items.cards.Card;
import game.models.items.cards.crew_cards.CrewCard;

import java.io.IOException;
import java.util.List;

/**
 * Custom serializer for the player class
 *
 * @author Adrian Zlotkowski [adz11]
 * @version 1.0
 */
public class PlayerSerializer extends StdSerializer<Player> {

    /**
     * Custom constructor
     */
    public PlayerSerializer() {
        this((JavaType) null);

    }

    public PlayerSerializer(Class<Player> t) {
        super(t);
    }

    protected PlayerSerializer(JavaType type) {
        super(type);
    }

    protected PlayerSerializer(Class<?> t, boolean dummy) {
        super(t, dummy);
    }

    protected PlayerSerializer(StdSerializer<?> src) {
        super(src);
    }

    @Override
    public void serialize(Player player, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeStringField("name", player.getName());
        jsonGenerator.writeNumberField("positionY", player.getY());
        jsonGenerator.writeNumberField("positionX", player.getX());
        jsonGenerator.writeStringField("city", player.getHomePort().getType().toString());
        jsonGenerator.writeNumberField("portX", player.getHomePort().getX());
        jsonGenerator.writeNumberField("portY", player.getHomePort().getY());
        for (int i = 0; i < player.getHomePort().getSafeInventory().size(); i++) {
            jsonGenerator.writeNumberField("treasure" + i, player.getHomePort().getSafeInventory().get(i).getValue());
        }
        jsonGenerator.writeNumberField("crewSize", player.getCrewSize());
        int crewHandSize = player.getCrewSize();
        for (int i = 0; i < crewHandSize; i++) {
            CrewCard crewCard = player.getCrewHand().get(i) instanceof CrewCard ? ((CrewCard) player.getCrewHand().get(i)) : null;
            if(crewCard!= null){
                jsonGenerator.writeNumberField("crewCard" + i + "rank", crewCard.getRank());
                jsonGenerator.writeBooleanField("crewCard" + i + "boolean", crewCard.getIsBlack());
            }
        }
        jsonGenerator.writeStringField("orientation", player.getOrientation().toString());
        jsonGenerator.writeStringField("colour", player.getColor().name());
        jsonGenerator.writeEndObject();

    }
}
