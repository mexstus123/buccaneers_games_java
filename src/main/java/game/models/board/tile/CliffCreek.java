package game.models.board.tile;

/**
 * Subclass for cliff creecks. The only method is the constructor and it sets the position, name and tileID.
 *
 * @author AAAA AAAAA []
 * @version 30th April 2022
 * @(#) CliffCreek.java 2022/22/22
 *
 * Copyright (c) 2022 Aberystwyth University.
 * All rights reserved.
 */
public class CliffCreek extends SpecialPlaceTile {

    public CliffCreek(int x, int y) {
        super(x, y, "Cliff Creek", 13);
    }
}
