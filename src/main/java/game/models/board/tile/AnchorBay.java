package game.models.board.tile;

/**
 * Subclass for anchor bays. The only method is the constructor and it sets the position, name and tileID.
 *
 * @author AAAA AAAAA []
 * @version 30th April 2022
 * @(#) AnchorBay.java 2022/22/22
 *
 * Copyright (c) 2022 Aberystwyth University.
 * All rights reserved.
 */
public class AnchorBay extends SpecialPlaceTile {

    public AnchorBay(int x, int y) {
        super(x, y, "Anchor Bay", 12);
    }
}
