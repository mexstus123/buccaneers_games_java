package game.models.board.tile;

/**
 * Subclass for mud bays. The only method is the constructor and it sets the position, name and tileID.
 *
 * @author AAAA AAAAA []
 * @version 30th April 2022
 * @(#) MudBay.java 2022/22/22
 *
 * Copyright (c) 2022 Aberystwyth University.
 * All rights reserved.
 */
public class MudBay extends SpecialPlaceTile {

    public MudBay(int x, int y) {
        super(x, y, "Mud Bay", 11);
    }
}
