package game.models.board.tile;

import game.models.board.BoardPiece;

/**
 * Subclass for anchor bays. The only method is the constructor and it sets the position and tileID.
 *
 * @author AAAA AAAAA []
 * @version 30th April 2022
 * @(#) SeaTile.java 2022/22/22
 *
 * Copyright (c) 2022 Aberystwyth University.
 * All rights reserved.
 */
public class SeaTile extends BoardPiece {
    public SeaTile(int x, int y) {
        super(x, y, 0);
    }
}
