package game.models.board.tile;

import game.models.board.BoardPiece;

/**
 * Superclass for special places like anchor bays, cliff creks, mud bays and sea tiles.
 *
 * @author AAAA AAAAA []
 * @version 30th April 2022
 * @(#) SpecialPlaceTile.java 2022/22/22
 *
 * Copyright (c) 2022 Aberystwyth University.
 * All rights reserved.
 */

public abstract class SpecialPlaceTile extends BoardPiece {

    private final String name;

    public SpecialPlaceTile(int x, int y, String name, int tileID) {
        super(x, y, tileID);
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
