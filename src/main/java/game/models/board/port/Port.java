package game.models.board.port;

import game.models.board.BoardPiece;
import game.models.items.TradableItem;
import game.models.items.treasure.Treasure;

import java.util.ArrayList;
import java.util.List;

/**
 * Superclass for ports. The constructor sets position, inventory and name. Getters for city and inventory. Setters for
 * inventory.
 *
 * @author AAAA AAAAA []
 * @version 30th April 2022
 * @(#) Port.java 2022/22/22
 *
 * Copyright (c) 2022 Aberystwyth University.
 * All rights reserved.
 */
public abstract class Port extends BoardPiece {

    private final String city;
    private List<TradableItem> inventory;
    //Constructor
    public Port(int x, int y, String city, int tileID) {
        super(x, y, tileID);
        this.city = city;
        this.inventory = new ArrayList<>();
    }
    //Getters
    public String getCity() {
        return city;
    }

    public List<TradableItem> getInventory() {
        return inventory;
    }

    //Setters
    public void setInventory(List<TradableItem> newInventory) {
        this.inventory = newInventory;
    }

    public abstract void addTradableItem(TradableItem item);
}
