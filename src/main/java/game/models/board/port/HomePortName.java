package game.models.board.port;

/**
 * This class manages the ports. The index is dedicated by the order of taking turns.
 *
 * @author AAAA AAAAA []
 * @version 30th April 2022
 * @(#) HomePortName.java 2022/22/22
 *
 * Copyright (c) 2022 Aberystwyth University.
 * All rights reserved.
 */

public enum HomePortName {
    LONDON(0), GENOA(1), MARSEILLES(2), CADIZ(3);

    private static final HomePortName[] values = values();

    // Index dedicated by the order of taking turns
    private final int turnIndex;

    // Initialises the home port type
    HomePortName(int turnIndex) {
        this.turnIndex = turnIndex;
    }

    /**
     * Returns the index of the port dedicated by the order of taking turns
     *
     * @return Index of the port
     */
    public int getTurnIndex() {
        return turnIndex;
    }

    /**
     * Returns the next port type based on the order of taking turns
     *
     * @return Next home port type
     */
    public HomePortName getNext() {
        return values[(getTurnIndex() + 1) % values.length];
    }
}
