package game.models.board.port;

import game.exceptions.EmptyInventoryException;
import game.models.items.TradableItem;

/**
 * Subclass for trade ports. Methods include inventory and constructor.
 *
 * @author AAAA AAAAA []
 * @version 30th April 2022
 * @(#) TradePort.java 2022/22/22
 *
 * Copyright (c) 2022 Aberystwyth University.
 * All rights reserved.
 */
public class TradePort extends Port{
    //Constructor
    public TradePort(int x, int y, String city, int tileID) {
        super(x, y, city, tileID);
    }
    //Inventory methods
    public void addTradableItem(TradableItem tradableItem){
        getInventory().add(tradableItem);
    }

    public TradableItem withdrawTradableItem(TradableItem tradableItem) throws EmptyInventoryException {
        for (TradableItem t : getInventory()) {
            if(t.equals(tradableItem)){
                getInventory().remove(t);
                return tradableItem;
            }
        }
        throw new EmptyInventoryException("Inventory is Empty!");
    }
}
