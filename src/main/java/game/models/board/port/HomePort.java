package game.models.board.port;


import game.exceptions.EmptyInventoryException;
import game.exceptions.EndGameDetected;
import game.exceptions.RequirementAlreadySatisfied;
import game.models.items.TradableItem;
import game.models.items.cards.Card;
import game.models.items.treasure.Treasure;
import game.models.player.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * This class gives home ports an inventory, and it includes methods for setting the player and adding items.
 *
 * @author AAAA AAAAA []
 * @version 30th April 2022
 * @(#) HomePort.java 2022/22/22
 *
 * Copyright (c) 2022 Aberystwyth University.
 * All rights reserved.
 */
public class HomePort extends Port{

    private final HomePortName type;
    private Player player;

    private final List<Treasure> safeInventory;

    public HomePort(int x, int y, HomePortName portName, int tileID) {
        super(x, y, portName.toString().charAt(0) + portName.toString().substring(1).toLowerCase(), tileID);
        type = portName;
        this.safeInventory = new ArrayList<>();
    }

    public HomePortName getType() {
        return type;
    }

    public void setPlayer(Player player) throws RequirementAlreadySatisfied {
        if (this.player == null)
            this.player = player;
        else
            throw new RequirementAlreadySatisfied("Player is already set");
    }


    public Player getPlayer() {
        return player;
    }

    @Override
    public void addTradableItem(TradableItem item) {
        if (item instanceof Treasure treasure) {
            int treasureCount = 0;
            ArrayList<Treasure> safeTreasureList = new ArrayList<>();
            for (TradableItem tradableItem : getInventory()) {
                if (tradableItem instanceof Treasure t && Objects.equals(t.getType(), treasure.getType())) {
                    treasureCount++;
                    safeTreasureList.add(t);
                }
            }
            if (treasureCount >= 2) {
                safeInventory.add(treasure);
                for (Treasure t : safeTreasureList) {
                    safeInventory.add(t);
                    getInventory().remove(t);
                }
            }
            else {
                getInventory().add(treasure);
            }
        } else if (item instanceof Card card) {
            getInventory().add(card);
        }
    }

    public int returnPortValue(){
        int value = 0;
        for (TradableItem t: getInventory()) {
            value += t.getValue();
        }
        return value;
    }

    public void unloadShip() throws EndGameDetected {
        if(player.getInventory().size() != 0) {
            getInventory().addAll(player.unloadShip());
        }
        if (returnPortValue() >= 20)
            throw new EndGameDetected("Player won");
    }

    public List<Treasure> getSafeInventory() {
        return safeInventory;
    }

    public TradableItem withdrawTreasure(TradableItem tradableItem) throws EmptyInventoryException {
        for (TradableItem t : getInventory()) {
            if (t.equals(tradableItem)) {
                getInventory().remove(t);
                return tradableItem;
            }
        }
        throw new EmptyInventoryException("Inventory is Empty!");
    }

    public void depositTradableItem(TradableItem tradableItem) {
        getInventory().add(tradableItem);
    }
}
