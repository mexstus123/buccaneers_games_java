package game.models.board;

import game.models.board.island.FlatIsland;
import game.models.board.island.PirateIsland;
import game.models.board.island.TreasureIsland;
import game.models.board.port.Port;
import game.models.board.tile.AnchorBay;
import game.models.board.tile.CliffCreek;
import game.models.board.tile.MudBay;
import game.models.board.tile.SeaTile;
import game.models.player.Orientation;
import game.models.player.Player;
import javafx.util.Pair;

import java.util.*;

/**
 * ToDo: finish descriptions
 * Class representing a board. Contains and manages all board pieces.
 * Coordinates start with (1, 1) - bottom left corner
 *
 * @author Yat Shi Cheng [ysc1], edited by Franciszek Myslek [frm20]
 * @version 3rd May 2022
 * @(#) Board.java 2022/05/03
 *
 * Copyright (c) 2022 Aberystwyth University.
 * All rights reserved.
 */
public class Board {

    // ////////// //
    // Constants. //
    // ////////// //
    /**
     * Number of rows on the board
     */
    public static final int ROWS = 20;
    /**
     * Number of columns on the board
     */
    public static final int COLUMNS = 20;

    /**
     *  Representation for a blank tile
     */
    public static final int BLANK = 0;

    /**
     *  Representation for position of all 4 players' ships
     */
    public static final int PLAYER_1 = 1;
    public static final int PLAYER_2 = 2;
    public static final int PLAYER_3 = 3;
    public static final int PLAYER_4 = 4;

    /**
     *  Representation for position of all 6 ports, ordered by turn specification in FR 11
     */
    public static final int PORT_LONDON= 5;
    public static final int PORT_GENOA = 6;
    public static final int PORT_MARSEILLES = 7;
    public static final int PORT_CADIZ = 8;
    public static final int PORT_VENICE = 9;
    public static final int PORT_AMSTERDAM = 10;

    /**
     *  Representation for position of all 3 bays
     */
    public static final int BAY_MUD = 11;
    public static final int BAY_ANCHOR = 12;
    public static final int BAY_CLIFF = 13;

    /**
     *  Representation for position of all 3 islands
     */
    public static final int ISLAND_FLAT = 14;
    public static final int ISLAND_PIRATE = 15;
    public static final int ISLAND_TREASURE = 16;

    // //////////////// //
    // Class variables. //
    // //////////////// //


    // ////////////// //
    // Class methods. //
    // ////////////// //


    // /////////////////// //
    // Instance variables. //
    // /////////////////// //


    // ///////////// //
    // Constructors. //
    // ///////////// //


    // ////////////////////// //
    // Read/Write properties. //
    // ////////////////////// //


    // ///////////////////// //
    // Read-only properties. //
    // ///////////////////// //


    // //////// //
    // Methods. //
    // //////// //






    /**
     * Returns the BoardPiece that is occupying position (x, y)
     *
     * @param x X coordinate of the piece
     * @param y Y coordinate of the piece
     * @return BoardPiece on position (x, y) or null
     */
    public BoardPiece getBoardPiece(int x, int y) {
        return board.get(new Pair<>(x, y));
    }



    // Clears tile with given coordinates
    private void clearTiles(int x, int y) {
        board.remove(new Pair<>(x, y));
    }

    // Clears all tiles in the given area
    private void clearTiles(int xStart, int yStart, int xEnd, int yEnd) {
        for (int i = xStart; i < xEnd; i++)
            for (int j = yStart; j < yEnd; j++)
                board.remove(new Pair<>(i, j));
    }







    /**
     *  Retrieve tileID of strictly adjacent island on staticTiles, return 0 if there is none
     * @param x
     * @param y
     * @return
     */
    public int getAdjacentIslandID(int x, int y) {
        final int MIN_POS = 1, MAX_POS = 20;
        int tileID = BLANK;

        for (int x_diff = -1; x_diff <= 1; x_diff++) {
            for (int y_diff = -1; y_diff <= 1; y_diff++) {
                if (x_diff == 0 && y_diff == 0) {
                    continue;
                }
                else {
                    int tmp_x = x + x_diff, tmp_y = y + y_diff;
                    if (tmp_x < MIN_POS || tmp_x > MAX_POS || tmp_y < MIN_POS || tmp_y > MAX_POS) {
                        continue;
                    }
                    else {
                        if (getStaticTiles(tmp_x, tmp_y) >= ISLAND_FLAT && getStaticTiles(tmp_x, tmp_y) <= ISLAND_TREASURE) {
                            tileID = getStaticTiles(tmp_x, tmp_y);
                        }
                    }
                }
            }
        }
        return tileID;
    }





    private final int MIN_POS = 1, MAX_POS = 20;

    /**
     *  Debugging display
     */
    public void staticInfo() {
        //final Map<Point, Integer> mutableStaticTiles = new HashMap<>();
/*
        for (int int_y = MAX_POS; int_y > MIN_POS - 1 ; int_y--) {
            //System.out.print(int_x + 1);
            for (int int_x = MIN_POS; int_x < MAX_POS + 1; int_x++) {

                if (int_x >= 2 && int_x <= 4 && int_y >= 16 && int_y <= MAX_POS - 1) {
                    mutableStaticTiles.put(new Point(int_x, int_y), ISLAND_FLAT);
                } else if (int_x >= 17 && int_x <= MAX_POS - 1 && int_y >= 2 && int_y <= 5) {
                    mutableStaticTiles.put(new Point(int_x, int_y), ISLAND_PIRATE);
                } else if (int_x >= 9 && int_x <= 12 && int_y >= 9 && int_y <= 12) {
                    mutableStaticTiles.put(new Point(int_x, int_y), ISLAND_TREASURE);
                } else if (int_x == 1) {
                    if (int_y == 1) {
                        mutableStaticTiles.put(new Point(int_x, int_y), BAY_MUD);
                    } else if (int_y == 7) {
                        mutableStaticTiles.put(new Point(int_x, int_y), PORT_VENICE);
                    } else if (int_y == 14) {
                        mutableStaticTiles.put(new Point(int_x, int_y), PORT_LONDON);
                    }
                } else if (int_x == 7) {
                    if (int_y == 1) {
                        mutableStaticTiles.put(new Point(int_x, int_y), PORT_GENOA);
                    }
                } else if (int_x == 14) {
                    if (int_y == 20) {
                        mutableStaticTiles.put(new Point(int_x, int_y), PORT_CADIZ);
                    }
                } else if (int_x == 20) {
                    if (int_y == 1) {
                        mutableStaticTiles.put(new Point(int_x, int_y), BAY_ANCHOR);
                    } else if (int_y == 7) {
                        mutableStaticTiles.put(new Point(int_x, int_y), PORT_MARSEILLES);
                    } else if (int_y == 14) {
                        mutableStaticTiles.put(new Point(int_x, int_y), PORT_AMSTERDAM);
                    } else if (int_y == 20) {
                        mutableStaticTiles.put(new Point(int_x, int_y), BAY_CLIFF);
                    }
                }
            }
        }

 */

        System.out.println();
        System.out.println("Current static tiles: ");
        // Print x-coordinates
        System.out.print("     ");
        // Print the board of staticTiles
        for (int x = MIN_POS; x < MAX_POS + 1; x++) {
            System.out.printf("%2d ", x);
        }
        System.out.println("\n");
        for (int y = MAX_POS; y > MIN_POS - 1; y--) {
            System.out.printf("%2d  ", y);
            for (int int_x = MIN_POS; int_x < MAX_POS + 1; int_x++) {
                System.out.printf(" %2d", getStaticTiles(int_x, y));
                    /*
                    if (mutableStaticTiles.get(new Point(int_x, y)) != null) {
                        System.out.printf(" %2d", mutableStaticTiles.get(new Point(int_x, y)));

                    }
                    else {
                        System.out.printf(" %2d", 0);
                    }


                     */
            }

            System.out.println();
        }
        System.out.println();
    }
    public void playerInfo(Player[] players) {
        for (Player p: players) {
            System.out.println("Player " + p.getName() + " location is " + p.getCoordinates());
        }
        System.out.println();
    }


    // Board with all the pieces saved by coordinates
    private final Map<Pair<Integer, Integer>, BoardPiece> board;


    /**
     * Initializes all board pieces
     */
    public Board(Port[] ports) {
        board = new HashMap<>();
        initializeBoard(ports);
        initializeIslands();
    }

    // Positions all the board pieces on the board
    private void initializeBoard(Port[] ports) {
        // Initialize Sea Tiles
        for (int i = 1; i <= ROWS; i++) {
            for (int j = 1; j <= COLUMNS; j++) {
                board.put(new Pair<>(i, j), new SeaTile(i, j));
            }
        }

        // Initialize Islands
        Pair<Integer, Integer> coordinate = new Pair<>(2, 16);
        clearTiles(coordinate.getKey(), coordinate.getValue(), 4, 19);
        board.put(coordinate, new FlatIsland(coordinate.getKey(), coordinate.getValue()));

        coordinate = new Pair<>(17, 2);
        clearTiles(coordinate.getKey(), coordinate.getValue(), 19, 5);
        board.put(coordinate, new PirateIsland(coordinate.getKey(), coordinate.getValue()));

        coordinate = new Pair<>(9, 9);
        clearTiles(coordinate.getKey(), coordinate.getValue(), 12, 12);
        board.put(coordinate, new TreasureIsland(coordinate.getKey(), coordinate.getValue()));

        // Initialize Ports
        for (Port port : ports) {
            board.put(port.getCoordinates(), port);
        }
//        List<Pair<Pair<Integer, Integer>, Port>> homePorts = new ArrayList<>();
//        homePorts.add(new Pair<>(new Pair<>(1, 14), ports[0]));
//        homePorts.add(new Pair<>(new Pair<>(14, 20), ports[1]));
//        homePorts.add(new Pair<>(new Pair<>(20, 7), ports[2]));
//        homePorts.add(new Pair<>(new Pair<>(7, 1), HomePortName.GENOA));
//        for (Pair<Pair<Integer, Integer>, HomePortName> c : homePorts) {
//            clearTiles(c.getKey().getKey(), c.getKey().getValue());
//            board.put(c.getKey(), new HomePort(c.getKey().getKey(), c.getKey().getValue(), c.getValue()));
//        }
//        List<Pair<Pair<Integer, Integer>, String>> tradePorts = new ArrayList<>();
//        tradePorts.add(new Pair<>(new Pair<>(20, 14), "Amsterdam"));
//        tradePorts.add(new Pair<>(new Pair<>(1, 7), "Venice"));
//        for (Pair<Pair<Integer, Integer>, String> c : tradePorts) {
//            clearTiles(c.getKey().getKey(), c.getKey().getValue());
//            board.put(c.getKey(), new TradePort(c.getKey().getKey(), c.getKey().getValue(), c.getValue()));
//        }

        // Initialize Anchor Bay, Cliff Creek and Mud Bay
        coordinate = new Pair<>(1, 1);
        clearTiles(coordinate.getKey(), coordinate.getValue());
        board.put(coordinate, new MudBay(coordinate.getKey(), coordinate.getValue()));

        coordinate = new Pair<>(20, 20);
        clearTiles(coordinate.getKey(), coordinate.getValue());
        board.put(coordinate, new CliffCreek(coordinate.getKey(), coordinate.getValue()));

        coordinate = new Pair<>(20, 1);
        clearTiles(coordinate.getKey(), coordinate.getValue());
        board.put(coordinate, new AnchorBay(coordinate.getKey(), coordinate.getValue()));
    }

    /**
     * Accessor method.
     * @param row - the row of the grid
     * @param col - the column of the grid
     * @return - the value at the coordinates of the row and column
     */
    public int getStaticTiles(int row, int col)
    {
        if (board.containsKey(new Pair<>(row, col)))
            return board.get(new Pair<>(row, col)).getTileID();
        else
            return -1;
    }
    public int getStaticTiles(Pair<Integer, Integer> coordinates) {
        if (board.containsKey(coordinates))
            return board.get(coordinates).getTileID();
        else
            return -1;
    }

    public boolean isDiagonalOpposite(Player p1, Player p2) {
        boolean isDiagonalOpposite = false;
        //TODO Compare horizontal and vertical difference
        if (Math.abs(p1.getX() - p2.getY()) == Math.abs(p1.getX() - p2.getX()))
            isDiagonalOpposite = true;
        return isDiagonalOpposite;
    }
    /**
     *  Detection of special and ordinary legal moves, such as occupied and unoccupied dynamicTiles
     *
     * @param x
     * @param y
     * @return
     */
    public boolean isValidMoves(int x, int y) {
        int staticID = getStaticTiles(x, y);
        boolean isValid = false;

        //TODO Validation of special legal moves
        //TODO Handling of players' collision at sea and occupied bays
        if (staticID == BLANK
                || staticID >= BAY_MUD && staticID <= BAY_CLIFF) {
            System.out.println("Enemy ship located at destination!");
            isValid = true;
        }
        //TODO Handling of players-features collision, all ports and bays
        else
        if (staticID >= PORT_LONDON && staticID <= BAY_CLIFF) {
            System.out.println("Arriving at a dock");
            isValid = true;
        }
        //TODO Handling of players-features adjacency, on all 3 islands
        else
        if (getAdjacentIslandID(x, y) != BLANK) {
            System.out.println("Arriving on the coast of an island!");
            isValid = true;
        }
        else
        //TODO Validation of ordinary legal moves, open sea
        {
            System.out.println("Arriving at open sea!");
            isValid = true;
        }
        return isValid;
    }

    /**
     *  Indicate corresponding turn state for a given player location on staticTiles
     * @param player
     */
    public void playerTurnState(Player player) {

        int tileID = getStaticTiles(player.getCoordinates());
        int x = player.getX(), y = player.getY();
        //TODO Arriving at ports
        if (tileID >= PORT_LONDON && tileID <= PORT_AMSTERDAM) {
            System.out.println("Player arrived at a port!");
            //code = 1;
        }
        else
        if (tileID != BAY_ANCHOR) {
            //TODO Arriving at Private Island
            if (getAdjacentIslandID(x, y) == ISLAND_PIRATE) {
                System.out.println("Player arrived at Private Island!");
                //code = 2;
            }
            else
                //TODO Arriving at Treasure Island
                if (getAdjacentIslandID(x, y) == ISLAND_TREASURE) {
                    System.out.println("Player arrived at Treasure Island!");
                    //code = 3;
                }
                else
                    //TODO Arriving at Flat Island
                    if (getAdjacentIslandID(x, y) == ISLAND_FLAT) {
                        System.out.println("Player arrived at Flat Island!");
                        //code = 4;
                    }
        }
        else
        //TODO Arriving at Anchor Bay
        {
            System.out.println("Player arrived at Anchor Bay!");
            //code = 5;
        }
    }


    //  Helper method for initilizing islands

    public void initializeIslands() {
        for (int int_y = MAX_POS; int_y > MIN_POS - 1 ; int_y--) {
            //System.out.print(int_x + 1);
            for (int int_x = MIN_POS; int_x < MAX_POS + 1; int_x++) {
                Pair<Integer, Integer> coordinate = new Pair<>(int_x, int_y);

                if (int_x >= 2 && int_x <= 4 && int_y >= 16 && int_y <= MAX_POS - 1) {
                    if (!(int_x == 2 && int_y == 16))
                        board.put(coordinate, new FlatIsland(coordinate.getKey(), coordinate.getValue(), true));
                } else if (int_x >= 17 && int_x <= MAX_POS - 1 && int_y >= 2 && int_y <= 5) {
                    if (!(int_x == 17 && int_y == 2))
                        board.put(coordinate, new PirateIsland(coordinate.getKey(), coordinate.getValue(), true));
                } else if (int_x >= 9 && int_x <= 12 && int_y >= 9 && int_y <= 12) {
                    if (!(int_x == 9 && int_y == 9))
                        board.put(coordinate, new TreasureIsland(coordinate.getKey(), coordinate.getValue(), true));
                }
            }
        }
    }




    /**
     *  New methods
     */

    /**
     *  Detection of illegal player moves for occupied ports and non-occupiable staticTiles
     *
     * @param players
     * @param x
     * @param y
     * @return
     */
    public boolean isInvalidMoves(Player[] players, int x, int y) {
        int staticID = getStaticTiles(x, y);
        boolean isInvalid = false;
        Pair<Integer, Integer> coordinates = new Pair<>(x, y);

        for (Player p: players) {
            if (p.getCoordinates().equals(coordinates)) {
                //TODO Rejection of illegal moves
                //TODO Handling of players' collision on occupied ports
                if (staticID >= PORT_LONDON && staticID <= PORT_AMSTERDAM) {
                    System.out.println("Invalid Move! Destination port is already occupied.");
                    isInvalid = true;
                    break;
                }
                else
                if (getAdjacentIslandID(coordinates.getKey(), coordinates.getValue()) == ISLAND_TREASURE) {
                    System.out.println("Invalid Move! Illegal to attack player on the coast of Treasure Island.");
                    isInvalid = true;
                    break;
                }
            }
            //TODO Handling of players-features collision on all 3 islands
            else
            if (staticID >= ISLAND_FLAT && staticID <= ISLAND_TREASURE) {
                System.out.println("Invalid Move! Destination is inland.");
                isInvalid = true;
                break;
            }
        }

        return isInvalid;
    }
    public List<BoardPiece> getPossibleMovesAtDirection(Player[] players, Player currPlayer, Set<Orientation> directionSet) {
        List<BoardPiece> possibleMoves = new ArrayList<>();
        Set<Orientation> invalidDirection = new HashSet<>();
        int crewSize = currPlayer.getCrewSize();
        if (crewSize == 0) {
            crewSize = 1;
        }

        for (int i = 1; i < crewSize + 1; i++) {
            if (!invalidDirection.isEmpty())
                directionSet.removeAll(invalidDirection);
            for (Orientation direction: directionSet) {
                int curr_x = currPlayer.getX(), curr_y = currPlayer.getY();

                switch (direction) {
                    case N -> curr_y += i;
                    case S -> curr_y -= i;
                    case E -> curr_x += i;
                    case W -> curr_x -= i;
                    case NE -> {
                        curr_x += i;
                        curr_y += i;
                    }
                    case SE -> {
                        curr_x += i;
                        curr_y -= i;
                    }
                    case NW -> {
                        curr_x -= i;
                        curr_y += i;
                    }
                    case SW -> {
                        curr_x -= i;
                        curr_y -= i;
                    }
                }
                if (board.containsKey(new Pair<>(curr_x, curr_y))) {

                    // Only sea tiles, ports, and bays are occupiable
                    if (!isInvalidMoves(players, curr_x, curr_y)) {
                        possibleMoves.add(getBoardPiece(curr_x, curr_y));
                    }
                    else {
                        invalidDirection.add(direction);
                    }
                }
            }
        }
        return possibleMoves;
    }
    public List<BoardPiece> getPossibleMoves(Player[] players, Player currPlayer, boolean isLostAttack) {
        List<BoardPiece> possibleMoves;
        Set<Orientation> directionSet = new HashSet<>();
        int x = currPlayer.getX(), y = currPlayer.getY();

        if (getStaticTiles(currPlayer.getCoordinates()) >= PORT_LONDON
                && getStaticTiles(currPlayer.getCoordinates()) <= PORT_AMSTERDAM) {

            // Conditional statements does not consider corner cases, as no ports are expected
            if (x == MIN_POS) {
                directionSet.add(Orientation.N);
                directionSet.add(Orientation.NE);
                directionSet.add(Orientation.E);
                directionSet.add(Orientation.SE);
                directionSet.add(Orientation.S);
            }
            else
            if (x == MAX_POS) {
                directionSet.add(Orientation.N);
                directionSet.add(Orientation.NW);
                directionSet.add(Orientation.W);
                directionSet.add(Orientation.SW);
                directionSet.add(Orientation.S);
            }
            if (y == MIN_POS) {
                directionSet.add(Orientation.N);
                directionSet.add(Orientation.NE);
                directionSet.add(Orientation.E);
                directionSet.add(Orientation.NW);
                directionSet.add(Orientation.W);
            }
            else
            if (y == MAX_POS) {
                directionSet.add(Orientation.W);
                directionSet.add(Orientation.SW);
                directionSet.add(Orientation.E);
                directionSet.add(Orientation.SE);
                directionSet.add(Orientation.S);
            }

        }
        else
        if (isLostAttack) {
            directionSet.add(Orientation.N);
            directionSet.add(Orientation.NW);
            directionSet.add(Orientation.W);
            directionSet.add(Orientation.SW);
            directionSet.add(Orientation.S);
            directionSet.add(Orientation.SE);
            directionSet.add(Orientation.E);
            directionSet.add(Orientation.NE);
        }
        else {
            directionSet.add(currPlayer.getOrientation());
        }
        possibleMoves = getPossibleMovesAtDirection(players, currPlayer, directionSet);

        return possibleMoves;
    }
}
