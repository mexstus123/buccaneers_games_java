package game.models.board.island;

import game.models.items.TradableItem;
import game.models.items.cards.Card;
import game.models.items.treasure.Treasure;

import java.util.ArrayList;
import java.util.List;

/**
 * Subclass for flat islands. Constructor sets position and specifies if it is special or not. Methods for inventory,
 * position and size.
 *
 * @author AAAA AAAAA []
 * @version 30th April 2022
 * @(#) FlatIsland.java 2022/22/22
 *
 * Copyright (c) 2022 Aberystwyth University.
 * All rights reserved.
 */
public class FlatIsland extends Island {

    List<TradableItem> inventory;

    public FlatIsland(int x, int y) {
        super(x, y, 3, 4, "Flat Island", 14);
        this.inventory = new ArrayList<>();
    }

    public FlatIsland(int x, int y, boolean isSpecial) {
        super(x, y, 14);
    }



    //
    //TODO Jac94 Merge
    //

    public List<TradableItem> getInventory(){
        return inventory;
    }

    public void setInventory(List<TradableItem> newInventory){
        this.inventory = newInventory;
    }

    public void depositTradableItem(TradableItem tradableItem){
        inventory.add(tradableItem);
    }

    public List<TradableItem> withdrawAllInventory(List<TradableItem> playerInventory){
        List<TradableItem> newList = new ArrayList<>();
        newList.addAll(inventory);
        newList.addAll(playerInventory);
        inventory.clear();
        return newList;
    }

    public List<Card> withdrawCards(){
        List<Card> cardList = new ArrayList<>();
        for (TradableItem t : inventory) {
            if(t instanceof Card){
                cardList.add((Card) t);
                inventory.remove(t);
            }
        }
        return cardList;
    }

    public List<Treasure> withdrawTreasure(int availableInventorySpace){
        List<Treasure> treasureList = new ArrayList<>();

        for(int i = 0; i < availableInventorySpace; i++){
            Treasure tmpTreasure = new Treasure("", 0);
            for (TradableItem t : inventory) {
                if(t instanceof Treasure){
                    if(t.getValue() > tmpTreasure.getValue()){
                        tmpTreasure = (Treasure) t;
                    }
                }
            }
            treasureList.add(tmpTreasure);
            inventory.remove(tmpTreasure);
        }
        return treasureList;
    }

}
