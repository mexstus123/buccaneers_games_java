package game.models.board.island;

import game.exceptions.EmptyInventoryException;
import game.models.items.TradableItem;
import game.models.items.cards.chance.ChanceCard;
import game.models.items.treasure.Treasure;

import java.util.*;

/**
 * Subclass for treasure islands. Methods include chance cards, inventory and treasure.
 *
 * @author AAAA AAAAA []
 * @version 30th April 2022
 * @(#) TreasureIsland.java 2022/22/22
 *
 * Copyright (c) 2022 Aberystwyth University.
 * All rights reserved.
 */
public class TreasureIsland extends Island {
    List<Treasure> treasureInventory; // Remove once safe because using List of TradableItem instead.
    List<TradableItem> inventory;

    //Not sure if needed

    Queue<ChanceCard> chanceDeck = new LinkedList<>();
    public ChanceCard peekDeck() { return chanceDeck.peek(); }
    public ChanceCard dealCard() {
        return chanceDeck.remove();
    }
    public void returnCard(ChanceCard newCard) {
        chanceDeck.add(newCard);
    }

    public TreasureIsland(int x, int y) {
        super(x, y, 4, 4, "Treasure Island", 16);
        treasureInventory = new ArrayList<>();
        inventory = new ArrayList<>();
    }

    public TreasureIsland(int x, int y, boolean isSpecial) {
        super(x, y, 16);
        treasureInventory = new ArrayList<>();
        inventory = new ArrayList<>();
    }

    public List<Treasure> getTreasureInventory() {
        return treasureInventory;
    }
    public boolean isContainTreasure(Treasure treasure) {
        return treasureInventory.contains(treasure);
    }

    public void removeTreasure(Treasure treasure){
        if (isContainTreasure(treasure)) {
            treasureInventory.remove(treasure);
        }
    }

    public void sortTreasureByAsc() {
        treasureInventory.sort(Comparator.comparing(Treasure::getValue).thenComparing(Comparator.comparing(Treasure::getValue)));
    }
    public void sortTreasureByDesc() {
        treasureInventory.sort(Comparator.comparing(Treasure::getValue).thenComparing(Comparator.comparing(Treasure::getValue).reversed()));
    }

    public void depositTradabelItem(TradableItem tradableItem){
        inventory.add(tradableItem);
    }

    public TradableItem withdrawTreasure(TradableItem tradableItem) throws EmptyInventoryException{
        for (TradableItem t : inventory) {
            if(t.equals(tradableItem)){
                inventory.remove(t);
                return tradableItem;
            }
        }
        throw new EmptyInventoryException("Inventory is empty!");
        //return null; //In theory should be fine, unreachable return statement.
    }
    public void setInventory(List<TradableItem> newInventory){
        this.inventory = newInventory;
    }

    public void depositTradableItem(TradableItem tradableItem){
        inventory.add(tradableItem);
    }

    public void setChanceDeck(Queue<ChanceCard> deck){
        chanceDeck = deck;
    }

}
