package game.models.board.island;

import game.models.board.BoardPiece;

/**
 * Island superclass. Includes methods for position, size and name.
 *
 * @author AAAA AAAAA []
 * @version 00th MONTH 2022
 * @(#) Island.java 2022/22/22
 *
 * Copyright (c) 2022 Aberystwyth University.
 * All rights reserved.
 */
public abstract class Island extends BoardPiece {

    private final int sizeX;
    private final int sizeY;

    private final String name;

    public Island(int x, int y, int sizeX, int sizeY, String name, int tileID) {
        super(x, y, tileID);
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        this.name = name;
    }

    public Island(int x, int y, int tileID) {
        super(x, y, tileID);
        sizeX = 0;
        sizeY = 0;
        this.name = null;
    }

    public int getSizeX() {
        return sizeX;
    }

    public int getSizeY() {
        return sizeY;
    }

    public String getName() {
        return name;
    }
}
