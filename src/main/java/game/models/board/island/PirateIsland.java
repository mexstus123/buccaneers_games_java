package game.models.board.island;

import game.exceptions.EmptyInventoryException;
import game.models.items.TradableItem;
import game.models.items.cards.Card;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * Subclass for pirate islands. Constructor sets position and specifies if it is special or not. Methods for inventory,
 * position and size.
 *
 * @author AAAA AAAAA []
 * @version 30th April 2022
 * @(#) PirateIsland.java 2022/22/22
 *
 * Copyright (c) 2022 Aberystwyth University.
 * All rights reserved.
 */
public class PirateIsland extends Island {
    List<TradableItem> inventory;
    private Queue<Card> crewDeck = new LinkedList<>();

    public PirateIsland(int x, int y) {
        super(x, y, 3, 4, "Pirate Island", 15);
        this.inventory = new ArrayList<>();
    }

    public PirateIsland(int x, int y, boolean isSpecial) {
        super(x, y, 15);

    }

    public void depositCard(Card card){
        crewDeck.add(card);
    }

    public Card dealCard() {
        return crewDeck.remove();
    }
    public void returnCard(Card newCard) {
        crewDeck.add(newCard);
    }

    //
    //TODO Jac94 Merge
    //

    public List<TradableItem> getInventory(){
        return inventory;
    }

    public void setInventory(List<TradableItem> newInventory){
        this.inventory = newInventory;
    }

    public void depositTradableItem(TradableItem tradableItem){
        inventory.add(tradableItem);
    }

    public TradableItem withdrawTradableItem(TradableItem tradableItem) throws EmptyInventoryException {
        for (TradableItem t : inventory) {
            if(t.equals(tradableItem)){
                inventory.remove(tradableItem);
                return tradableItem;
            }
        }
        throw new EmptyInventoryException("Inventory is Empty!");
    }
}
