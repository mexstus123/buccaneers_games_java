package game.models.board;

import javafx.util.Pair;

/**
 * Abstract superclass for board pieces. Constructor sets position and tileID. Getters and setters for position and
 * TileID
 *
 * @author AAAA AAAAA []
 * @version 30th April 2022
 * @(#) FlatIsland.java 2022/22/22
 *
 * Copyright (c) 2022 Aberystwyth University.
 * All rights reserved.
 */

public abstract class BoardPiece {

    private Pair<Integer, Integer> coordinates;
    private int tileID;

    public BoardPiece(int x, int y, int tileID) {
        coordinates = new Pair<>(x, y);
        setTileID(tileID);
    }

    public int getX() {
        return coordinates.getKey();
    }

    public int getY() {
        return coordinates.getValue();
    }

    public Pair<Integer, Integer> getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(int x, int y) {
        this.coordinates = new Pair<>(x, y);
    }

    public void setCoordinates(Pair<Integer, Integer> coordinates) {
        this.coordinates = coordinates;
    }

    public int getTileID() {
        return tileID;
    }

    public void setTileID(int tileID) {
        this.tileID = tileID;
    }
}
