package game.models.items.treasure;

import game.models.items.TradableItem;

/**
 * Class for treasures. It is a subclass of tradeable item.
 *
 * @author (name)
 * @version 0.? (put status of version here)
 * @see //(ref to related classes)
 */
public class Treasure extends TradableItem {
    /**
     * Constructor
     * @param type
     * @param value
     */
    public Treasure(String type,int value) {
        super(type,value);
    }

    @Override
    public String getType() {
        return super.getType();
    }

    @Override
    public boolean equals(TradableItem tradableItem) {
        return super.equals(tradableItem);
    }

}

