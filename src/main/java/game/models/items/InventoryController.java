package game.models.items;

import game.models.board.island.FlatIsland;
import game.models.board.island.PirateIsland;
import game.models.board.island.TreasureIsland;
import game.models.items.cards.Card;
import game.models.items.treasure.Treasure;
import game.models.player.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * ToDo Add Class Functionality
 *
 * @author AAAA AAAAAA [AAA1]
 * @version AAAA
 * @(#) InventoryController.java 2022/11/11
 *
 * Copyright (c) 2022 Aberystwyth University.
 * All rights reserved.
 */
public class InventoryController {

    // ////////// //
    // Constants. //
    // ////////// //


    // //////////////// //
    // Class variables. //
    // //////////////// //


    // ////////////// //
    // Class methods. //
    // ////////////// //


    // /////////////////// //
    // Instance variables. //
    // /////////////////// //


    // ///////////// //
    // Constructors. //
    // ///////////// //


    // ////////////////////// //
    // Read/Write properties. //
    // ////////////////////// //


    // ///////////////////// //
    // Read-only properties. //
    // ///////////////////// //


    // //////// //
    // Methods. //
    // //////// //

    public static final int MAX_TREASURE_VALUE = 5, MIN_TREASURE_VALUE = 2;

    /**
     * Helper methods for determining player's inventory status for UI of PR-35,
     * ref Card ID: 11 to 14, 27
     *
     * @param treasureList
     * @param roomForTreasure either be 0, 1 or 2
     * @return
     */
    public static int maxTreasureValue(TreasureIsland treasureList, int roomForTreasure) {
        //TODO Return maxTreasureValue given player available slots
        for (int n = 0; n < roomForTreasure; n++) {

        }
        return 0;
    }

    public static int getTreasureIndex(List<Treasure> treasureList, int value) {
        int index = -1;
        for (Treasure t : treasureList) {
            if (t.getValue() == value) {
                index = treasureList.indexOf(t);
                break;
            }
        }
        return index;
    }

    public static void addTreasure(List<Treasure> treasureInventory, int value) {
        switch (value) {
            case 2:
                treasureInventory.add(new Treasure("",2));
                break;
            case 3:
                treasureInventory.add(new Treasure("",3));
                break;
            case 4:
                treasureInventory.add(new Treasure("", 4));
                break;
            case 5:
                treasureInventory.add(new Treasure("", 5));
                treasureInventory.add(new Treasure("",6));
                break;
        }
    }

    public static List<Treasure> getTreasureList(int maxValue) {
        List<Treasure> tmpTreasureList = new ArrayList<>();
        for (int n = Math.min(maxValue, MAX_TREASURE_VALUE); n > MIN_TREASURE_VALUE; n--) {
            addTreasure(tmpTreasureList, n);
        }
        return tmpTreasureList;
    }

    public static void getTreasureDictList(List<Treasure> sourceList, List<Treasure> dictList) {
        dictList.removeIf(t -> !sourceList.contains(t));
    }

    /**
     *  Suffix Extreme denotes transaction of lowest / highest values items
     *  Suffix BySize denotes transaction of crew cards by computed size
     *  Suffix ByAmount denotes transaction of crew cards capping at a given maxAmount
     *  Suffix ByValue denotes transaction of treasures capping at a given maxValue
     */

    /**
     *  Transaction methods for treasures
     */
    /**
     * It is assumed that TreasureIsland has at least 1 treasure, before calling this method
     *
     * @param treasureIsland
     * @param player
     * @param maxTotalValue
     */
    public static void collectTreasureByValue(TreasureIsland treasureIsland, Player player, int maxTotalValue) {
        treasureIsland.sortTreasureByDesc();

        //TODO Collect highest possible total value of treasures for player
        // Ref Card ID: 11, 13, 12, 14, 16, 17, 18, 25, 26, 27
        int roomForTreasure = player.getRoomForTreasure();
        int currTotalValue = 0;

        switch (Math.min(player.getRoomForTreasure(), treasureIsland.getTreasureInventory().size())) {
            //TODO Case of player only 1 room || treasureIsland has only 1 treasure
            case 1:
                // player can only get a treasure as valuable as value 5
                if (maxTotalValue > MAX_TREASURE_VALUE) {
                    currTotalValue = MAX_TREASURE_VALUE;
                }
                for (Treasure t : treasureIsland.getTreasureInventory()) {
                    if (t.getValue() <= currTotalValue) {
                        treasureIsland.removeTreasure(t);
                        player.addTreasure(t);
                        break;
                    }
                }
                break;
            //TODO Case of player has 2 rooms && treasureIsland has at least 2 treasures
            case 2:
                List<Treasure> dictTreasureList = getTreasureList(maxTotalValue);
                getTreasureDictList(treasureIsland.getTreasureInventory(), dictTreasureList);

                int currDictValue = dictTreasureList.get(0).getValue();


                boolean isFound = false;
                int i = 0, j = 1;
                Treasure currTreasure = treasureIsland.getTreasureInventory().get(i);

                while (!isFound && i != treasureIsland.getTreasureInventory().size()) {

                    if (currDictValue + currTreasure.getValue() <= maxTotalValue) {
                        treasureIsland.removeTreasure(currTreasure);
                        treasureIsland.removeTreasure(dictTreasureList.get(j - 1));
                        player.addTreasure(currTreasure);
                        player.addTreasure(dictTreasureList.get(j - 1));

                        isFound = true;

                    }
                    else {
                        // Case of encountering a treasure with same value of currDictValue
                        //    || a level of cycle has reached the end
                        if (currTreasure.getValue() == currDictValue
                                || i == treasureIsland.getTreasureInventory().size() - 1) {

                            // Start from 2nd largest treasure, as indicated in dictTreasureList, if any
                            if (dictTreasureList.size() - 1 > j) {
                                j++;
                                currTreasure = dictTreasureList.get(j);
                                currDictValue = currTreasure.getValue();

                                // Reset the start index of treasureIsland.getTreasureInventory()
                                i = treasureIsland.getTreasureInventory().indexOf(currTreasure);
                            }
                        }

                        i++;
                        currTreasure = treasureIsland.getTreasureInventory().get(i);
                    }
                }

                // Case of only 1 treasure could be issued
                if (!isFound) {
                    for (Treasure tmpTreasure : dictTreasureList) {
                        if (tmpTreasure.getValue() <= Math.min(MAX_TREASURE_VALUE, maxTotalValue)) {
                            treasureIsland.removeTreasure(currTreasure);
                        }
                    }
                }
        }
    }

    public static void returnTreasureExtreme(FlatIsland flatIsland, Player player, boolean isLeast) {
        player.sortTreasureByAsc();

        //TODO Return lowest possible value of treasure from player to TreasureIsland
        // Ref Card ID 8
        if (isLeast) {
            Treasure t = player.getTreasure().get(0);
            player.removeTreasure(t);
            flatIsland.depositTradableItem(t);
        }
        else {
            //TODO Return highest possible value of treasure from player to TreasureIsland
            // Ref Card ID 9
            Treasure t = player.getTreasure().get(player.getTreasure().size() - 1);
            player.removeTreasure(t);
            flatIsland.depositTradableItem(t);
        }
    }
    public static void transferTreasureExtreme(Player recipient, Player sender, boolean isLeast) {
        //TODO Return least possible value of treasure from sender to recipient
        // Ref Card ID 7
        if (isLeast) {
            sender.sortTreasureByAsc();
            Treasure t = sender.getTreasure().get(0);
            sender.removeTreasure(t);
            recipient.addTreasure(t);

        }
    }


    /**
     *  Transaction methods for crew cards
     */
    public static void collectCrewCardByAmount(PirateIsland pirateIsland, Player player, int maxAmount) {
        //TODO Collect highest possible number of crew cards for player, by maxAmount, of lowest possible values
        // Ref Card ID: 1, 3, 4, 5, 6, 13, 12, 14, 15, 18, 27, 28
        for (int i = 0; i < maxAmount; i++) {
            player.addCrewCard(pirateIsland.dealCard());
        }
    }
    public static void reduceCrewCardBySize(Player player, PirateIsland pirateIsland, int leastFinalCardTotalSize) {
        //TODO Reduce player crew card size down to leastFinalCardTotalSize, resultant size should be as high as possible
        // Ref Card ID: 16, 17
        int cardTotalSize = player.getCrewSize();

        if (cardTotalSize > leastFinalCardTotalSize) {
            player.sortCrewByAsc();

            for (Card c: player.getCrewHand()) {
                cardTotalSize -= c.getValue();

                if (cardTotalSize >= leastFinalCardTotalSize) {
                    player.withdrawCrewCard(c);
                    pirateIsland.depositCard(c);
                }
                else {
                    break;
                }
            }
        }
    }

    /**
     *  Island destination should either be Pirate Island or Flat Island
     * @param pirateIsland
     * @param flatIsland
     * @param cardDrawer
     * @param isLowest
     * @param maxAmount
     */
    public static void returnCrewCardExtremeByAmount(PirateIsland pirateIsland, FlatIsland flatIsland, Player cardDrawer, boolean isLowest, int maxAmount) {
        cardDrawer.sortCrewByAsc();

        //TODO Return lowest crew card held by players by maxAmount
        // Ref Card ID 8, 22
        if (isLowest) {

            for (int i = 0; i < Math.min(cardDrawer.getCrewCount(), maxAmount); i++) {
                Card c = cardDrawer.getCrewHand().get(i);
                cardDrawer.withdrawCrewCard(c);
                pirateIsland.returnCard(c);
            }
        }
        else {
            //TODO Return highest crew card held by players by maxAmount
            for (int i = Math.min(cardDrawer.getCrewCount(), maxAmount); i > 0; i++) {
                Card c = cardDrawer.getCrewHand().get(i);
                cardDrawer.withdrawCrewCard(c);

                // Ref Card ID 10
                if (pirateIsland != null) {
                    pirateIsland.returnCard(c);
                }
                // Ref Card ID 9
                else {
                    flatIsland.depositTradableItem(c);
                }
            }
        }
    }
    public static void transferCrewCardExtremeByAmount(Player recipient, Player sender, boolean isLowest, int maxAmount) {
        //TODO Give lowest possible value of crew card from sender to recipient, up to maxAmount
        // Ref Card ID 7
        if (isLowest) {
            sender.sortCrewByAsc();

            for (int i = 0; i < Math.min(sender.getCrewCount(), maxAmount); i++) {
                Card c = sender.getCrewHand().get(i);
                sender.withdrawCrewCard(c);
                recipient.addCrewCard(c);
            }
        }
    }
    public static void exchangeCrewCardExtremeByAmount(Player player1, Player player2, int maxAmount) {
        //TODO Exchange crew cards up to maxAmount randomly, among 2 players
        // Exact exchange amount depends on min(player1.getCrewCount(), player2.getCrewCount())
        // Ref Card ID 20

        int exchangeAmount = Math.min(player1.getCrewCount(), player2.getCrewCount());
        for (int i = 0; i < Math.min(exchangeAmount, maxAmount); i++) {
            Random rand = new Random();
            Card c1 = player1.getCrewHand().get(rand.nextInt(player1.getCrewHand().size()));
            Card c2 = player1.getCrewHand().get(rand.nextInt(player2.getCrewHand().size()));

            if (!c1.equals(c2)) {
                player1.withdrawCrewCard(c1);
                player2.withdrawCrewCard(c2);
                player1.addCrewCard(c2);
                player2.addCrewCard(c1);
            }
        }
    }
}
