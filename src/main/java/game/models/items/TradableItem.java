package game.models.items;

import javafx.scene.image.Image;

import java.util.Objects;

/**
 * ToDo Add Class Functionality
 *
 * @author AAAA AAAAAA [AAA1]
 * @version AAAA
 * @(#) TradableItem.java 2022/11/11
 *
 * Copyright (c) 2022 Aberystwyth University.
 * All rights reserved.
 */
public class TradableItem {
    private String type;
    private int value;
    private Image art;

    public TradableItem(String type,int value){
        this.value = value;
        this.type = type;
        setTypeByValue(type,value);
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public boolean equals(TradableItem tradableItem){
        if(this == tradableItem){
            return true;
        }
        else{
            return false;
        }
    }

    /**
     * Method to set the item type by value.
     * @param type
     * @param value
     */
    private void setTypeByValue(String type, int value) {
        if (Objects.equals(type, "")) {
            switch (value) {
                case 2 -> this.type = "Rum";
                case 3 -> this.type = "Pearl";
                case 4 -> this.type = "Gold";
                case 5 -> this.type = "Ruby";
                case 6 -> {
                    this.type = "Diamond";
                    this.value = 5;
                }
                default -> {
                }
            }
        }
    }

    public String getType() {
        return type;
    }
}
