package game.models.items.cards.chance;

import game.models.board.Board;
import game.models.board.island.FlatIsland;
import game.models.board.island.PirateIsland;
import game.models.board.island.TreasureIsland;
import game.models.player.Orientation;
import game.models.player.Player;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * A class that only responsible for branching the execution of individual card behaviours, excluding Card ID 21, 23, 24
 * Presumption of ChanceCardController is that the caller should provide takeTreasure and chosenPlayer values,
 * according to relevant ChanceCard Card behaviours, ie Card ID 11, 20 respectively
 *
 * @author Yat Shi Cheng [ysc1]
 * @version 3rd May 2022
 * @(#) ChanceCardController.java 2022/05/03
 * Copyright (c) 2022 Aberystwyth University.
 * All rights reserved.
 */

public class ChanceCardController {

    // ////////// //
    // Constants. //
    // ////////// //


    // //////////////// //
    // Class variables. //
    // //////////////// //


    // ////////////// //
    // Class methods. //
    // ////////////// //


    // /////////////////// //
    // Instance variables. //
    // /////////////////// //


    // ///////////// //
    // Constructors. //
    // ///////////// //


    // ////////////////////// //
    // Read/Write properties. //
    // ////////////////////// //


    // ///////////////////// //
    // Read-only properties. //
    // ///////////////////// //


    // //////// //
    // Methods. //
    // //////// //

    Set<Integer> collectorSet1 = new HashSet<Integer>(Arrays.asList(11, 12, 13, 14, 15, 16, 17, 18, 27, 28));
    Set<Integer> collectorSet2 = new HashSet<Integer>(Arrays.asList(3, 4));
    Set<Integer> collectorSet3 = new HashSet<Integer>(Arrays.asList(5, 6, 25, 26));
    Set<Integer> collectorSet4 = new HashSet<Integer>(List.of(1));
    Set<Integer> rmvSet1 = new HashSet<Integer>(Arrays.asList(8, 9, 10));
    Set<Integer> rmvSet2 = new HashSet<Integer>(Arrays.asList(7, 2, 22));
    Set<Integer> exchangeSet1 = new HashSet<Integer>(List.of(19));
    Set<Integer> exchangeSet2 = new HashSet<Integer>(List.of(20));

    ChanceCardCollector collector = new ChanceCardCollector();
    ChanceCardRemover remover = new ChanceCardRemover();
    ChanceCardExchanger exchanger = new ChanceCardExchanger();

    public void execute(int cardID, Board board, TreasureIsland treasureIsland, PirateIsland pirateIsland, FlatIsland flatIsland, Player[] players, Player player, Orientation newDirection, boolean takeTreasure, Player chosenPlayer) {

        //ChanceCardCollector Branch
        if (collectorSet1.contains(cardID)) {
            collector.branchCommon(cardID, treasureIsland, pirateIsland, flatIsland, player, takeTreasure);
        } else if (collectorSet2.contains(cardID)) {
            collector.branchOrientation(cardID, treasureIsland, pirateIsland, player, newDirection, takeTreasure);
        } else if (collectorSet3.contains(cardID)) {
            collector.branchPlayers(cardID, board, treasureIsland, pirateIsland, players, player, takeTreasure, chosenPlayer);
        } else if (collectorSet4.contains(cardID)) {
            collector.branchPlayersAndOrientation(cardID, treasureIsland, pirateIsland, players, player, newDirection, takeTreasure);
        }

        // ChanceCardRemover Branch
        else if (rmvSet1.contains(cardID)) {
            remover.branchCommon(cardID, treasureIsland, pirateIsland, flatIsland, player, takeTreasure);
        } else if (rmvSet2.contains(cardID)) {
            remover.branchPlayers(cardID, board, treasureIsland, pirateIsland, players, player, takeTreasure, chosenPlayer);
        }

        // ChanceCardExchanger Branch
        else if (exchangeSet1.contains(cardID)) {
            exchanger.branchCommon(cardID, treasureIsland, pirateIsland, flatIsland, player, takeTreasure);
        } else if (exchangeSet2.contains(cardID)) {
            exchanger.branchPlayers(cardID, board, treasureIsland, pirateIsland, players, player, takeTreasure, chosenPlayer);
        }
    }

    public ChanceCard peekDeck(TreasureIsland treasureIsland) {
        return treasureIsland.peekDeck();
    }

    public void drawDeck(TreasureIsland treasureIsland, Player cardDrawer) {
        final int MIN_HOLDABLE_CARD_NUM = 23, MAX_HOLDABLE_CARD_NUM = 26;
        ChanceCard drawnCard = treasureIsland.dealCard();
        if (drawnCard.getValue() >= MIN_HOLDABLE_CARD_NUM && drawnCard.getValue() <= MAX_HOLDABLE_CARD_NUM) {
            //Add Card to player
            cardDrawer.addChanceCard(drawnCard);
        } else {
            treasureIsland.returnCard(drawnCard);
        }
    }

    public void returnDeck(TreasureIsland treasureIsland, Player user, ChanceCard cardUsed) {
        // Return card from cardUser to treasureIsland
        if (user.withdrawCrewCard(cardUsed)) {
            treasureIsland.returnCard(cardUsed);
        }
    }
}

