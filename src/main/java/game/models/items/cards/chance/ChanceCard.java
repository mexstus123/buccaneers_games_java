package game.models.items.cards.chance;

import game.models.items.TradableItem;
import game.models.items.cards.Card;

/**
 * Card subclass for chance cards
 *
 * @author AAAA AAAAAA [AAA1]
 * @version AAAA
 * @(#) ChanceCard.java 2022/11/11
 *
 * Copyright (c) 2022 Aberystwyth University.
 * All rights reserved.
 */

public class ChanceCard extends Card {

    // ////////// //
    // Constants. //
    // ////////// //


    // //////////////// //
    // Class variables. //
    // //////////////// //


    // ////////////// //
    // Class methods. //
    // ////////////// //


    // /////////////////// //
    // Instance variables. //
    // /////////////////// //


    // ///////////// //
    // Constructors. //
    // ///////////// //


    // ////////////////////// //
    // Read/Write properties. //
    // ////////////////////// //


    // ///////////////////// //
    // Read-only properties. //
    // ///////////////////// //


    // //////// //
    // Methods. //
    // //////// //


    private String textOnCard;

    private int cardNumber;
    //private int cardNumber;
    private boolean usable = false;

    /**
     * constructor of the class
     * @param cardNumber
     */
    public ChanceCard(String type, int cardNumber){
        super(type,cardNumber, false);//TODO - value of cards
        usable = false;
        this.cardNumber = cardNumber;
        setTextOnCard(cardNumber);
    }

    public String getTextOnCard() {
        return textOnCard;
    }

    public int getCardNumber(){return cardNumber;}

    public boolean isUsable() {
        return usable;
    }

    /**
     * assigning the text for the card
     * depending on the card number
     * @param cardNumber int
     */
    public void setTextOnCard(int cardNumber) {
        switch (cardNumber) {
            case 1 -> textOnCard = """
                    Your ship is blown 5 leagues (5 squares)
                    off the coast of Treasure Island. If your
                    crew total is 3 or less, take 4 crew cards
                    from Pirate Island. If the square you are
                    blown to is already occupied, move one
                    square further)""";
            case 2 -> textOnCard = """
                    Present this card to any player who must
                    then give you 3 crew cards. This card must
                    be used at once then returned to the
                    Chance card pack""";
            case 3 -> textOnCard = """
                    You are blown to Mud Bay. If your crew
                    total is 3 or less, take 4 crew cards from
                    Pirate Island.""";
            case 4 -> textOnCard = """
                    You are blown to Cliff Creek. If your crew
                    total is 3 or less, take 4 crew cards from
                    Pirate Island""";
            case 5 -> textOnCard = """
                    You are blown to your Home Port. If your
                    crew total is 3 or less, take 4 crew cards
                    from Pirate Island""";
            case 6 -> textOnCard = """
                    You are blown to the nearest port in the
                    direction you are heading. If your crew
                    total is 3 or less, take 4 crew cards from
                    Pirate Island""";
            case 7 -> textOnCard = """
                    One treasure from your ship or 2 crew
                    cards from your hand are lost and washed
                    overboard to the nearest ship. If 2 ships are
                    equidistant from yours you may ignore this
                    instruction.""";
            case 8 -> textOnCard = """
                    One treasure from your ship or 2 crew
                    cards from your hand are lost and washed
                    overboard to Flat Island.""";
            case 9 -> textOnCard = """
                    Your most valuable treasure on board or if
                    no treasure, the best crew card from your
                    hand is washed overboard to Flat Island.""";
            case 10 -> textOnCard = """
                    The best crew card in your hand deserts for
                    Pirate Island. The card must be placed
                    there immediately""";
            case 11 -> textOnCard = """
                    Take treasure up to 5 in total value, or 2
                    crew cards from Pirate Island.""";
            case 12 -> textOnCard = """
                    Take treasure up to 4 in total value, or 2
                    crew cards from Pirate Island.""";
            case 13 -> textOnCard = """
                    Take treasure up to 5 in total value, or 2
                    crew cards from Pirate Island""";
            case 14 -> textOnCard = """
                    Take treasure up to 7 in total value, or 3
                    crew cards from Pirate Island.""";
            case 15 -> textOnCard = """
                    Take 2 crew cards from Pirate Island""";
            case 16 -> textOnCard = """
                    Take treasure up to 7 in total value and
                    reduce your ship's crew to 10, by taking
                    crew cards from your hand and placing
                    them on Pirate Island.""";
            case 17 -> textOnCard = """
                    Take treasure up to 6 in total value and
                    reduce your ship's crew to 11, by taking
                    crew cards from your hand and placing
                    them on Pirate Island.""";
            case 18 -> textOnCard = """
                    Take treasure up to 4 in total value, and if
                    your crew total is 7 or less, take 2 crew
                    cards from Pirate Island.""";
            case 19 -> textOnCard = """
                    Exchange all crew cards in your hand as
                    far as possible for the same number of
                    crew cards from Pirate Island.""";
            case 20 -> textOnCard = """
                    If the ship of another player is anchored at
                    Treasure Island, exchange 2 of your crew
                    cards with that player. Both turn your cards
                    face down and take 2 cards from each
                    others hands without looking at them. If
                    there is no other player at Treasure Island,
                    place 2 of your crew cards on Pirate Island.""";
            case 21 -> {textOnCard = """
                    Long John Silver (Keep this card).
                    When you arrive at a port where there are
                    crew for sale, you may exchange Long
                    John for up to 5 crew in value. If you land
                    at a Port where Long John has been left,
                    you may take him on payment of one
                    treasure to the Port. Once Long John has
                    been played, he is not returned to the pack""";
                super.setValue(5);
            }
            case 22 -> textOnCard = """
                    Yellow fever! An epidemic of yellow fever
                    strikes all ships and reduces the number of
                    crew. Every player with more than 7 crew
                    cards in their hand must bury the surplus 
                    crew cards at once on Pirate Island. Players
                    are at liberty to choose which cards to
                    bury""";
            case 23 -> {textOnCard = """
                    Doubloons (Keep this card).
                    This card may be traded for crew or
                    treasure up to value 5 in any port you visit.""";
                super.setValue(5);}
            case 24 -> {textOnCard = """
                    Pieces of eight (Keep this card).
                    This card may be traded for crew or
                    treasure up to value 4 in any port you visit.""";
                super.setValue(4);}
            case 25, 26 -> {textOnCard = """
                    Kidd's chart (Keep this card).
                    You may sail to the far side of Pirate
                    Island, on to the square marked with an
                    anchor. Land this chart there, and take
                    treasure up to 7 in total value from
                    Treasure Island.""";
                super.setUsable(true);}
            case 27 -> textOnCard = """
                    Take treasure up to 5 in total value, or 3
                    crew cards from Pirate Island.""";
            case 28 -> textOnCard = """
                    Take 2 crew cards from Pirate Island""";
        }
    }

    @Override
    public String toString() {
        return "ChanceCard{" +
                "cardNumber=" + super.getValue() +
                ",\ntextOnCard='" + textOnCard + '\'' +
                '}'+'\n';
    }

    @Override
    public boolean equals(TradableItem tradableItem) {
        return super.equals(tradableItem);
    }
}
