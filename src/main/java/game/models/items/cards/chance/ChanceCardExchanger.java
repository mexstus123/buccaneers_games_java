package game.models.items.cards.chance;


import game.models.board.Board;
import game.models.board.island.FlatIsland;
import game.models.board.island.PirateIsland;
import game.models.board.island.TreasureIsland;
import game.models.items.InventoryController;
import game.models.player.Orientation;
import game.models.player.Player;

/**
 * ToDo Add Class Functionality
 *
 * @author AAAA AAAAAA [AAA1]
 * @version AAAA
 * @(#) ChanceCardExchanger.java 2022/11/11
 *
 * Copyright (c) 2022 Aberystwyth University.
 * All rights reserved.
 */
public class ChanceCardExchanger extends ChanceCardMaster {

    // ////////// //
    // Constants. //
    // ////////// //


    // //////////////// //
    // Class variables. //
    // //////////////// //


    // ////////////// //
    // Class methods. //
    // ////////////// //


    // /////////////////// //
    // Instance variables. //
    // /////////////////// //


    // ///////////// //
    // Constructors. //
    // ///////////// //


    // ////////////////////// //
    // Read/Write properties. //
    // ////////////////////// //


    // ///////////////////// //
    // Read-only properties. //
    // ///////////////////// //


    // //////// //
    // Methods. //
    // //////// //

    // Helper method for identifying which player is anchored at Treasure Island
    // Ref Card ID 20
    public Player playerAtTreasureIsland(Board board, Player[] players, Player cardDrawer) {
        final int ISLAND_TREASURE_TILE_ID = 16;
        Player playerAtTreasureIsland = null;

        for (Player player : players) {

            // Case of other players anchored
            if (!player.equals(cardDrawer)) {
                if (board.getAdjacentIslandID(player.getX(), player.getY()) == ISLAND_TREASURE_TILE_ID) {
                    playerAtTreasureIsland = player;
                    break;
                }
            }
        }
        return playerAtTreasureIsland;
    }

    @Override
    public void branchCommon(int cardID, TreasureIsland treasureIsland, PirateIsland pirateIsland, FlatIsland flatIsland, Player cardDrawer, boolean takeTreasure) {
        // Ref Card ID 19
        transactCommon(cardID, treasureIsland, pirateIsland, cardDrawer, takeTreasure);
    }

    @Override
    public void branchOrientation(int cardID, TreasureIsland treasureIsland, PirateIsland pirateIsland, Player cardDrawer, Orientation newDirection, boolean takeTreasure) {

    }

    @Override
    public void branchPlayers(int cardID, Board board, TreasureIsland treasureIsland, PirateIsland pirateIsland, Player[] players, Player cardDrawer, boolean takeTreasure, Player chosenPlayer) {
        // Ref Card ID 20
        // Case of at least 2 other players anchored at Treasure Island
        if (chosenPlayer != null) {
            transactPlayers(cardID, null, null, players, chosenPlayer, cardDrawer);
        } else {
            // Case of only 1 other player anchored at Treasure Island
            Player playerAtTreasureIsland = playerAtTreasureIsland(board, players, cardDrawer);
            if (playerAtTreasureIsland != null) {
                transactPlayers(cardID, null, null, players, playerAtTreasureIsland, cardDrawer);
            } else {
                // Case of no other player anchored at Treasure Island
                transactCommon(cardID, treasureIsland, pirateIsland, cardDrawer, takeTreasure);
            }
        }
    }

    @Override
    public void branchPlayersAndOrientation(int cardID, TreasureIsland treasureIsland, PirateIsland pirateIsland, Player[] players, Player cardDrawer, Orientation newDirection, boolean takeTreasure) {

    }

    @Override
    public void transactCommon(int cardID, TreasureIsland treasureIsland, PirateIsland pirateIsland, Player cardDrawer, boolean takeTreasure) {
        // Ref Card ID 19, 20
        switch (cardID) {
            case 19:
                int initialCrewCardNum = cardDrawer.getCrewCount();
                InventoryController.reduceCrewCardBySize(cardDrawer, pirateIsland, 0);
                InventoryController.collectCrewCardByAmount(pirateIsland, cardDrawer, initialCrewCardNum);
                break;
            case 20:
                InventoryController.returnCrewCardExtremeByAmount(pirateIsland, null, cardDrawer, true, 2);
                break;
        }
    }

    @Override
    public void transactFlat(int cardID, FlatIsland flatIsland, Player cardDrawer, boolean takeTreasure) {

    }

    @Override
    public void transactPlayers(int cardID, Board board, PirateIsland pirateIsland, Player[] players, Player chosenPlayer, Player cardDrawer) {
        //Ref Card ID 20
        InventoryController.exchangeCrewCardExtremeByAmount(chosenPlayer, cardDrawer, 2);
    }

    @Override
    public void moveByCoordinates(int cardID, Player cardDrawer) {

    }

    @Override
    public void moveByPlayers(int cardID, Player[] players, Player cardDrawer) {

    }

}
