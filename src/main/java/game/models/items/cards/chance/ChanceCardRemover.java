/*
 *
 *  @(#) SomeClass.java ?.? 2022/05/??
 *
 *  Copyright (c) 2022 Aberystwyth University.
 *  All rights reserved.
 *
 * /
 */

package game.models.items.cards.chance;

import game.models.board.Board;
import game.models.board.island.FlatIsland;
import game.models.board.island.PirateIsland;
import game.models.board.island.TreasureIsland;
import game.models.items.InventoryController;
import game.models.player.Orientation;
import game.models.player.Player;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * ToDo Add Class Functionality
 *
 * @author AAAA AAAAAA [AAA1]
 * @version AAAA
 * @(#) ChanceCardRemover.java 2022/11/11
 *
 * Copyright (c) 2022 Aberystwyth University.
 * All rights reserved.
 */

public class ChanceCardRemover extends ChanceCardMaster{

    // ////////// //
    // Constants. //
    // ////////// //


    // //////////////// //
    // Class variables. //
    // //////////////// //


    // ////////////// //
    // Class methods. //
    // ////////////// //


    // /////////////////// //
    // Instance variables. //
    // /////////////////// //


    // ///////////// //
    // Constructors. //
    // ///////////// //


    // ////////////////////// //
    // Read/Write properties. //
    // ////////////////////// //


    // ///////////////////// //
    // Read-only properties. //
    // ///////////////////// //


    // //////// //
    // Methods. //
    // //////// //

    //TODO Helper method for determining distance of other players
    public Player nearestPlayer(Board board, Player[] players, Player cardDrawer) {
        // Ref Card ID 7
        // Case of 2 or more equally near players
        Player nearestPlayer = null;
        Map<Integer, Player> playersDistanceMap = new HashMap<>();
        ArrayList<Integer> playersDistanceList = new ArrayList<>();

        for (Player player: players) {
            int playerDistance = 0;
            if (!cardDrawer.equals(player)) {
                boolean isDiagionalOpposite = board.isDiagonalOpposite(cardDrawer, player);
                int end_x = player.getX(), end_y = player.getY();
                int curr_x = cardDrawer.getX(), curr_y = cardDrawer.getY();

                if (isDiagionalOpposite) {
                    //TODO Case of diagonal movement
                    while (!(end_x == curr_x && end_y == curr_y)) {

                        if (end_x > curr_x) {
                            curr_x++;
                        }
                        else
                            curr_x--;
                        if (end_y > curr_y) {
                            curr_y++;
                        }
                        else
                            curr_y--;
                        if (board.getStaticTiles(curr_x, curr_y) == 0) {
                            playerDistance++;
                        }
                    }
                }
                else {
                    //TODO Case of edging, both possible paths have to computed, as their total distance could differ
                    int tmpPlayerDistance1 = 0, tmpPlayerDistance2 = 0;

                    // First traverse vertically then horizontally
                    while (curr_y != end_y) {
                        if (end_y > curr_y) {
                            curr_y++;
                        }
                        else
                            curr_y--;
                        if (board.getStaticTiles(curr_x, curr_y) == 0) {
                            tmpPlayerDistance1++;
                        }
                    }
                    while (curr_x != end_x) {
                        if (end_x > curr_x) {
                            curr_x++;
                        }
                        else
                            curr_x--;
                        if (board.getStaticTiles(curr_x, curr_y) == 0) {
                            tmpPlayerDistance1++;
                        }
                    }

                    // First traverse horizontally then vertically
                    // Reset curr_x and curr_y
                    curr_x = cardDrawer.getX();
                    curr_y = cardDrawer.getY();
                    while (curr_x != end_x) {
                        if (end_x > curr_x) {
                            curr_x++;
                        }
                        else
                            curr_x--;
                        if (board.getStaticTiles(curr_x, curr_y) == 0) {
                            tmpPlayerDistance2++;
                        }
                    }
                    while (curr_y != end_y) {
                        if (end_y > curr_y) {
                            curr_y++;
                        }
                        else
                            curr_y--;
                        if (board.getStaticTiles(curr_x, curr_y) == 0) {
                            tmpPlayerDistance2++;
                        }
                    }
                    if (tmpPlayerDistance1 < tmpPlayerDistance2) {
                        playerDistance = tmpPlayerDistance1;
                    }
                    else {
                        playerDistance = tmpPlayerDistance2;
                    }
                }
            }
            if (!playersDistanceMap.containsKey(playerDistance)) {
                playersDistanceList.add(playerDistance);
                playersDistanceMap.put(playerDistance, player);
            }
            else
                break;

        }
        Collections.sort(playersDistanceList);

        // Case of a 2 equally nearest players
        if (playersDistanceMap.size() == 3) {
            nearestPlayer = playersDistanceMap.get(playersDistanceList.get(0));
        }

        return nearestPlayer;
    }

    @Override
    public void branchCommon(int cardID, TreasureIsland treasureList, PirateIsland crewPack, FlatIsland flatIsland, Player cardDrawer, boolean takeTreasure) {
        // Card ID 8 to 10
        if (cardID == 8 || cardID == 9)
            transactFlat(cardID, flatIsland, cardDrawer, takeTreasure);
        else
            transactCommon(cardID, treasureList, crewPack, cardDrawer, takeTreasure);
    }

    @Override
    public void branchOrientation(int cardID, TreasureIsland treasureList, PirateIsland crewPack, Player cardDrawer, Orientation newDirection, boolean takeTreasure) {

    }

    @Override
    public void branchPlayers(int cardID, Board board, TreasureIsland treasureList, PirateIsland crewPack, Player[] players, Player player, boolean takeTreasure, Player chosenPlayer) {
        // Card ID 7, 2 and 22
        // Case of 2 or more equally near players
        switch (cardID) {
            case 7:
                Player nearestPlayer = nearestPlayer(board, players, player);
                if (nearestPlayer != null)
                    transactPlayers(cardID, board, null, players, nearestPlayer, player);
                break;
            case 2:
                transactPlayers(cardID, null, null, players, chosenPlayer, player);
                break;
            case 22:
                transactPlayers(cardID, null, crewPack, players, null, null);
                break;
        }
    }

    @Override
    public void branchPlayersAndOrientation(int cardID, TreasureIsland treasureList, PirateIsland crewPack, Player[] players, Player cardDrawer, Orientation newDirection, boolean takeTreasure) {

    }

    @Override
    public void transactCommon(int cardID, TreasureIsland treasureList, PirateIsland crewPack, Player cardDrawer, boolean takeTreasure) {
        InventoryController.returnCrewCardExtremeByAmount(crewPack, null, cardDrawer, false, 1);
    }

    @Override
    public void transactFlat(int cardID, FlatIsland flatIsland, Player cardDrawer, boolean takeTreasure) {
        switch (cardID) {
            case 8:
                if (cardDrawer.getTreasure() != null)
                    InventoryController.returnTreasureExtreme(flatIsland, cardDrawer, true);
                else
                if (cardDrawer.getCrewHand() != null)
                    InventoryController.returnCrewCardExtremeByAmount(null, flatIsland, cardDrawer, true, 2);
                break;
            case 9:
                if (cardDrawer.getTreasure() != null)
                    InventoryController.returnTreasureExtreme(flatIsland, cardDrawer, false);
                else
                if (cardDrawer.getCrewHand() != null)
                    InventoryController.returnCrewCardExtremeByAmount(null, flatIsland, cardDrawer, false, 1);
                break;
        }
    }

    @Override
    public void transactPlayers(int cardID, Board board, PirateIsland crewPack, Player[] players, Player chosenPlayer, Player cardDrawer) {
        //TODO Card ID 7
        switch (cardID) {
            case 7:
                Player nearestPlayer = nearestPlayer(board, players, cardDrawer);
                if (nearestPlayer != null) {
                    if (cardDrawer.getTreasure() != null && nearestPlayer.getRoomForTreasure() > 0) {
                        InventoryController.transferTreasureExtreme(nearestPlayer, cardDrawer, true);
                    }
                    else
                    if (cardDrawer.getCrewHand() != null) {
                        InventoryController.transferCrewCardExtremeByAmount(nearestPlayer, cardDrawer, true, 2);
                    }
                }
                break;
            case 2:
                InventoryController.transferCrewCardExtremeByAmount(cardDrawer, chosenPlayer, true, 3);
                break;
            case 22:
                for (Player player: players) {
                    int maxAmount = player.getCrewCount() - 7;
                    if (maxAmount > 0)
                        InventoryController.returnCrewCardExtremeByAmount(crewPack, null, player, true, maxAmount);
                }
                break;
        }

    }

    @Override
    public void moveByCoordinates(int cardID, Player cardDrawer) {

    }

    @Override
    public void moveByPlayers(int cardID, Player[] players, Player cardDrawer) {

    }
}
