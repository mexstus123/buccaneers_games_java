package game.models.items.cards.chance;


import game.models.board.Board;
import game.models.board.island.FlatIsland;
import game.models.board.island.PirateIsland;
import game.models.board.island.TreasureIsland;
import game.models.items.InventoryController;
import game.models.player.Orientation;
import game.models.player.Player;

/**
 * ToDo Add Class Functionality
 *
 * @author AAAA AAAAAA [AAA1]
 * @version AAAA
 * @(#) ChanceCardCollector.java 2022/11/11
 *
 * Copyright (c) 2022 Aberystwyth University.
 * All rights reserved.
 */
public class ChanceCardCollector extends ChanceCardMaster {

    // ////////// //
    // Constants. //
    // ////////// //


    // //////////////// //
    // Class variables. //
    // //////////////// //


    // ////////////// //
    // Class methods. //
    // ////////////// //


    // /////////////////// //
    // Instance variables. //
    // /////////////////// //


    // ///////////// //
    // Constructors. //
    // ///////////// //


    // ////////////////////// //
    // Read/Write properties. //
    // ////////////////////// //


    // ///////////////////// //
    // Read-only properties. //
    // ///////////////////// //


    // //////// //
    // Methods. //
    // //////// //


    public void branchCommon(int cardID, TreasureIsland treasureIsland, PirateIsland pirateIsland, FlatIsland flatIsland, Player cardDrawer, boolean takeTreasure) {
        //TODO Card ID: 11 to 15, 16 to 18, 27, 28
        if (cardID > 10 && cardID < 19 || cardID == 27 || cardID == 28)
            transactCommon(cardID, treasureIsland, pirateIsland, cardDrawer, takeTreasure);
    }

    public void branchOrientation(int cardID, TreasureIsland treasureIsland, PirateIsland pirateIsland, Player cardDrawer, Orientation newDirection, boolean takeTreasure) {
        //TODO Card ID: 3, 4
        if (cardID == 3 || cardID == 4) {
            transactCommon(cardID, treasureIsland, pirateIsland, cardDrawer, takeTreasure);
            moveByCoordinates(cardID, cardDrawer);
            cardDrawer.setOrientation(newDirection);
        }
    }

    public void branchPlayers(int cardID, Board board, TreasureIsland treasureIsland, PirateIsland pirateIsland, Player[] players, Player player, boolean takeTreasure, Player chosenPlayer) {
        // Card ID: 5, 6, 25, 26
        if (cardID == 5 || cardID == 6 || cardID == 25 || cardID == 26) {
            transactCommon(cardID, treasureIsland, pirateIsland, player, takeTreasure);
            moveByPlayers(cardID, players, player);
        }
    }

    public void branchPlayersAndOrientation(int cardID, TreasureIsland treasureIsland, PirateIsland pirateIsland, Player[] players, Player cardDrawer, Orientation newDirection, boolean takeTreasure) {
        // Card ID: 1
        if (cardID == 1) {
            transactCommon(cardID, treasureIsland, pirateIsland, cardDrawer, takeTreasure);
            moveByPlayers(cardID, players, cardDrawer);
            cardDrawer.setOrientation(newDirection);
        }
    }

    public void transactCommon(int cardID, TreasureIsland treasureIsland, PirateIsland pirateIsland, Player cardDrawer, boolean takeTreasure) {
        switch (cardID) {
            case 1:
            case 3:
            case 4:
            case 5:
            case 6:
                if (cardDrawer.getCrewSize() <= 3)
                    InventoryController.collectCrewCardByAmount(pirateIsland, cardDrawer, 4);
                break;
            case 11:
            case 13:
                if (takeTreasure) InventoryController.collectTreasureByValue(treasureIsland, cardDrawer, 5);
                else InventoryController.collectCrewCardByAmount(pirateIsland, cardDrawer, 2);
                break;
            case 12:
                if (takeTreasure) InventoryController.collectTreasureByValue(treasureIsland, cardDrawer, 4);
                else InventoryController.collectCrewCardByAmount(pirateIsland, cardDrawer, 2);
                break;
            case 14:
                if (takeTreasure) InventoryController.collectTreasureByValue(treasureIsland, cardDrawer, 7);
                else InventoryController.collectCrewCardByAmount(pirateIsland, cardDrawer, 3);
                break;
            case 15:
                InventoryController.collectCrewCardByAmount(pirateIsland, cardDrawer, 2);
                break;
            case 16:
                if (takeTreasure) InventoryController.collectTreasureByValue(treasureIsland, cardDrawer, 7);
                else InventoryController.reduceCrewCardBySize(cardDrawer, pirateIsland, 10);
                break;
            case 17:
                if (takeTreasure) InventoryController.collectTreasureByValue(treasureIsland, cardDrawer, 6);
                else InventoryController.reduceCrewCardBySize(cardDrawer, pirateIsland, 11);
                break;
            case 18:
                InventoryController.collectTreasureByValue(treasureIsland, cardDrawer, 4);
                if (cardDrawer.getCrewSize() <= 7)
                    InventoryController.collectCrewCardByAmount(pirateIsland, cardDrawer, 2);
                break;
            case 25:
            case 26:
                InventoryController.collectTreasureByValue(treasureIsland, cardDrawer, 7);
                break;
            case 27:
                if (takeTreasure) InventoryController.collectTreasureByValue(treasureIsland, cardDrawer, 5);
                else InventoryController.collectCrewCardByAmount(pirateIsland, cardDrawer, 3);
                break;
            case 28:
                InventoryController.collectCrewCardByAmount(pirateIsland, cardDrawer, 2);
                break;
        }
    }

    @Override
    public void transactFlat(int cardID, FlatIsland flatIsland, Player cardDrawer, boolean takeTreasure) {

    }

    @Override
    public void transactPlayers(int cardID, Board board, PirateIsland pirateIsland, Player[] players, Player chosenPlayer, Player cardDrawer) {

    }

    public void moveByCoordinates(int cardID, Player cardDrawer) {
        switch (cardID) {
            case 3:
                cardDrawer.setCoordinates(1, 1);
                break;
            case 4:
                cardDrawer.setCoordinates(20, 20);
                break;
        }
    }

    public void moveByPlayers(int cardID, Player[] players, Player cardDrawer) {
        final int MIN_POS = 0, MAX_POS = 20, MIN_DISTANCE = 5, ISLAND_UPPER_BOUND = 13, ISLAND_LOWER_BOUND = 8;

        switch (cardID) {
            case 1:
                //TODO Calculation of nearest unoccupied tile off coast, at least 5 squares away
                int playerX = cardDrawer.getX(), playerY = cardDrawer.getY();

                // Vertical movement
                if ((playerX > ISLAND_LOWER_BOUND && playerX < ISLAND_UPPER_BOUND) && (playerY == ISLAND_LOWER_BOUND || playerY == ISLAND_UPPER_BOUND)) {
                    if (playerY > MAX_POS / 2) playerY += MIN_DISTANCE;
                    else playerY -= MIN_DISTANCE;
                    while (isOccupied(players, playerX, playerY) && (playerY != MIN_POS && playerY != MAX_POS)) {
                        if (playerY > MAX_POS / 2) playerY += 1;
                        else playerY -= 1;
                    }

                }
                // Horizontal movement
                else if ((playerY > ISLAND_LOWER_BOUND && playerY < ISLAND_UPPER_BOUND) && (playerX == ISLAND_LOWER_BOUND || playerX == ISLAND_UPPER_BOUND)) {
                    if (playerX > MAX_POS / 2) playerX += MIN_DISTANCE;
                    else playerX -= MIN_DISTANCE;
                    while (isOccupied(players, playerX, playerY) && (playerX != MIN_POS && playerX != MAX_POS)) {
                        if (playerX > MAX_POS / 2) playerX += 1;
                        else playerX -= 1;
                    }
                }
                // Diagonal movement
                else {
                    // Move away diagonally if MIN_DISTANCE not reached || another player occupied the tile
                    while (isOccupied(players, playerX, playerY) && (Math.abs(cardDrawer.getX() - playerX) < MIN_DISTANCE || (playerX < MAX_POS && playerY < MAX_POS && playerX > MIN_POS && playerY > MIN_POS))) {
                        if (playerX < MAX_POS / 2) {
                            playerX -= 1;
                        } else playerX += 1;

                        if (playerY < MAX_POS / 2) {
                            playerY -= 1;
                        } else playerY += 1;

                    }
                }
                cardDrawer.setCoordinates(playerX, playerY);

                break;
            case 5:
                if (!isOccupied(players, cardDrawer.getHomePort().getX(), cardDrawer.getHomePort().getY()))
                    cardDrawer.setCoordinates(cardDrawer.getHomePort().getCoordinates());
                break;
            case 6:
                if (!isOccupied(players, 20, 14)) cardDrawer.setCoordinates(19, 13);
                break;
            case 25:
            case 26:
                if (!isOccupied(players, 20, 1)) cardDrawer.setCoordinates(20, 1);
                break;
        }
    }
}
