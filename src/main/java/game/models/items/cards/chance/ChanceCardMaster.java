package game.models.items.cards.chance;


import game.models.board.Board;
import game.models.board.island.FlatIsland;
import game.models.board.island.PirateIsland;
import game.models.board.island.TreasureIsland;
import game.models.player.Orientation;
import game.models.player.Player;


/**
 * ToDo Add Class Functionality
 *
 * @author AAAA AAAAAA [AAA1]
 * @version AAAA
 * @(#) ChanceCardMaster.java 2022/11/11
 *
 * Copyright (c) 2022 Aberystwyth University.
 * All rights reserved.
 */
public abstract class ChanceCardMaster {

    // ////////// //
    // Constants. //
    // ////////// //


    // //////////////// //
    // Class variables. //
    // //////////////// //


    // ////////////// //
    // Class methods. //
    // ////////////// //


    // /////////////////// //
    // Instance variables. //
    // /////////////////// //


    // ///////////// //
    // Constructors. //
    // ///////////// //


    // ////////////////////// //
    // Read/Write properties. //
    // ////////////////////// //


    // ///////////////////// //
    // Read-only properties. //
    // ///////////////////// //


    // //////// //
    // Methods. //
    // //////// //

    /**
     *  Suffix Common indicates behaviours only involve a given player and island packs
     *  Suffix Orientation indicates behaviours affecting final orientation of player's ship
     *  Suffix Players indicates behaviours / computation involve 2 or more players
     */

    public abstract void branchCommon(int cardID, TreasureIsland treasureIsland, PirateIsland pirateIsland, FlatIsland flatIsland, Player cardDrawer, boolean takeTreasure);
    public abstract void branchOrientation(int cardID, TreasureIsland treasureIsland, PirateIsland pirateIsland, Player cardDrawer, Orientation newDirection, boolean takeTreasure);
    public abstract void branchPlayers(int cardID, Board board, TreasureIsland treasureIsland, PirateIsland pirateIsland, Player[] players, Player player, boolean takeTreasure, Player chosenPlayer);
    public abstract void branchPlayersAndOrientation(int cardID, TreasureIsland treasureIsland, PirateIsland pirateIsland, Player[] players, Player cardDrawer, Orientation newDirection, boolean takeTreasure);

    public abstract void transactCommon(int cardID, TreasureIsland treasureIsland, PirateIsland pirateIsland, Player cardDrawer, boolean takeTreasure);
    public abstract void transactFlat(int cardID, FlatIsland flatIsland, Player cardDrawer, boolean takeTreasure);
    public abstract void transactPlayers(int cardID, Board board, PirateIsland pirateIsland, Player[] players, Player chosenPlayer, Player cardDrawer);

    public abstract void moveByCoordinates(int cardID, Player cardDrawer);
    public abstract void moveByPlayers(int cardID, Player[] players, Player cardDrawer);

    // Helper method
    public boolean isOccupied(Player[] players, int x, int y) {
        boolean isOccupied = false;
        for (Player player : players) {
            if (player.getX() == x && player.getY() == y) {
                isOccupied = true;
                break;
            }
        }
        return isOccupied;
    }
}
