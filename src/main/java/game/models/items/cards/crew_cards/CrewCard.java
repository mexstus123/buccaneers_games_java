package game.models.items.cards.crew_cards;

import game.models.items.TradableItem;
import game.models.items.cards.Card;

/**
 * Class for crew cards.
 * <p>
 * Constructor sets rank, colour and if it's black or not.
 *
 * @author (name)
 * @version 0.? (put status of version here)
 * @see //(ref to related classes)
 */
public class CrewCard extends Card {

    // ////////// //
    // Constants. //
    // ////////// //


    // //////////////// //
    // Class variables. //
    // //////////////// //


    // ////////////// //
    // Class methods. //
    // ////////////// //


    // /////////////////// //
    // Instance variables. //
    // /////////////////// //


    // ///////////// //
    // Constructors. //
    // ///////////// //


    // ////////////////////// //
    // Read/Write properties. //
    // ////////////////////// //


    // ///////////////////// //
    // Read-only properties. //
    // ///////////////////// //


    // //////// //
    // Methods. //
    // //////// //


    private int rank;
    private int color;
    private boolean isBlack;

    /**
     * constructor
     * @param type
     * @param rank int
     * @param setIsBlack int
     */
    public CrewCard(String type, int rank, int setIsBlack) {
        super(type, rank, false);
        this.rank = rank;
        this.color = color; // TODO BUG!!!!
        if(setIsBlack == 1){
            this.isBlack = false;
        }
        else{
            this.isBlack = true;
        }
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public boolean getIsBlack(){
        return isBlack;
    }


    @Override
    public String toString() {
        StringBuilder displayCard = new StringBuilder();

        switch (rank) {
            case 1, 2, 3 -> displayCard.append(rank);
            default -> {
            }
        }
        displayCard.append(" pirate ");

        switch (color) {
            case 1 -> displayCard.append("Red");
            case 2 -> displayCard.append("Black");
            default -> {
            }
        }
        return displayCard.toString();
    }

    //
    //TODO Jac94 Merging
    //

    @Override
    public boolean equals(TradableItem tradableItem) {
        return super.equals(tradableItem);
    }
}
