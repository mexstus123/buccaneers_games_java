package game.models.items.cards;

import game.models.items.TradableItem;

/**
 * Class for cards. It is a subclass of tradeable item.
 *
 * @author (name)
 * @version 0.? (put status of version here)
 * @see //(ref to related classes)
 */
public class Card extends TradableItem {
    private boolean usable;
    /**
     * Constructor
     * @param type
     * @param value
     */
    public Card(String type, int value, boolean usable) {
        super(type,value);
        this.usable = usable;
    }

    @Override
    public String getType() {
        return super.getType();
    }

    @Override
    public boolean equals(TradableItem tradableItem) {
        return super.equals(tradableItem);
    }



    public boolean isUsable() {
        return usable;
    }

    public void setUsable(boolean usable) {
        this.usable = usable;
    }


}
