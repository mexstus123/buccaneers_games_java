module BoardImplementation {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.graphics;
    requires com.fasterxml.jackson.databind;

    opens game.ui.main to javafx.fxml;
    exports game.ui.main;

    opens game.ui.controllers.gameplay to javafx.fxml;
    exports game.ui.controllers.gameplay;

    opens game.ui.controllers.setup to javafx.fxml;
    exports game.ui.controllers.setup;

    opens game.ui.components;
    exports game.ui.components;

    exports game.models.app;
    exports game.models.board;
    exports game.models.board.port;
    exports game.models.board.island;
    exports game.models.board.tile;
    exports game.models.player;
    exports game.exceptions;
    exports game.models.items;
    exports game.models.items.treasure;
    exports game.models.items.cards;
    exports game.models.items.cards.crew_cards;
    exports game.models.items.cards.chance;
    exports game.ui.components.tile;
}