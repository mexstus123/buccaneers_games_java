package game.models.items.cards.chance;

import game.models.MasterTest;
import game.models.board.Board;
import game.models.board.island.FlatIsland;
import game.models.board.island.PirateIsland;
import game.models.board.island.TreasureIsland;
import game.models.items.cards.crew_cards.CrewCard;
import game.models.items.treasure.Treasure;
import game.models.player.Orientation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class BranchCommonTests extends MasterTest {

    private final ChanceCardController chanceCardController = new ChanceCardController();
    private final ChanceCardCollector chanceCardCollector = new ChanceCardCollector();
    private final ArrayList<Treasure> treasures = new ArrayList<>();
    private final Board board = new Board(ports);
    private final TreasureIsland treasureIsland = new TreasureIsland(9, 9);
    private final FlatIsland flatIsland = new FlatIsland(2, 16);
    private final PirateIsland pirateIsland = new PirateIsland(17, 2);

    @BeforeEach
    @Override
    protected void initialize() {

    }



    /**
     * Check if:
     * -player adds 5 total value treasure to their inventory
     */

    @Test
    void chanceCard11Test1() {
        Treasure tradableItem = new Treasure("",5);
        treasureIsland.depositTradabelItem(tradableItem);
        chanceCardController.execute(11, board, treasureIsland, pirateIsland, flatIsland, players, players[0], Orientation.E, true, players[1]);
        assertEquals(5, players[0].getTotalTreasureValue());
    }

    /**
     * Check if:
     * -player adds 2 crew cards to hand when treasure is full
     */

    @Test
    void chanceCard11Test2() {
        CrewCard tradableItem = new CrewCard("",1,1);
        for (int i = 0; i < 10; i++){
            pirateIsland.depositCard(tradableItem);
        }
        chanceCardController.execute(11, board, treasureIsland, pirateIsland, flatIsland, players, players[0], Orientation.N, false, players[1]);
        assertEquals(2, players[0].getCrewSize());
    }

    /**
     * Check if:
     * -player adds 4 total treasure to their inventory
     */

    @Test
    void chanceCard12Test1() {
        Treasure tradableItem = new Treasure("",4);
        treasureIsland.depositTradabelItem(tradableItem);
        chanceCardController.execute(12, board, treasureIsland, pirateIsland, flatIsland, players, players[0], Orientation.E, true, players[1]);
        assertEquals(4, players[0].getTotalTreasureValue());
    }

    /**
     * Check if:
     * -player adds 2 crew cards to hand when treasure is full
     */

    @Test
    void chanceCard12Test2() {
        CrewCard crewCard = new CrewCard("",1,1);
        for (int i = 0; i < 10; i++){
            pirateIsland.depositCard(crewCard);
        }
        chanceCardController.execute(12, board, treasureIsland, pirateIsland, flatIsland, players, players[0], Orientation.N, false, players[1]);
        assertEquals(2, players[0].getCrewSize());
    }

    /**
     * Check if:
     * -player adds 5 total treasure to their inventory
     */


    @Test
    void chanceCard13Test1() {
        Treasure tradableItem = new Treasure("",5);
        treasureIsland.depositTradabelItem(tradableItem);
        chanceCardController.execute(13, board, treasureIsland, pirateIsland, flatIsland, players, players[0], Orientation.E, true, players[1]);
        assertEquals(5, players[0].getTotalTreasureValue());
    }

    /**
     * Check if:
     * -player adds 2 crew cards to their hand when treasure is full
     */

    @Test
    void chanceCard13Test2() {
        CrewCard crewCard = new CrewCard("",1,1);
        for (int i = 0; i < 10; i++){
            pirateIsland.depositCard(crewCard);
        }
        chanceCardController.execute(13, board, treasureIsland, pirateIsland, flatIsland, players, players[0], Orientation.N, false, players[1]);
        assertEquals(2, players[0].getCrewSize());
    }

    /**
     * Check if:
     * - player adds 7 total treasure to their inventory
     */

    @Test
    void chanceCard14Test1() {
        Treasure tradableItem1 = new Treasure("",5);
        Treasure tradableItem2 = new Treasure("",2);
        treasureIsland.depositTradabelItem(tradableItem1);
        treasureIsland.depositTradabelItem(tradableItem2);
        chanceCardController.execute(14, board, treasureIsland, pirateIsland, flatIsland, players, players[0], Orientation.E, true, players[1]);
        assertEquals(7, players[0].getTotalTreasureValue());
    }

    /**
     * Check if:
     * - player adds 3 crew cards to their hand when treasure is full
     */

    @Test
    void chanceCard14Test2() {
        CrewCard crewCard = new CrewCard("",1,1);
        for (int i = 0; i < 10; i++){
            pirateIsland.depositCard(crewCard);
        }
        chanceCardController.execute(14, board, treasureIsland, pirateIsland, flatIsland, players, players[0], Orientation.N, false, players[1]);
        assertEquals(3, players[0].getCrewSize());
    }

    /**
     * Check if:
     * - player adds 2 crew cards to their hand
     */

    @Test
    void chanceCard15Test() {
        CrewCard crewCard = new CrewCard("",1,1);
        for (int i = 0; i < 10; i++){
            pirateIsland.depositCard(crewCard);
        }
        chanceCardController.execute(15, board, treasureIsland, pirateIsland, flatIsland, players, players[0], Orientation.N, false, players[1]);
        assertEquals(2, players[0].getCrewSize());
    }

    /**
     * Check if:
     * - player adds 7 total treasure to their inventory
     * -player reduces their crew cards in hand from 10 when they have 15 total
     */

    @Test
    void chanceCard16Test1() {
        Treasure tradableItem1 = new Treasure("",5);
        Treasure tradableItem2 = new Treasure("",2);
        Treasure tradableItem3 = new Treasure("",3);
        treasureIsland.depositTradabelItem(tradableItem1);
        treasureIsland.depositTradabelItem(tradableItem2);
        treasureIsland.depositTradabelItem(tradableItem3);
        for (int i = 0; i < 15; i++) {
            players[0].addCard(new CrewCard("",1, 1));
        }
        chanceCardController.execute(16, board, treasureIsland, pirateIsland, flatIsland, players, players[0], Orientation.N, true, players[1]);
        assertEquals(7, players[0].getTotalTreasureValue());
        assertEquals(10, players[0].getCrewSize());
    }

    @Test
    void chanceCard16Test2() {
        Treasure tradableItem1 = new Treasure("",5);
        Treasure tradableItem2 = new Treasure("",2);
        Treasure tradableItem3 = new Treasure("",3);
        treasureIsland.depositTradabelItem(tradableItem1);
        treasureIsland.depositTradabelItem(tradableItem2);
        treasureIsland.depositTradabelItem(tradableItem3);
        for (int i = 0; i < 5; i++) {
            players[0].addCard(new CrewCard("",1, 1));
        }
        chanceCardController.execute(16, board, treasureIsland, pirateIsland, flatIsland, players, players[0], Orientation.N, true, players[1]);
        assertEquals(7, players[0].getTotalTreasureValue());
        assertEquals(5, players[0].getCrewSize());
    }

    @Test
    void chanceCard17Test1() {
        Treasure tradableItem1 = new Treasure("",4);
        Treasure tradableItem2 = new Treasure("",2);
        Treasure tradableItem3 = new Treasure("",3);
        treasureIsland.depositTradabelItem(tradableItem1);
        treasureIsland.depositTradabelItem(tradableItem2);
        treasureIsland.depositTradabelItem(tradableItem3);
        for (int i = 0; i < 15; i++) {
            players[0].addCard(new CrewCard("",1, 1));
        }
        chanceCardController.execute(17, board, treasureIsland, pirateIsland, flatIsland, players, players[0], Orientation.N, true, players[1]);
        assertEquals(6, players[0].getTotalTreasureValue());
        assertEquals(11, players[0].getCrewSize());
    }

    @Test
    void chanceCard17Test2() {
        Treasure tradableItem1 = new Treasure("",4);
        Treasure tradableItem2 = new Treasure("",2);
        Treasure tradableItem3 = new Treasure("",3);
        treasureIsland.depositTradabelItem(tradableItem1);
        treasureIsland.depositTradabelItem(tradableItem2);
        treasureIsland.depositTradabelItem(tradableItem3);
        for (int i = 0; i < 7; i++) {
            players[0].addCard(new CrewCard("",1, 1));
        }
        chanceCardController.execute(17, board, treasureIsland, pirateIsland, flatIsland, players, players[0], Orientation.N, true, players[1]);
        assertEquals(6, players[0].getTotalTreasureValue());
        assertEquals(7, players[0].getCrewSize());
    }

    @Test
    void chanceCard18Test1() {
        for (int i = 0; i < 10; i++) {
            pirateIsland.depositCard(new CrewCard("",1,1));
        }
        for (int i = 0; i < 3; i++) {
            players[0].addCard(new CrewCard("",1, 1));
        }
        Treasure tradableItem = new Treasure("",4);
        treasureIsland.depositTradabelItem(tradableItem);
        chanceCardController.execute(18, board, treasureIsland, pirateIsland, flatIsland, players, players[0], Orientation.E, true, players[1]);
        assertEquals(4, players[0].getTotalTreasureValue());
        assertEquals(5, players[0].getCrewSize());
    }

    @Test
    void chanceCard18Test2() {
        for (int i = 0; i < 10; i++) {
            pirateIsland.depositCard(new CrewCard("",1,1));
        }
        for (int i = 0; i < 8; i++) {
            players[0].addCard(new CrewCard("",1, 1));
        }
        Treasure tradableItem = new Treasure("",4);
        treasureIsland.depositTradabelItem(tradableItem);
        chanceCardController.execute(18, board, treasureIsland, pirateIsland, flatIsland, players, players[0], Orientation.E, true, players[1]);
        assertEquals(4, players[0].getTotalTreasureValue());
        assertEquals(8, players[0].getCrewSize());
    }

    @Test
    void chanceCard27Test1() {
        Treasure tradableItem = new Treasure("",5);
        treasureIsland.depositTradabelItem(tradableItem);
        chanceCardController.execute(27, board, treasureIsland, pirateIsland, flatIsland, players, players[0], Orientation.E, true, players[1]);
        assertEquals(5, players[0].getTotalTreasureValue());
    }

    @Test
    void chanceCard27Test2() {
        CrewCard crewCard = new CrewCard("",1,1);
        for (int i = 0; i < 10; i++){
            pirateIsland.depositCard(crewCard);
        }
        chanceCardController.execute(27, board, treasureIsland, pirateIsland, flatIsland, players, players[0], Orientation.N, false, players[1]);
        assertEquals(3, players[0].getCrewSize());
    }

    @Test
    void chanceCard28Test1() {
        CrewCard crewCard = new CrewCard("",1,1);
        for (int i = 0; i < 10; i++){
            pirateIsland.depositCard(crewCard);
        }
        chanceCardController.execute(28, board, treasureIsland, pirateIsland, flatIsland, players, players[0], Orientation.N, false, players[1]);
        assertEquals(2, players[0].getCrewSize());
    }

    @Override
    protected void playerInfo() {

    }
}
