package game.models.items.cards.chance;

import game.models.MasterTest;
import game.models.board.Board;
import game.models.board.island.FlatIsland;
import game.models.board.island.PirateIsland;
import game.models.board.island.TreasureIsland;
import game.models.items.cards.crew_cards.CrewCard;
import game.models.items.treasure.Treasure;
import game.models.player.Orientation;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BranchPlayersAndOrientationTest extends MasterTest {

    private final ChanceCardController chanceCardController = new ChanceCardController();
    private final ChanceCardCollector chanceCardCollector = new ChanceCardCollector();
    private final ArrayList<Treasure> treasures = new ArrayList<>();
    private final Board board = new Board(ports);
    private final TreasureIsland treasureIsland = new TreasureIsland(9, 9);
    private final FlatIsland flatIsland = new FlatIsland(2, 16);
    private final PirateIsland pirateIsland = new PirateIsland(17, 2);

    @BeforeEach
    @Override
    protected void initialize() {
        testBoard = new Board(ports);
        //testBoard.initialize();
        System.out.println("Before testing:");
        testBoard.staticInfo();
        testBoard.playerInfo(players);
    }
    @AfterEach
    public void playerInfo() {
        System.out.println("After testing:");
        testBoard.playerInfo(players);
    }
    @Test
    void chanceCard1Test1(){
        for (int i = 0; i < 10; i++) {
            pirateIsland.depositCard(new CrewCard("",1,1));
        }
        players[0].setCoordinates(8,12);
        chanceCardController.execute(1, board, treasureIsland, pirateIsland, flatIsland, players, players[0], Orientation.N, true, players[1]);
        assertEquals(3, players[0].getX()) ;
        assertEquals(12, players[0].getY());
    }

    @Test
    void chanceCard1Test2(){
        for (int i = 0; i < 10; i++) {
            pirateIsland.depositCard(new CrewCard("",1,1));
        }
        players[0].setCoordinates(9,13);
        chanceCardController.execute(1, board, treasureIsland, pirateIsland, flatIsland, players, players[0], Orientation.N, true, players[1]);
        assertEquals(9, players[0].getX()) ;
        assertEquals(18, players[0].getY());
    }

    @Test
    void chanceCard1Test3(){
        for (int i = 0; i < 10; i++) {
            pirateIsland.depositCard(new CrewCard("",1,1));
        }
        players[0].setCoordinates(13,13);
        chanceCardController.execute(1, board, treasureIsland, pirateIsland, flatIsland, players, players[0], Orientation.N, true, players[1]);
        assertEquals(18, players[0].getX()) ;
        assertEquals(18, players[0].getY());
    }

    @Test
    void chanceCard1Test4(){
        for (int i = 0; i < 10; i++) {
            pirateIsland.depositCard(new CrewCard("",1,1));
        }
        players[0].setCoordinates(13,8);
        chanceCardController.execute(1, board, treasureIsland, pirateIsland, flatIsland, players, players[0], Orientation.N, true, players[1]);
        assertEquals(16, players[0].getX()) ;
        assertEquals(5, players[0].getY());
    }

    @Test
    void chanceCard1Test5() {
        for (int i = 0; i < 10; i++) {
            pirateIsland.depositCard(new CrewCard("",1, 1));
        }
        players[0].setCoordinates(13, 13);
        players[1].setCoordinates(18,18);
        chanceCardController.execute(1, board, treasureIsland, pirateIsland, flatIsland, players, players[0], Orientation.N, true, players[1]);
        assertEquals(19, players[0].getX());
        assertEquals(19, players[0].getY());
    }

}
