/*
 *
 *  @(#) SomeClass.java ?.? 2022/05/??
 *
 *  Copyright (c) 2022 Aberystwyth University.
 *  All rights reserved.
 *
 * /
 */

package game.models.items.cards.chance;

import game.models.MasterTest;
import game.models.board.Board;
import game.models.board.island.FlatIsland;
import game.models.board.island.PirateIsland;
import game.models.board.island.TreasureIsland;
import game.models.items.cards.crew_cards.CrewCard;
import game.models.items.treasure.Treasure;
import game.models.player.Orientation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import game.models.MasterTest;

/**
 * SomeClass - A class that does something.
 * <p>
 * How it is used
 *
 * @author adz11
 * @version 0.? (put status of version here)
 * @see //(ref to related classes)
 */

class BranchPlayersTests extends MasterTest {

    // ////////// //
    // Constants. //
    // ////////// //

    // //////////////// //
    // Class variables. //
    // //////////////// //

    private final ChanceCardController chanceCardController = new ChanceCardController();
    private final ChanceCardCollector chanceCardCollector = new ChanceCardCollector();
    private final ArrayList<Treasure> treasures = new ArrayList<>();
    private final Board board = new Board(ports);
    private final TreasureIsland treasureIsland = new TreasureIsland(9, 9);
    private final FlatIsland flatIsland = new FlatIsland(2, 16);
    private final PirateIsland pirateIsland = new PirateIsland(17, 2);


    // ////////////// //
    // Class methods. //
    // ////////////// //


    // /////////////////// //
    // Instance variables. //
    // /////////////////// //


    // ///////////// //
    // Constructors. //
    // ///////////// //


    // ////////////////////// //
    // Read/Write properties. //
    // ////////////////////// //


    // ///////////////////// //
    // Read-only properties. //
    // ///////////////////// //


    // //////// //
    // Methods. //
    // //////// //


    @Override
    @BeforeEach
    protected void initialize() {

    }

    /**
     * Check if:
     * - player is blown to Home Port
     * - player takes 4 crew cards with 3 in hand
     */
    @Test
    void chanceCard5Test1() {
        for (int i = 0; i < 10; i++) {
            pirateIsland.depositCard(new CrewCard("",1,1));
        }
        for( int i = 0; i < 3; i++){
            players[0].addCard(new CrewCard("",1, 1));

        }
        chanceCardController.execute(5, board, treasureIsland, pirateIsland, flatIsland, players, players[0], Orientation.E, true, players[1]);
        assertEquals(players[0].getX(), players[0].getHomePort().getX());
        assertEquals(players[0].getY(), players[0].getHomePort().getY());
        assertEquals(7, players[0].getCrewSize());

    }

    /**
     * Check if:
     * - player is blown to Home Port
     * - player takes 4 crew cards with 0 in hand
     */
    @Test
    void chanceCard5Test2() {
        for (int i = 0; i < 10; i++) {
            pirateIsland.depositCard(new CrewCard("",1,1));
        }
        for( int i = 0; i < 0; i++){
            players[0].addCard(new CrewCard("",1, 1));

        }
        chanceCardController.execute(5, board, treasureIsland, pirateIsland, flatIsland, players, players[0], Orientation.E, true, players[1]);
        assertEquals(players[0].getX(), players[0].getHomePort().getX());
        assertEquals(players[0].getY(), players[0].getHomePort().getY());
        assertEquals(4, players[0].getCrewSize());
    }

    /**
     * Check if:
     * - player is blown to Home Port
     * - player takes 0 crew cards with 7 in hand
     */
    @Test
    void chanceCard5Test3() {
        for (int i = 0; i < 10; i++) {
            pirateIsland.depositCard(new CrewCard("",1,1));
        }
        for( int i = 0; i < 7; i++){
            players[0].addCard(new CrewCard("",1, 1));

        }
        chanceCardController.execute(5, board, treasureIsland, pirateIsland, flatIsland, players, players[0], Orientation.E, true, players[1]);
        assertEquals(players[0].getX(), players[0].getHomePort().getX());
        assertEquals(players[0].getY(), players[0].getHomePort().getY());
        assertEquals(7, players[0].getCrewSize());
    }

    /**
     * Check if:
     * - player is blown to Venice port while heading West and on square 7, 7
     * - player takes 4 crew cards with 3 in hand
     * <p>
     */
    @Test
    void chanceCard6Test1() {
        for (int i = 0; i < 10; i++) {
            pirateIsland.depositCard(new CrewCard("",1,1));
        }
        players[0].move(7, 7);
        players[0].setOrientation(Orientation.W);
        for( int i = 0; i < 3; i++){
            players[0].addCard(new CrewCard("",1, 1));

        }
        chanceCardController.execute(6, board, treasureIsland, pirateIsland, flatIsland, players, players[0], Orientation.E, true, players[1]);
        assertEquals(7, players[0].getCrewSize());
        assertEquals(20,players[0].getX());
        assertEquals(14,players[0].getY());
    }

    /**
     * Check if:
     * - player is blown to Venice port while heading North-West and on square 7,1
     * - player takes 4 crew cards if he has 0 in hand
     * <p>
     */
    @Test
    void chanceCard6Test2() {
        players[0].move(7, 1);
        players[0].setOrientation(Orientation.NW);
        players[0].addCard(new CrewCard("",1, 1));
        players[0].addCard(new CrewCard("",1, 1));
        players[0].addCard(new CrewCard("",1, 1));
        players[0].addCard(new CrewCard("",1, 1));
        chanceCardController.execute(6, board, treasureIsland, pirateIsland, flatIsland, players, players[0], Orientation.E, true, players[1]);
        assertEquals(4, players[0].getHand().size());
        assertEquals(1,players[0].getX() );
        assertEquals(7,players[0].getY());
    }

    /**
     * Check if:
     * - player is blown to Venice port while heading North-West and on square 4,10
     * <p>
     */
    @Test
    void chanceCard6Test3() {
        players[0].move(4, 10);
        players[0].setOrientation(Orientation.NW);
        chanceCardController.execute(6, board, treasureIsland, pirateIsland, flatIsland, players, players[0], Orientation.E, true, players[1]);
        assertEquals(1,players[0].getX() );
        assertEquals(7,players[0].getY());
    }

    /**
     * Check if:
     * - player is blown to London Port while heading West and on square 4,11
     * <p>
     */
    @Test
    void chanceCard6Test4() {
        players[0].move(4, 11);
        players[0].setOrientation(Orientation.W);
        chanceCardController.execute(6, board, treasureIsland, pirateIsland, flatIsland, players, players[0], Orientation.E, true, players[0]);
        assertEquals(4, players[0].getHand().size());
        assertEquals(1,players[0].getX() );
        assertEquals(14, players[0].getY());
    }

    /**
     * Check if:
     * - player gets value worth of 7 from treasure island
     * <p>
     */
    @Test
    void chanceCard25Test1() {
        Treasure tradableItem1 = new Treasure("",4);
        Treasure tradableItem2 = new Treasure("",3);
        Treasure tradableItem3 = new Treasure("",2);
        treasureIsland.depositTradabelItem(tradableItem1);
        treasureIsland.depositTradabelItem(tradableItem2);
        treasureIsland.depositTradabelItem(tradableItem3);
        chanceCardController.execute(26, board, treasureIsland, pirateIsland, flatIsland, players, players[0], Orientation.E, true, players[1]);

        assertEquals(7, players[0].getTotalTreasureValue());
        assertEquals(20, players[0].getX());
        assertEquals(1, players[0].getY());
    }

    /**
     * Check if:
     * - player gets value worth of 7 from treasure island
     * <p>
     */
    @Test
    void chanceCard25Test2() {
        Treasure tradableItem1 = new Treasure("",4);
        Treasure tradableItem2 = new Treasure("",4);
        Treasure tradableItem3 = new Treasure("",3);
        treasureIsland.depositTradabelItem(tradableItem1);
        treasureIsland.depositTradabelItem(tradableItem2);
        treasureIsland.depositTradabelItem(tradableItem3);
        chanceCardController.execute(26, board, treasureIsland, pirateIsland, flatIsland, players, players[0], Orientation.E, true, players[1]);

        assertEquals(7, players[0].getTotalTreasureValue());
        assertEquals(20, players[0].getX());
        assertEquals(1, players[0].getY());
    }

    /**
     * Check if:
     * - player gets value worth of 7 from treasure island
     * <p>
     */
    @Test
    void chanceCard25Test3() {
        treasures.add(new Treasure("",5));
        treasures.add(new Treasure("",4));
        treasures.add(new Treasure("",3));
        treasures.add(new Treasure("",1));
        int treasureValue = 0;
        chanceCardController.execute(26, board, treasureIsland, pirateIsland, flatIsland, players, players[0], Orientation.E, true, players[1]);
        for (Treasure treasure : players[0].getTreasure()) {
            treasureValue += treasure.getValue();
        }
        assertEquals(6, treasureValue);
        assertEquals(2, treasures.size());
    }

    /**
     * Check if:
     * - player gets value worth of 7 from treasure island
     * <p>
     */
    @Test
    void chanceCard25Test4() {
        treasures.add(new Treasure("",1));
        treasures.add(new Treasure("",1));
        treasures.add(new Treasure("",1));
        int treasureValue = 0;
        chanceCardController.execute(26, board, treasureIsland, pirateIsland, flatIsland, players, players[0], Orientation.E, true, players[1]);
        for (Treasure treasure : players[0].getTreasure()) {
            treasureValue += treasure.getValue();
        }
        assertEquals(2, treasureValue);
        assertEquals(1, treasures.size());
    }

    /**
     * Check if:
     * - player gets only one most valuable treasure from TreasureIsland
     * <p>
     */
    @Test
    void chanceCard25Test5() {
        treasures.add(new Treasure("",5));
        treasures.add(new Treasure("",4));
        treasures.add(new Treasure("",3));
        players[0].addTreasure(new Treasure("",1));
        int treasureValue = 0;
        chanceCardController.execute(26, board, treasureIsland, pirateIsland, flatIsland, players, players[0], Orientation.E, true, players[1]);
        assertEquals(1, players[0].getTreasure().get(0).getValue());
        assertEquals(5, players[0].getTreasure().get(1).getValue());
        assertEquals(2, treasures.size());
    }

    /**
     * Check if:
     * - player gets only one most valuable treasure from TreasureIsland
     * <p>
     */
    @Test
    void chanceCard25Test6() {
        Treasure tradableItem1 = new Treasure("",3);
        Treasure tradableItem2 = new Treasure("",4);
        Treasure tradableItem3 = new Treasure("",5);
        treasureIsland.depositTradabelItem(tradableItem1);
        treasureIsland.depositTradabelItem(tradableItem2);
        treasureIsland.depositTradabelItem(tradableItem3);
        players[0].addTreasure(new Treasure("",1));
        chanceCardController.execute(26, board, treasureIsland, pirateIsland, flatIsland, players, players[0], Orientation.E, true, players[1]);
        assertEquals(1, players[0].getTreasure().get(0).getValue());
        assertEquals(5, players[0].getTreasure().get(1).getValue());
        assertEquals(2, treasures.size());
    }

    @Override
    protected void playerInfo() {

    }
}