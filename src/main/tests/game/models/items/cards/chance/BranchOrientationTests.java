package game.models.items.cards.chance;

import game.models.MasterTest;
import game.models.board.Board;
import game.models.board.island.FlatIsland;
import game.models.board.island.PirateIsland;
import game.models.board.island.TreasureIsland;
import game.models.items.cards.crew_cards.CrewCard;
import game.models.items.treasure.Treasure;
import game.models.player.Orientation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BranchOrientationTests extends MasterTest {

    private final ChanceCardController chanceCardController = new ChanceCardController();
    private final ChanceCardCollector chanceCardCollector = new ChanceCardCollector();
    private final ArrayList<Treasure> treasures = new ArrayList<>();
    private final Board board = new Board(ports);
    private final TreasureIsland treasureIsland = new TreasureIsland(9, 9);
    private final FlatIsland flatIsland = new FlatIsland(2, 16);
    private final PirateIsland pirateIsland = new PirateIsland(17, 2);

    @BeforeEach
    @Override
    protected void initialize() {

    }


    @Test
    void chanceCard3Test1() {
        for (int i = 0; i < 10; i++) {
            pirateIsland.depositCard(new CrewCard("",1, 1));
        }
        for (int i = 0; i < 3; i++) {
            players[0].addCard(new CrewCard("",1, 1));
        }
        chanceCardController.execute(3, board, treasureIsland, pirateIsland, flatIsland, players, players[0], Orientation.N, true, players[1]);
        assertEquals(7, players[0].getCrewSize());
        assertEquals(1, players[0].getX());
        assertEquals(1, players[0].getY());
    }

    @Test
    void chanceCard3Test2() {
        for (int i = 0; i < 10; i++) {
            pirateIsland.depositCard(new CrewCard("",1, 1));
        }
        for (int i = 0; i < 5; i++) {
            players[0].addCard(new CrewCard("",1, 1));
        }
        chanceCardController.execute(3, board, treasureIsland, pirateIsland, flatIsland, players, players[0], Orientation.N, true, players[1]);
        assertEquals(5, players[0].getCrewSize());
        assertEquals(1, players[0].getX());
        assertEquals(1, players[0].getY());
    }


    @Test
    void chanceCard4Test1() {
        for (int i = 0; i < 10; i++) {
            pirateIsland.depositCard(new CrewCard("",1, 1));
        }
        for (int i = 0; i < 3; i++) {
            players[0].addCard(new CrewCard("",1, 1));
        }
        chanceCardController.execute(4, board, treasureIsland, pirateIsland, flatIsland, players, players[0], Orientation.N, true, players[1]);
        assertEquals(7, players[0].getCrewSize());
        assertEquals(20, players[0].getX());
        assertEquals(20, players[0].getY());
    }

    @Test
    void chanceCard4Test2() {
        for (int i = 0; i < 10; i++) {
            pirateIsland.depositCard(new CrewCard("",1, 1));
        }
        for (int i = 0; i < 5; i++) {
            players[0].addCard(new CrewCard("",1, 1));
        }
        chanceCardController.execute(4, board, treasureIsland, pirateIsland, flatIsland, players, players[0], Orientation.N, true, players[1]);
        assertEquals(5, players[0].getCrewSize());
        assertEquals(20, players[0].getX());
        assertEquals(20, players[0].getY());
    }

    @Override
    protected void playerInfo() {

    }
}
