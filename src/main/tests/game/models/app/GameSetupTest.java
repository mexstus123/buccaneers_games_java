/*
 *
 *  @(#) SomeClass.java ?.? 2022/05/??
 *
 *  Copyright (c) 2022 Aberystwyth University.
 *  All rights reserved.
 *
 * /
 */

package game.models.app;

import game.models.MasterTest;
import game.models.board.port.HomePort;
import game.models.board.port.HomePortName;
import game.models.board.port.TradePort;
import game.models.items.TradableItem;
import game.models.items.cards.Card;
import game.models.items.cards.chance.ChanceCard;
import game.models.items.cards.crew_cards.CrewCard;
import game.models.player.Player;
import game.models.player.PlayerColor;
import game.models.player.PlayerSetup;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

import java.util.*;

import static game.models.board.port.HomePortName.*;
import static org.junit.jupiter.api.Assertions.*;

/**
 * SomeClass - A class that does something.
 * <p>
 * How it is used
 *
 * @author (name)
 * @version 0.? (put status of version here)
 * @see //(ref to related classes)
 */

public class GameSetupTest extends MasterTest {

    // ////////// //
    // Constants. //
    // ////////// //


    // //////////////// //
    // Class variables. //
    // //////////////// //


    // ////////////// //
    // Class methods. //
    // ////////////// //


    // /////////////////// //
    // Instance variables. //
    // /////////////////// //


    // ///////////// //
    // Constructors. //
    // ///////////// //

    private final PlayerSetup playerSetup1 = new PlayerSetup(PlayerColor.GREEN,false);
    private final PlayerSetup playerSetup2 = new PlayerSetup(PlayerColor.BLUE,false);
    private final PlayerSetup playerSetup3 = new PlayerSetup(PlayerColor.RED,false);
    private final PlayerSetup playerSetup4 = new PlayerSetup(PlayerColor.PINK,false);

    private final PlayerSetup[] playerSetupList = {playerSetup1, playerSetup2, playerSetup3, playerSetup4};

    private GameSetup gameSetup;

    private final int MIN_CHANCE_CARD_ID = 1, MAX_CHANCE_CARD_ID = 28, MIN_CREW_RANK = 1, MAX_CREW_RANK = 3,
            MAX_CREW_DECK_COUNT = 36, PLAYER_HAND_SETUP_COUNT = 5, CREW_SINGLE_COLOR_RANK_COUNT = 6,
            TRADE_PORT_SETUP_TOTAL_VALUE = 8, TRADE_PORT_CREW_CARD_SETUP_COUNT = 2;
    // ////////////////////// //
    // Read/Write properties. //
    // ////////////////////// //


    // ///////////////////// //
    // Read-only properties. //
    // ///////////////////// //


    // //////// //
    // Methods. //
    // //////// //


    @Override
    @BeforeEach
    protected void initialize() {
        gameSetup = new GameSetup(playerSetupList);
    }

    @Override
    @AfterEach
    protected void playerInfo() {

    }

    @RepeatedTest(3)
    public void testCrewDeckRandomizedThrice() {
        ArrayList<CrewCard> crewDeck1, crewDeck2;

        crewDeck1 = gameSetup.getCrewDeck();
        initialize();
        crewDeck2 = gameSetup.getCrewDeck();

        assertNotEquals(crewDeck1, crewDeck2);
    }

    @RepeatedTest(3)
    public void testCrewDeckSizeIs36() {
        assertEquals(MAX_CREW_DECK_COUNT, gameSetup.getSetupCrewSize());
    }

    @Test
    public void testCrewDeckColorsEachOf18() {

        assertEquals(18, gameSetup.getSetupCrewBlackCount());
        assertEquals(18, gameSetup.getSetupCrewRedCount());
    }
    @Test
    public void testCrewDeckAllRankEachOf6() {

        assertEquals(12, gameSetup.getSetupCrewRank1Count());
        assertEquals(12, gameSetup.getSetupCrewRank2Count());
        assertEquals(12, gameSetup.getSetupCrewRank3Count());
    }



    @RepeatedTest(3)
    public void testChanceDeckRandomizedThrice() {
        ArrayList<ChanceCard> chanceDeck1, chanceDeck2;

        chanceDeck1 = gameSetup.getChanceDeck();
        initialize();
        chanceDeck2 = gameSetup.getChanceDeck();

        assertNotEquals(chanceDeck1, chanceDeck2);
    }
    @Test
    public void testChanceDeckSizeIs28() {
        assertEquals(MAX_CHANCE_CARD_ID, gameSetup.getChanceDeck().size());
    }
    @Test
    public void testChanceDeckContainExactCardId() {
        ArrayList<Integer> chanceDeckIdList = new ArrayList<>();
        Set<Integer> chanceCardIdDefaultSet = new HashSet<>();

        for (int i = MIN_CHANCE_CARD_ID; i < MAX_CHANCE_CARD_ID + 1; i++) {
            chanceCardIdDefaultSet.add(i);
        }
        for (ChanceCard c: gameSetup.getChanceDeck()) {
            chanceDeckIdList.add(c.getValue());
        }

        assertTrue(chanceCardIdDefaultSet.containsAll(chanceDeckIdList));
    }

    @Test
    public void testPlayerHasHomePort() {
        List<HomePortName> playerHpList = new ArrayList<>();
        Set<HomePortName> hpDefaultSet = new HashSet<>();
        hpDefaultSet.add(LONDON);
        hpDefaultSet.add(GENOA);
        hpDefaultSet.add(MARSEILLES);
        hpDefaultSet.add(CADIZ);

        for (Player p: gameSetup.getPlayers()) {
            playerHpList.add(p.getHomePort().getType());
        }

        assertTrue(hpDefaultSet.containsAll(playerHpList));
    }
    @RepeatedTest(3)
    public void testPlayerHandRandomizedThrice() {
        List<List<Card>> playerHandList1 = new ArrayList<>(), playerHandList2 = new ArrayList<>();

        for (Player p: gameSetup.getPlayers()) {
            playerHandList1.add(p.getCrewHand());
        }

        initialize();
        for (Player p: gameSetup.getPlayers()) {
            playerHandList2.add(p.getCrewHand());
        }

        for (int i = 0; i < gameSetup.getPlayers().length; i++) {
            assertNotEquals(playerHandList1.get(i), playerHandList2.get(i));
        }
    }
    @Test
    public void testPlayerHandSizeIs5() {
        for (Player p: gameSetup.getPlayers()) {
            assertEquals(PLAYER_HAND_SETUP_COUNT, p.getCrewSize());
        }
    }


    @Test
    public void testTradePortCrewCardCountIs2() {

        for (TradePort tp: gameSetup.getTradePort()) {
            int crewCount = 0;
            for (TradableItem ti: tp.getInventory()) {
                if (ti instanceof CrewCard) {
                    crewCount++;
                }
            }
            assertEquals(TRADE_PORT_CREW_CARD_SETUP_COUNT, crewCount);
        }
    }
    @Test
    public void testTradePortTotalResourceSizeIs8() {

        for (TradePort tp: gameSetup.getTradePort()) {
            int itemValue = 0;
            for (TradableItem ti: tp.getInventory()) {
                itemValue += ti.getValue();
            }
            assertEquals(TRADE_PORT_SETUP_TOTAL_VALUE, itemValue);
        }
    }

}
