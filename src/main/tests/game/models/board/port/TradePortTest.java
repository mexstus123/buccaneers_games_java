package game.models.board.port;

import game.exceptions.EmptyInventoryException;
import game.models.MasterTest;
import game.models.items.TradableItem;
import game.models.items.cards.crew_cards.CrewCard;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * SomeClass - A class that does something.
 * <p>
 * How it is used
 *
 * @author (name)
 * @version 0.? (put status of version here)
 * @see //(ref to related classes)
 */

public class TradePortTest extends MasterTest {

    // ////////// //
    // Constants. //
    // ////////// //


    // //////////////// //
    // Class variables. //
    // //////////////// //


    // ////////////// //
    // Class methods. //
    // ////////////// //


    // /////////////////// //
    // Instance variables. //
    // /////////////////// //


    // ///////////// //
    // Constructors. //
    // ///////////// //


    // ////////////////////// //
    // Read/Write properties. //
    // ////////////////////// //


    // ///////////////////// //
    // Read-only properties. //
    // ///////////////////// //


    // //////// //
    // Methods. //
    // //////// //



    private final TradePort amsterdam = new TradePort(20, 14, "Amsterdam", 10);
    ArrayList<TradableItem> inventory;
    TradableItem tradableItem1, tradableItem2;



    @BeforeEach
    protected void initialize() {
        inventory = new ArrayList<>();
        tradableItem1 = new CrewCard(1, 1);
        tradableItem2 = new TradableItem(9);
    }

    @Override
    protected void playerInfo() {

    }

    @Test
    void testAddTradeableItem() {
        /*
        amsterdam.addTradableItem(tradableItem1);
        amsterdam.addTradableItem(tradableItem2);
        Assertions.assertEquals(2, inventory.size());

         */

    }

    @Test
    void testWithdrawItem() throws EmptyInventoryException {

        Assertions.assertEquals(0, inventory.size());
        amsterdam.addTradableItem(tradableItem1);
        Assertions.assertEquals(tradableItem1, amsterdam.withdrawTradableItem(tradableItem1));
    }

}
