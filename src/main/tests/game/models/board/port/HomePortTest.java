package game.models.board.port;

import game.models.MasterTest;
import game.models.items.TradableItem;
import game.models.items.cards.crew_cards.CrewCard;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.awt.*;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * SomeClass - A class that does something.
 * <p>
 * How it is used
 *
 * @author (name)
 * @version 0.? (put status of version here)
 * @see //(ref to related classes)
 */

public class HomePortTest extends MasterTest {

    // ////////// //
    // Constants. //
    // ////////// //


    // //////////////// //
    // Class variables. //
    // //////////////// //


    // ////////////// //
    // Class methods. //
    // ////////////// //


    // /////////////////// //
    // Instance variables. //
    // /////////////////// //


    // ///////////// //
    // Constructors. //
    // ///////////// //


    // ////////////////////// //
    // Read/Write properties. //
    // ////////////////////// //


    // ///////////////////// //
    // Read-only properties. //
    // ///////////////////// //


    // //////// //
    // Methods. //
    // //////// //

    HomePort london;
    ArrayList<TradableItem> inventory = new ArrayList<>();
    ArrayList<TradableItem> safeInventory = new ArrayList<>();
    game.models.items.TradableItem tradableItem1, tradableItem2, tradableItem3;



    @BeforeEach
    protected void initialize() {
        london = new HomePort(1, 14, HomePortName.LONDON, 5);
    }

    @Override
    protected void playerInfo() {

    }

    @Test
    void testName() {
        assertEquals(HomePortName.LONDON, london.getType());
    }

    @Test
    void testPositionX() {
        Assertions.assertEquals(1, london.getCoordinates().getKey());
    }

    @Test
    void testPositionY() {
        Assertions.assertEquals(14, london.getCoordinates().getValue());
    }

    @Test
    void testTitleID() {
        Assertions.assertEquals(5, london.getTileID());
    }

    @Test
    void testAddTradeableItem() {
        /*
        tradableItem1 = new CrewCard(1, 1);
        tradableItem2 = new game.models.items.treasure.TradableItem(3);
        tradableItem3 = new game.models.items.treasure.TradableItem(6);

        london.addTradableItem(tradableItem1);
        london.addTradableItem(tradableItem2);
        london.addTradableItem(tradableItem3);

        Assertions.assertEquals(3, london.getSafeInventory().size());
        Assertions.assertEquals(tradableItem1, london.getSafeInventory().get(0));
        Assertions.assertEquals(tradableItem3, london.getSafeInventory().get(2));


         */
    }

    @Test
    void testWithdrawItem() {
/*
        Assertions.assertNull(london.withdrawTreasure(tradableItem1));
        playerPort.addTradeableItem(tradableItem1);
        Assertions.assertEquals(tradableItem1, playerPort.withdrawTreasure(tradableItem1));


 */
    }

    @Test
    void testReturnTotalValue() {
        /*
        playerPort.addTradeableItem(tradableItem1);
        playerPort.addTradeableItem(tradableItem2);
        playerPort.addTradeableItem(tradableItem3);

        Assertions.assertEquals(27, playerPort.returnTotalValue());

         */
    }
}
