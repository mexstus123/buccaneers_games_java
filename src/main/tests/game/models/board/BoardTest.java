/*
 * @(#) SomeClass.java 1.1 2022/05/??
 *
 * Copyright (c) 2022 Aberystwyth University.
 * All rights reserved.
 *
 */
package game.models.board;

import game.models.MasterTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.*;

/**
 * BoardTest - A class that includes all possible boundary cases for current
 *              Board Class methods about validation of moves.
 * <p>
 * How it is used
 *
 * @author ysc1
 * @version 0.1.2 latest dev at dev/20220426/proto_pr-06_v3.2_ysc1
 * @see ()
 */

class BoardTest extends MasterTest {

    // ////////// //
    // Constants. //
    // ////////// //

    // //////////////// //
    // Class variables. //
    // //////////////// //


    // ////////////// //
    // Class methods. //
    // ////////////// //


    // /////////////////// //
    // Instance variables. //
    // /////////////////// //


    // ///////////// //
    // Constructors. //
    // ///////////// //


    // ////////////////////// //
    // Read/Write properties. //
    // ////////////////////// //


    // ///////////////////// //
    // Read-only properties. //
    // ///////////////////// //


    // //////// //
    // Methods. //
    // //////// //


    @BeforeEach
    protected void initialize() {
        testBoard = new Board(ports);
        testBoard.staticInfo();
        testBoard.playerInfo(players);
    }

    @AfterEach
    public void playerInfo() {
        testBoard.playerInfo(players);
    }

    /**
     *  Given coordinates validity check
     */
    @Test
    void testRejectMoveToOccupiedPort() {
        // Port London, ID 5
        assertTrue(testBoard.isInvalidMoves(players, 1, 14));
        // Port Cadiz, ID 8
        assertTrue(testBoard.isInvalidMoves(players, 14, 20));
    }
    @Test
    void testRejectMoveToInlandFlat() {
        for (int x = 2; x < 4; x++) {
            for (int y = 19; y > 16; y--) {
                assertTrue(testBoard.isInvalidMoves(players, x, y));
            }
        }
    }
    @Test
    void testRejectMoveToInLandPirate() {
        for (int x = 17; x < 20; x++) {
            for (int y = 5; y > 1; y--) {
                assertTrue(testBoard.isInvalidMoves(players, x, y));
            }
        }
    }
    @Test
    void testRejectMoveToInlandTreasure() {
        for (int x = 9; x < 13; x++) {
            for (int y = 12; y > 8; y--) {
                assertTrue(testBoard.isInvalidMoves(players, x, y));
            }
        }
    }

    @Test
    void testAcceptMoveToCombatOccupiedSea() {
        int[] ui_x = {7, 1, 20, 12};
        int[] ui_y = {20, 12, 11, 1};
        for (int i = 0; i < ui_x.length; i++) {
            players[0].move(ui_x[i], ui_y[i]);
            assertEquals(ui_x[i], players[0].getX());
            assertEquals(ui_y[i], players[0].getY());
            assertTrue(testBoard.isValidMoves(ui_x[i], ui_y[i]));
        }
    }
    @Test
    void testAcceptMoveToCombatOccupiedBay() {
        // ID 11, 12, 13
        int[] ui_x = {1, 20, 20};
        int[] ui_y = {1, 1, 20};
        for (int i = 0; i < ui_x.length; i++) {
            players[0].move(ui_x[i], ui_y[i]);
            assertEquals(ui_x[i], players[0].getX());
            assertEquals(ui_y[i], players[0].getY());
            assertTrue(testBoard.isValidMoves(ui_x[i], ui_y[i]));
        }
    }

    @Test
    void testAcceptMoveToUnoccupiedPortVenice() {
        // ID 9
        assertFalse(testBoard.isInvalidMoves(players, 1, 7));
        assertTrue(testBoard.isValidMoves(1, 7));
    }
    @Test
    void testAcceptMoveToUnoccupiedPortAmsterdam() {
        // ID 10
        assertFalse(testBoard.isInvalidMoves(players, 20, 14));
        assertTrue(testBoard.isValidMoves(20, 14));
    }
    @Test
    void testAcceptMoveToCoastIslandFlat() {
        // ID 14
        int[] ui_x1 = {1, 5};
        int[] ui_y1 = {19, 16};
        int[] ui_x2 = {2, 4};
        int[] ui_y2 = {20, 15};
        for (int i = 0; i < ui_x1.length; i++) {
            for (int j = 0; j < ui_y1.length; j++) {
                assertFalse(testBoard.isInvalidMoves(players, ui_x1[i], ui_y1[j]));
                assertTrue(testBoard.isValidMoves(ui_x1[i], ui_y1[j]));
                assertFalse(testBoard.isInvalidMoves(players, ui_x2[i], ui_y2[j]));
                assertTrue(testBoard.isValidMoves(ui_x2[i], ui_y2[j]));
            }
        }
    }
    @Test
    void testAcceptMoveToCoastIslandPirate() {
        // ID 15
        int[] ui_x1 = {16, 20};
        int[] ui_y1 = {5, 2};
        int[] ui_x2 = {17, 19};
        int[] ui_y2 = {6, 1};
        for (int i = 0; i < ui_x1.length; i++) {
            for (int j = 0; j < ui_y1.length; j++) {
                assertFalse(testBoard.isInvalidMoves(players, ui_x1[i], ui_y1[j]));
                assertTrue(testBoard.isValidMoves(ui_x1[i], ui_y1[j]));
                assertFalse(testBoard.isInvalidMoves(players, ui_x2[i], ui_y2[j]));
                assertTrue(testBoard.isValidMoves(ui_x2[i], ui_y2[j]));
            }
        }
    }
    @Test
    void testAcceptMoveToCoastIslandTreasure() {
        // ID 16
        int[] ui_x1 = {8, 13};
        int[] ui_y1 = {12, 9};
        int[] ui_x2 = {9, 12};
        int[] ui_y2 = {13, 8};
        for (int i = 0; i < ui_x1.length; i++) {
            for (int j = 0; j < ui_y1.length; j++) {
                assertFalse(testBoard.isInvalidMoves(players, ui_x1[i], ui_y1[j]));
                assertTrue(testBoard.isValidMoves(ui_x1[i], ui_y1[j]));
                assertFalse(testBoard.isInvalidMoves(players, ui_x2[i], ui_y2[j]));
                assertTrue(testBoard.isValidMoves(ui_x2[i], ui_y2[j]));
            }
        }
    }

    @Test
    void testFalseArriveStateNoPlayers() {
        players[0].move(1, 20);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(stream);
        PrintStream originalPrintStream = System.out;
        System.setOut(ps);

        // Output to stream
        testBoard.playerTurnState(players[0]);
        // Set it back
        System.setOut(originalPrintStream);
        String output = stream.toString();
        assertEquals("Player arrived at Flat Island!\r\n", output);
    }


    /**
     *  Player's arrival check
     */
    @Test
    void testArriveStatePort() {
        // Move to Port Venice, ID 9
        players[0].move(1, 7);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(stream);
        PrintStream originalPrintStream = System.out;
        System.setOut(ps);

        // Output to stream
        testBoard.playerTurnState(players[0]);
        // Set it back
        System.setOut(originalPrintStream);
        String output = stream.toString();
        assertEquals("Player arrived at a port!\r\n", output);
    }
    @Test
    void testArriveStateFlat() {
        // ID 14
        int[] ui_x1 = {1, 5};
        int[] ui_y1 = {19, 16, 20, 15};
        int[] ui_x2 = {2, 4};
        int[] ui_y2 = {20, 15};
        for (int i = 0; i < ui_x1.length; i++) {
            for (int j = 0; j < ui_y1.length; j++) {
                // Set 1
                players[0].move(ui_x1[i], ui_y1[j]);

                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                PrintStream ps = new PrintStream(stream);
                PrintStream originalPrintStream = System.out;
                System.setOut(ps);

                // Output to stream
                testBoard.playerTurnState(players[0]);
                // Set it back
                System.setOut(originalPrintStream);
                String output = stream.toString();
                assertEquals("Player arrived at Flat Island!\r\n", output);

                // Set 2
                if (i < ui_x2.length && j < ui_y2.length) {
                    players[0].move(ui_x2[i], ui_y2[j]);

                    stream = new ByteArrayOutputStream();
                    ps = new PrintStream(stream);
                    originalPrintStream = System.out;
                    System.setOut(ps);

                    // Output to stream
                    testBoard.playerTurnState(players[0]);
                    // Set it back
                    System.setOut(originalPrintStream);
                    output = stream.toString();
                    assertEquals("Player arrived at Flat Island!\r\n", output);
                }
            }
        }
    }
    @Test
    void testArriveStatePrivate() {
        // ID 15
        int[] ui_x1 = {16, 20};
        int[] ui_y1 = {5, 2, 6, 1};
        int[] ui_x2 = {17, 19};
        int[] ui_y2 = {6, 1};
        for (int i = 0; i < ui_x1.length; i++) {
            for (int j = 0; j < ui_y1.length; j++) {
                // Set 1
                if (ui_x1[i] != 20 && ui_y1[j] != 1) { // Special case for ID 12
                    players[0].move(ui_x1[i], ui_y1[j]);

                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    PrintStream ps = new PrintStream(stream);
                    PrintStream originalPrintStream = System.out;
                    System.setOut(ps);

                    // Output to stream
                    testBoard.playerTurnState(players[0]);
                    // Set it back
                    System.setOut(originalPrintStream);
                    String output = stream.toString();
                    assertEquals("Player arrived at Private Island!\r\n", output);

                    // Set 2
                    if (j < ui_y2.length) {
                        players[0].move(ui_x2[i], ui_y2[j]);

                        stream = new ByteArrayOutputStream();
                        ps = new PrintStream(stream);
                        originalPrintStream = System.out;
                        System.setOut(ps);

                        // Output to stream
                        testBoard.playerTurnState(players[0]);
                        // Set it back
                        System.setOut(originalPrintStream);
                        output = stream.toString();
                        assertEquals("Player arrived at Private Island!\r\n", output);
                    }
                }
            }
        }
    }
    @Test
    void testArriveStateTreasure() {
        // ID 16
        int[] ui_x1 = {8, 13};
        int[] ui_y1 = {12, 9, 13, 8};
        int[] ui_x2 = {9, 12};
        int[] ui_y2 = {13, 10};
        for (int i = 0; i < ui_x1.length; i++) {
            for (int j = 0; j < ui_y1.length; j++) {
                // Set 1
                players[0].move(ui_x1[i], ui_y1[j]);

                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                PrintStream ps = new PrintStream(stream);
                PrintStream originalPrintStream = System.out;
                System.setOut(ps);

                // Output to stream
                testBoard.playerTurnState(players[0]);
                // Set it back
                System.setOut(originalPrintStream);
                String output = stream.toString();
                assertEquals("Player arrived at Treasure Island!\r\n", output);

                // Set 2
                // Set 2
                if (i < ui_x2.length && j < ui_y2.length) {
                    players[0].move(ui_x2[i], ui_y2[j]);

                    stream = new ByteArrayOutputStream();
                    ps = new PrintStream(stream);
                    originalPrintStream = System.out;
                    System.setOut(ps);

                    // Output to stream
                    testBoard.playerTurnState(players[0]);
                    // Set it back
                    System.setOut(originalPrintStream);
                    output = stream.toString();
                    assertEquals("Player arrived at Treasure Island!\r\n", output);
                }
            }
        }
    }
    @Test
    void testArriveStateAnchor() {
        // ID 12
        players[0].move(20, 1);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(stream);
        PrintStream originalPrintStream = System.out;
        System.setOut(ps);

        // Output to stream
        testBoard.playerTurnState(players[0]);
        // Set it back
        System.setOut(originalPrintStream);
        String output = stream.toString();
        assertEquals("Player arrived at Anchor Bay!\r\n", output);
    }

    /**
     *  New tests for getPossibleMoves(), isDiagonalOpposite()
     */
    @Test
    void testPlayersDiagonalOpposite() {
        // Ref Card ID 7
        players[0].move(13, 13);
        players[1].move(15, 11);
        assertTrue(testBoard.isDiagonalOpposite(players[0], players[1]));
        players[0].move(8, 8);
        players[1].move(6, 10);
        assertTrue(testBoard.isDiagonalOpposite(players[0], players[1]));
    }
    @Test
    void testPlayersNotDiagonalOpposite() {
        // Ref Card ID 7
        players[0].move(13, 13);
        players[1].move(1, 20);
        assertFalse(testBoard.isDiagonalOpposite(players[0], players[1]));
        players[0].move(8, 8);
        players[1].move(7, 11);
        assertFalse(testBoard.isDiagonalOpposite(players[0], players[1]));
    }

    /**
     *  Methods to be completed after integration with Inventory System
     */
    @Test
    void testPlayerPossibleMovesPorts() {
        //List<BoardPiece> possibleMoves = testBoard.getPossibleMoves(player1);

    }
    @Test
    void testPlayerPossibleMovesCorner() {
        // Ref Card ID: 10, 7, 9
        //List<BoardPiece> possibleMoves = testBoard.getPossibleMoves(player1);
    }
    @Test
    void testPlayerPossibleMovesObstruct() {
        //List<BoardPiece> possibleMoves = testBoard.getPossibleMoves(player1);
    }
}
