package game.models.board.island;

import game.exceptions.EmptyInventoryException;
import game.models.MasterTest;
import game.models.items.TradableItem;
import game.models.items.cards.Card;
import game.models.items.cards.crew_cards.CrewCard;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * SomeClass - A class that does something.
 * <p>
 * How it is used
 *
 * @author (name)
 * @version 0.? (put status of version here)
 * @see //(ref to related classes)
 */

public class PirateIslandTest extends MasterTest {

    // ////////// //
    // Constants. //
    // ////////// //

    private final int MIN_CHANCE_CARD_ID = 1, MAX_CHANCE_CARD_ID = 28, MIN_CREW_RANK = 1, MAX_CREW_RANK = 3,
            MAX_CREW_DECK_COUNT = 36, PLAYER_HAND_SETUP_COUNT = 5, CREW_SINGLE_COLOR_RANK_COUNT = 6,
            TRADE_PORT_SETUP_TOTAL_VALUE = 8, TRADE_PORT_CREW_CARD_SETUP_COUNT = 2;
    private final int MAX_DECK_SIZE = 36;
    // //////////////// //
    // Class variables. //
    // //////////////// //


    // ////////////// //
    // Class methods. //
    // ////////////// //


    // /////////////////// //
    // Instance variables. //
    // /////////////////// //


    // ///////////// //
    // Constructors. //
    // ///////////// //


    // ////////////////////// //
    // Read/Write properties. //
    // ////////////////////// //


    // ///////////////////// //
    // Read-only properties. //
    // ///////////////////// //


    // //////// //
    // Methods. //
    // //////// //

    PirateIsland pirateIsland;

    @BeforeEach
    @Override
    protected void initialize() {
        pirateIsland = new PirateIsland(1, 1);
    }

    @Override
    protected void playerInfo() {

    }

    @Test
    void testDepositAllCardTypes() {

        //nested for loops to create each different type of crew card 6 times
        for (int i = 1; i <= 2; i++) { //for colour
            for (int j = MIN_CREW_RANK; j <= MAX_CREW_RANK; j++) { //for strength
                Card c = new CrewCard(j, i);
                pirateIsland.returnCard(c);
                assertTrue(pirateIsland.getInventory().contains(c));
            }
        }
    }

    @Test
    void testWithdrawAllCardTypes() {
        Queue<Card> cardQueue = new LinkedList<>();

        //nested for loops to create each different type of crew card 6 times
        for (int i = 1; i <= 2; i++) { //for colour
            for (int j = MIN_CREW_RANK; j <= MAX_CREW_RANK; j++) { //for strength
                Card c = new CrewCard(j, i);
                pirateIsland.returnCard(c);
                cardQueue.add(c);
            }
        }
        for (int i = 0; i < pirateIsland.getInventory().size(); i ++) {
            assertEquals(cardQueue.remove(), (pirateIsland.dealCard()));
        }
    }

    /**
     *  Expansion
     */
    /*
    @Test
    void testMaxSizeIs36() {
        //nested for loops to create each different type of crew card 6 times
        for (int i = 1; i <= 2; i++) { //for colour
            for (int j = MIN_CREW_RANK; j <= MAX_CREW_RANK; j++) { //for strength
                for (int k = 1; k <= 7; k++) {
                    pirateIsland.depositTradableItem(new CrewCard(j, i));
                }

            }
        }
        assertEquals(MAX_DECK_SIZE, pirateIsland.getInventory().size());
    }

     */
}
