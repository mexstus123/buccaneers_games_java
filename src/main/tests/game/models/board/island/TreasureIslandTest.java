package game.models.board.island;

import game.exceptions.EmptyInventoryException;
import game.models.MasterTest;
import game.models.items.TradableItem;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * SomeClass - A class that does something.
 * <p>
 * How it is used
 *
 * @author (name)
 * @version 0.? (put status of version here)
 * @see //(ref to related classes)
 */

public class TreasureIslandTest extends MasterTest {

    // ////////// //
    // Constants. //
    // ////////// //


    // //////////////// //
    // Class variables. //
    // //////////////// //


    // ////////////// //
    // Class methods. //
    // ////////////// //


    // /////////////////// //
    // Instance variables. //
    // /////////////////// //


    // ///////////// //
    // Constructors. //
    // ///////////// //


    // ////////////////////// //
    // Read/Write properties. //
    // ////////////////////// //


    // ///////////////////// //
    // Read-only properties. //
    // ///////////////////// //


    // //////// //
    // Methods. //
    // //////// //


    private final int MIN_CHANCE_CARD_ID = 1, MAX_CHANCE_CARD_ID = 28, MIN_CREW_RANK = 1, MAX_CREW_RANK = 3,
            MAX_CREW_DECK_COUNT = 36, PLAYER_HAND_SETUP_COUNT = 5, CREW_SINGLE_COLOR_RANK_COUNT = 6,
            TRADE_PORT_SETUP_TOTAL_VALUE = 8, TRADE_PORT_CREW_CARD_SETUP_COUNT = 2;
    private final int MAX_DECK_SIZE = 36;

    TreasureIsland treasureIsland;
    @BeforeEach
    protected void initialize() {
        treasureIsland = new TreasureIsland(1, 1);
    }

    @Override
    protected void playerInfo() {

    }

    @Test
    void testDepositAllTradableItemTypes() {
        //nested for loops to create each different type of crew card 6 times
        for (int i = 2; i <= 6; i++) { //for types
            TradableItem tradableItem = new TradableItem(i);

            //treasureIsland.depositTradableItem(tradableItem);
            //assertTrue(treasureIsland.getTreasureInventory().contains(tradableItem));

            //System.out.println(treasureIsland.getTreasureInventory().contains(new Treasure(i)));
            //assertTrue(treasureIsland.getTreasureInventory().contains(new Treasure(i)));
        }
    }

    @Test
    void testWithdrawAllTradableItemTypes() throws EmptyInventoryException {
        //nested for loops to create each different type of crew card 6 times
        for (int i = 1; i <= 2; i++) { //for colour
            for (int j = MIN_CREW_RANK; j <= MAX_CREW_RANK; j++) { //for strength
                //pirateIsland.depositTradableItem(new CrewCard(j, i));
            }
        }

        //nested for loops to create each different type of crew card 6 times
        for (int i = 1; i <= 2; i++) { //for colour
            for (int j = MIN_CREW_RANK; j <= MAX_CREW_RANK; j++) { //for strength
                //pirateIsland.withdrawTradableItem(new CrewCard(j, i));

            }
        }
        //assertTrue(pirateIsland.getInventory().isEmpty());
        //Assertions.assertNull(pirateIsland.withdrawTradableItem(tradableItem1));
        // pirateIsland.withdrawTradableItem(tradableItem1);
        //Assertions.assertEquals(tradableItem1, pirateIsland.withdrawTradableItem(tradableItem1));
    }

    @Test
    void testMaxTotalSizeIs20() {
        //nested for loops to create each different type of crew card 6 times
        for (int i = 2; i <= 6; i++) { //for types
            for (int j = MIN_CREW_RANK; j <= MAX_CREW_RANK; j++) { //for strength
                for (int k = 1; k <= 7; k++) {
                    // pirateIsland.depositTradableItem(new CrewCard(j, i));
                }

            }
        }
        //assertEquals(MAX_DECK_SIZE, pirateIsland.getInventory().size());
    }

    @Test
    void testMaxTypeSizeIs5() {

    }
}
