/*
 *
 *  @(#) SomeClass.java ?.? 2022/05/??
 *
 *  Copyright (c) 2022 Aberystwyth University.
 *  All rights reserved.
 *
 * /
 */

package game.models;

import game.models.board.Board;
import game.models.board.port.HomePort;
import game.models.board.port.HomePortName;
import game.models.board.port.Port;
import game.models.board.port.TradePort;
import game.models.player.Player;

/**
 * SomeClass - A class that does something.
 * <p>
 * How it is used
 *
 * @author ysc1
 * @version 0.? (put status of version here)
 * @see //(ref to related classes)
 */

public abstract class MasterTest {

    // ////////// //
    // Constants. //
    // ////////// //


    public static final int MAX_POS = 20, MIN_POS = 0;
    public static Board testBoard;

    private final HomePort london = new HomePort(1, 14, HomePortName.LONDON, 5);
    private final HomePort genoa = new HomePort(7, 1, HomePortName.GENOA, 6);
    private final HomePort marseilles = new HomePort(20, 7, HomePortName.MARSEILLES, 7);
    private final HomePort cadiz = new HomePort(14, 20, HomePortName.CADIZ, 8);

    private final TradePort amsterdam = new TradePort(20, 14, "Amsterdam", 10);
    private final TradePort venice = new TradePort(1, 7, "Venice", 9);

    public final Port[] ports = {london, genoa, marseilles, cadiz, venice, amsterdam};

    //creating the players
    private final Player player1 = new Player(null, london, null);
    private final Player player2 = new Player(null, genoa, null);
    private final Player player3 = new Player(null, marseilles, null);
    private final Player player4 = new Player(null, cadiz, null);

    public final Player[] players = {player1, player2, player3, player4};


    // //////////////// //
    // Class variables. //
    // //////////////// //


    // ////////////// //
    // Class methods. //
    // ////////////// //


    // /////////////////// //
    // Instance variables. //
    // /////////////////// //


    // ///////////// //
    // Constructors. //
    // ///////////// //


    // ////////////////////// //
    // Read/Write properties. //
    // ////////////////////// //


    // ///////////////////// //
    // Read-only properties. //
    // ///////////////////// //


    // //////// //
    // Methods. //
    // //////// //

    protected abstract void initialize();
    protected abstract void playerInfo();
}
